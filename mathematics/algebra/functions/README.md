# Basic Functions

## Types of basic functions

The types of basic functions that can be expressed in terms of operations of functions are


- Exponential functions in terms  of constant base
-  Linear functions
- Exponential functions in terms of variable base
- Logarithmic functions
- Special Functions 


## Nature of exponential functions

- A exponential function has a constant base.
- The power of an exponential function is variable.
- The expression for exponential function is expressed as
- $$ f(x) = a^{x} $$ 




## Nature of linear functions

- A linear function has a term on \( x \) .
- The term in \( x \) of a linear function is in the power of \( 1 \) .
- The expression for linear function is expressed as
- $$ f(x) = kx $$ 

## Nature of exponential functions in terms of a variable base

- A exponential function in terms of a variable base has a constant power.
- The expression for exponential functions in terms of variable base is expressed as
- $$ f(x) = x^{n} $$ 



## Nature of logarithmic function

- A logarithmic function is an inverse function.
- The function of which the logarithmic function is a inverse of is exponential function.
- The expression for logarithmic function is
- $$ f(x) = \log_{a}(x) $$ 

## Nature of special functions

- A special function in operation of basic function is a special case.
- The expression for special function in operations of basic function is

\begin{align*}
  f(x) = \left.
  \begin{cases}
  1 + x^{n} \\
  1 - x^{n} 
  \end{cases}
\right.
\end{align*}


##  Derivation on Operations on exponential functions in terms of constant base

\begin{align*}
f(x) = a^{x} \\
f(y) = a^{y} \\
f(x+y) = a^{x+y} \\
f(x+y) = a^{x} \times a^{y} \\
\end{align*}



## Expression for  Operations on exponential functions in terms of constant base

- A expression for the operation with a function satisfying relation in terms of a constant base is
- $$ f(x+y) = f(x)f(y) \implies f(x) = a^{x} $$ 

## Derivation on  Operations on linear functions


\begin{align*}
f(x) = kx \\
f(y) = ky \\
f(x+y) = k(x+y) \\
f(x+y) = kx + ky \\
\end{align*}


## Expression for  Operations on linear functions

- The expression for the operation with a function satisfying relation in terms of linear function is
- $$ f(x+y) =  f(x) + f(y) \implies f(x) = kx $$ 


## Derivation on  Operations on exponential functions in terms of variable base


\begin{align*}
f(x) = x^{n} \\
f(y) = y^{n} \\
f(xy) = (xy)^{n} \\
f(x+y) = x^{n} \times y^{n} \\
\end{align*}


## Expression for  Operations on exponential functions in terms of variable base

- The expression for the operation with a function satisfying relation in terms of exponential function with variable base is

- $$ f(xy) = f(x) \times f(y) \implies f(x) = x^{n} $$ 



##  Derivation on Operations of logarithmic functions


\begin{align*}
f(x) = \log_{a}(x) \\
f(y) = \log_{a}(y) \\
f(xy) = \log_{a}(xy) \\
f(xy) = \log_{a}(x) + \log_{a}(y)  \\
\end{align*}



## Expression for  Operations of logarithmic functions

- The expression for the operation with a function satisfying the relation in terms of logarithmic functions is
- $$ f(xy) = f(x) + f(y) \implies f(x) = \log_{a}(x) $$ 

## Expression for  Special functions


- The expression for special functions is



\begin{align*}
  f(x) \times f(\frac{1}{x}) = f(x) + f(\frac{1}{x}) \implies f(x) = \left.
  \begin{cases}
  1 + x^{n} \\
  1 - x^{n} 
  \end{cases}
\right.
\end{align*}

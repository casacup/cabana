# Co-ordinates in Space

## Numbers of dimensions in space

The number of dimensions in space is


- infinite \( \infty \) 


## Types of  co ordinate system

The types of co ordinate system are

- Cartesian co ordinate system
- Polar co ordinate system
- Cylindrical co ordinate system


## Expression of ordered pairs 

- Every ordered triplet is a point in the space.
- Conversely every point in the space an ordered triplet.

\begin{align*}
 \mathbb{R}^{3} = \mathbb{R} \times \mathbb{R} \times \mathbb{R}  \\
 \mathbb{R}^{3} = { (x,y,z): x,y,z \epsilon \mathbb{R}   }
\end{align*}



# Octants

- The three axes in space divide the space in 3 parts.
- Each division  of the three axes of the space is called an octant.



### Derivation of the distance of a  point from the origin

- An octant in space is taken.
- The three perpendicular axes in space represent the
   - $$ x - axis $$
   - $$ y - axis $$ 
   - $$ z - axis $$ 
- A point \( P(x,y,z) \) is taken in space.
- The point is resolved in the \( x-y \) plane.
- The point in the \( x-y \) plane is resolved in \( x \) axis.
- The point in the \( x-y \) plane is resolved in \( y \) axis.
- The resolution of the point in the \( x-y \) plane forms a rectangle.
- The diagonal of the rectangle represents the point resolved in the \( x-y \) plane.
- The magnitude of the length of diagonal of the rectangle is expressed as
    - $$ x^2 + y^2 $$   
- The perpendicular elevation of the point in space from the \( x-y \) plane is 
    - $$ z $$  
- The expression for the distance of the point in space from the origin is expressed in terms of
    - diagonal of the rectangle in the \( x-y \) plane
    - elevation of \( z \) to the point in space from the \( x-y \) plane
- The pythagorean theorem expresses the relation of the point in space with it's parameters as
    - $$ OP^{2} = x^{2} + y^{2} + z^{2}  $$  



## Expression for the distance of the point in the space from the origin

- The distance of a point in space from the origin is the non negative square root of sum of square of resolved coordinates of the point in the  space.
The expression for the distance of the point in the space from the origin is expressed as


- $$ |OP| = \sqrt{x^2 + y^2 + z^2 } $$ 

## Vector notation of the point in space

The point in the space is expressed in terms of vector notation as

\begin{align*}
\vec{r} = \vec{OP} = 
\left\langle
\begin{matrix}
x\\
y \\
z
\end{matrix}
\right\rangle
= x \hat{i} + y \hat{j} + z \hat{k}
\end{align*}
 

## Equation of the xy plane

The equation of the xy plane is expressed as


- $$ z = 0 $$ 


## Equation of the yz plane

The equation of the yz plane is expressed as


- $$ x = 0 $$ 



## Equation of the zx plane

The equation of the zx plane is expressed as


- $$ y = 0 $$ 



## Relation of three and two dimensional structures

Any equation on 2 dimension represents a plane on 3 dimensions.


## Pair of planes

The pair of planes in 3 dimensions is represented by the operator \( or \) .


- \( yz = 0  \) implies \( y =  0 \) or \( z = 0 \) which implies set of points in the \( y \) plane and \( z \) plane. 


# Equation of axes

The equation of axes in 3 dimensions is represented by the operator \( and \) .



## Equation of x axis

The equation of x axis is expressed as


-  $$ y^{2} + z^{2} = 0 $$ 
-  \( y = 0 \) and \( z = 0 \) which implies \( y^{2} + z^{2} = 0 \)  



## Equation of y axis

The equation of y axis is expressed as


-  $$ x^{2} + z^{2} = 0 $$ 
-  \( x = 0 \) and \( z = 0 \) which implies \( x^{2} + z^{2} = 0 \)  


## Equation of z axis

The equation of x axis is expressed as


-  $$ x^{2} + z^{2} = 0 $$ 
-  \( x = 0 \) and \( x = 0 \) which implies \( x^{2} + z^{2} = 0 \)  



### Distance of point in space 





















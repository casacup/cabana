### Condition for differential equation of first order and first degree to be homogenous

Differential equation should be a function of $$ \frac{y}{x} $$ 


### General expression for homogenous differential equation of first order and first degree 

$$ \frac{dy}{dx} = f(\frac{y}{x}) $$

### Derivation of solution of homogenous differential equation


- $$ \frac{y}{x} = v $$
- $$ y = vx $$ 
- $$ \frac{dy}{dx} = v + x \frac{dv}{dx} $$ 
- $$ f(v) = v + x \frac{dv}{dx} $$
- $$ \frac{dv}{f(v)-v} = \frac{dx}{x} $$ 


### Expression of solution of homogenous differential equation 

$$ \frac{dv}{f(v) -v} = \frac{dx}{x} $$




### Differential equation

Equation which contains derivatives with or without dependent variable or independent variable or both

### Condition for ordinary differential equation

Absence of partial derivative

### Order of differential equation

Order of highest derivative occurring in the equation

### Degree of differential equation

Power the highest derivative is raised to in a equation

### Solution of differential equation

Relation of variables free from derivatives satisfying source equation

### Terms for general solution in differential equation


- Complete solution
- Complete primitive

### General solution in differential equation

Relation of variable containing one arbitrary constant

### Particular solution in differential equation

Relation of variable containing specific constant instead of an arbitrary one



### Expression of autonomous differential equation

$$ \frac{dy}{dx} = f(y) $$ 


### Expression of general second order differential equation


- $$ P = Q = R = \text{ Function of x } $$ 

$$ \frac{d^{2}y}{dx^{2}} + P\frac{dy}{dx} + Q = R $$ 





### Expression of representation of differential equation in variables separated form

$$ f(y)dy = f(x)dx $$ 


### Expression of solution of differential equation in variables separated form

$$ \int f(y)dy = \int f(x)dx + C $$ 

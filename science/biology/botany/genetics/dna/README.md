﻿# Nucleic acids 


Linear polymer of nucleotides


### Discoverer of nucleic acids 

F. Meischer

### Year of discovery of nucleic acids

1868

### Term used by F. Meischer for nucleic acid

Nuclein

### Source of discovery of nucleic acid by F. Meischer 

Pus cells


### Discoverer of purine and pyrimidine bases

Fischer

### Year of discovery of purine and pyrimidine base by Fisher 

1880


### Person claiming nuclein for transmission of hereditary materials

Oskar Hertwig

### Year Oskar Hertwig claimed nuclein as hereditary material 

1884


### Person coining the term nucleic acid

Altman


### Year Altmann coined the term nucleic acid 

1899


### Cause of name of nucleic acid of nuclein 

Acidic nature of nuclein

### Discoverer of pentose sugars in nucleic acids 

Levene

### Year of discovery of pentose sugar by Levene in nucleic acids 

1910

# Components of nucleic Acid 

- Pentose sugar
- Nitrogen base
- Phosphate




### Number of types of pentose sugar

2

### Types of pentose sugars 



- Ribose sugar  

- Deoxy ribose sugar 


### Number of types of nitrogenous bases

2

### Types of nitrogenous bases 

- Purine base
- Pyrimidine base


### Purine Bases

Double ring heterocyclic compound

### List of purine Bases

- Adenine
- Guanine

### Pyrimidine Bases

Single ring heterocyclic compound

### List of pyrimidine Bases 

- Thymine
- Cytosine
- Uracil


### Number of purine bases

2

### Number of pyrimidine bases 


3


### Form of presence of phosphate in nucleic acids 

Phosphoric acid








# List of Types of DNA

- Balanced DNA
- Alternate DNA
- C DNA
- D DNA
- Zigzag DNA
- Circular DNA
- Linear DNA



### Abbreviation of Balanced DNA 

B DNA


### Number of base pairs at Balanced DNA

10

### Arrangement of base pairs of Balanced DNA with the axis of helix 

Right Angles


### Nature of activity of Balanced DNA 

- Physiological
- Biological

### Most common type of DNA

B DNA





### Abbreviation of Alternate DNA 

A DNA 


### Number of base pairs at alternate DNA 

11 



### Number of base pairs at C DNA 


9


### Number of base pairs at D DNA 


8



### Abbreviation of Zigzag DNA 

Z DNA


### Type of DNA exhibited by Z DNA on the basis of side

Left handed



### Number of base pairs at Zigzag DNA 

 12




### Arrangement of circular DNA

Covalent bond between ends of DNA duplex



### Location of circular DNA in types of  organisms 

Prokayrotes



### Condition of presence of histone proteins in circular DNA

Absent




### Location of Linear DNA in types of  organisms 

Eukayrotes


### Condition of presence of histone proteins in linear DNA

Present

# Functions of DNA 

- Transcription
- Regulation
- Combination
- Genetic stability
- Biochemistry


### Transcription in DNA

- Synthesis of RNA



Structure of DNA
================

-   **Type of Molecule:** DNA is a linear polymer.

-   **Quantity of nucleotides** :

    -   DNA is a linear polymer of millions of nucleotides.

-   **Contents:**

    -   DNA is a macromolecule.

    -   DNA is a long chain of subunits.

    -   The subunits of DNA are nucleotides.

-   **Composition of DNA** :

    -   Sugar

        -   DNA is composed of sugar called deoxyribose.

    -   Phosphate group

    -   Nitrogenous base

    **Location:** DNA is present in:

    -   Nucleus

    -   Cytoplasm of prokyarotes

    -   Chloroplast

-   **Measurement:**

    -   DNA is measured in picogram,

    -   $1pg = 10^{-12}gm$

-   **Quantity of DNA in human:**

    -   One human cells contains $5.6 pg$ of DNA.

    -   The length of DNA in a human cell is $174cm$ .

History
=======

Equality
--------

-   The amount of purine is equal to amount of pyrimidine base.
    $$A = T$$ $$G = C$$

-   This is the rule given by Chargaff.

-   This rule was given in 1950.

Xray Diffraction
================

-   The study was done by

    -   Rosalind Franklin

    -   Maurice Wilkins

-   The chemical and physical properties of DNA was determined by this
    experiment.

-   This experiment was done by x-ray diffraction method.

Double Helical Structure of DNA
===============================

-   The double helical molecular model of DNA was proposed by Watson and
    Crick.

-   The double helical molecular model of DNA was proposed in $1953$ .

-   This proposal was done on the basis of explanation of chemical data
    of:

    -   Wilkins

    -   Franklin

    -   Chragaff

-   The work for discovery of double helical structure of DNA was
    awarded a Nobel Prize.

-   The Nobel Prize was awarded in $1962$.

Characters of double helical structure of DNA
---------------------------------------------

-   The DNA is double helical right handed B-DNA.

-   **Contents:**

    -   DNA consists of two helical strands.

    -   The strands are of polynucleotide chains.

    -   **Contents of strand:**

        -   Phospate Sugar is present as backbone.

            -   The bond joining phosphate and sugar is called
                phosphodiester bond.

    -   The strands are joined together by nitrogenous bases.

        -   Nitrogenous bases are joined with one another by hydrogen
            bonds.

        -   **Location of nitrogenous bases** :

            -   Nitrogenous bases are located inside the strands.

            -   Their location is arranged perpendicularly to the long
                axis of DNA.

-   The strands are plectonimically coiled.

-   The strands are antiparallel.

    -   One strand runs in $5^{'}$ to $3^{'}$ carbon.

    -   One strand runs in $3^{'}$ to $5^{'}"$ carbon.

Base pairing of nitrogenous bases
---------------------------------

The base pairing of nitrogenous bases is very specific.

-   **A** *Adenine* pairs with **T** *Thymine*

    -   There is presence of only two hydrogen bond between adenine and
        thymine.

-   **G** *Guanine* pairs with **C** *Cytosine*

    -   There is presence of three hydrogen bond between guanine and
        cytosine.

Structural arrangement of DNA
=============================

-   **Number of base pairs in one complete spiral:** $10$

-   **Length of one complete spiral:** $34 \AA$

-   **Distance between two adjacent base pairs:** $3.4 \AA$

-   **Diameter of two strands:** $20 \AA$

Nature of information
---------------------

-   Genetic information is not coded by both strands of DNA.

-   One strand codes genetic information.

    -   The strand which codes genetic information is called sense
        strand.

-   One strand does not code genetic information.

    -   The strand which does not code genetic information is called non sense strand
 






Introduction
============

Plant anatomy
-------------

-   **Definition:**

    -   Plant anatomy is the study of:

        -   internal organization

        -   structure of plants

-   **Father of Plant Anatomy:** Nehemiah Grew (N. Grew)

Tissue
------

-   **Definition:** Tissues are :

    -   group of similar cells

    -   group of dissimilar cells

    -   alike in

        -   origin

        -   function

-   **Histology:** **Histology** is the study of tissues.

-   **Coined by:** N.Grew

-   **Coined in:** $1682$

### Types of plant tissues

The types of plant tissues are:

-   Meristematic Tissues

-   Permanent Tissues

-   Secretory Tissues

Meristematic Tissues
====================

Introduction
------------

-   **Nature of Division:** Meristematic tissues divide continuously.

-   **Location:** Growth Regions

    -   Tip of plant

    -   Apices of

        -   root

        -   shoot

-   **Function:** Meristematic tissue helps in

    -   growth of the plant.

Characteristics of meristematic tissues
---------------------------------------

-   **Coined by:** C. Nagelli

-   **Age:** Immature

-   **Spacing:** Compact

    -   Meristematic tissues have no intercellular spaces.

-   **Shape:** Isodiametric

    -   Meristematic tissues have same

        -   length

        -   breadth

        -   height

-   **Geometrical shape:**

    -   rounded

    -   oval

    -   polygonal

    -   fusiform

        -   Cells of cambium have fusiform cells.

-   **Type of Nucleus:** multi nucleated

-   **Structure of cell wall:**

    -   Cell walls are thin.

    -   Cell wall have only only primary cell wall.

    -   The primary cell wall is cellulosic.

    -   Cell wall is elastic.

        -   Cell wall is elastic because of immature cells.

-   **Type of plastids:** Pro plastids are present.

    -   Proplastids are the precursor of plastids.

-   **Structure of mitocohnodria:**

    -   Mitochondria have fewer cristae.

    -   Mitochondria have shorter cristae.

    -   Mitochondrian respiration rate is high.

-   **Storage:** Meristematic cells do not store reserve food.

-   **Examples:**

    -   root and shoot apices

    -   cambium

Functions of meristematic tissues:
----------------------------------

The functions of meristematic tissues are:

-   Growth of plant.

    -   Meristematic tissue does primary growth.

    -   Meristematic tissue does the secondary growth.

    -   Formation of new organs

        -   wood

        -   cork

Classification of meristematic tissues
--------------------------------------

### Origin

The meristematic tissues on the basis of origin are:

-   Promeristem

-   Primary meristem

-   Secondary meristem

### Position

The meristematic tissues on the basis of position are:

-   Apical meristem

-   Intercalary meristem

-   Lateral meristem

### Function

The meristematic tissue son the basis of functions are:

-   Protoderm

-   Procambium

-   Ground meristem

### Plane of division

The meristematic tissue son the basis of plane of division are:

-   Rib meristem

-   Mass meristem

-   Plate meristem

Promeristem
===========

-   **Origin:** Promeristem has embryonic origin.

-   Promeristem is also called

    -   Primordial meristem

    -   Eumeristem

-   **Age:** Promeristem are earliest and youngest.

-   **Location:** Promeristem are located at:

    -   Extreme tip of young growing root.

    -   Extreme tip of young growing shoot.

-   **Quantity:** Promeristem contain only few cells.

-   **Function:** Promeristem give rise to

    -   primary meristem.

Primary Meristem
================

-   **Origin:** Primary meristem is originated from promeristem.

-   **Age of formation:** Early in plant

-   **Type of Tissues:**

    -   active

    -   dividing

-   **Location:**

    -   below promeristem in the root tip

    -   below promeristem in the shoot tip

    -   intercalary position

-   **Function:**

    -   Primary meristem gives rise to primary permanent tissue

        -   Primary permanent tissues are formed by differentiation.

    -   Primary meristem gives rise to secondary permanent tissue.

        -   Secondary permanent tissue are formed by dedifferentation.

    -   Primary permanent tissues increases the

        -   length and size of root.

        -   length and size of stem.

-   **Examples**

    -   apical meristem

    -   intercalary meristem

    -   lateral meristem

    -   intra fasicular cambium

Secondary Meristem
==================

-   **Time of origin:** Secondary meristem originates

    -   later in the life cycle of plant.

-   **Functions:**

    -   Secondary meristem does the secondary growth of plant.

    -   Secondary meristem gives rise to secondary permanent tissue.

    -   **Examples of secondary permanent tissues:**

        -   secondary cortex

        -   secondary xylem

    -   Secondary permanent tissue are originated by dedifferentiation.

-   **Examples:**

    -   dicot stem

        -   inter fasicular cambium

        -   cork cambium

    -   dicot root

        -   cork cambium

        -   vascular cambium

Apical Meristem 
===============

-   **Location:**

    -   Apices of plant meristem

    -   Tip of root

    -   Tip of stem

    -   Tip of branches

    -   Root apex

    -   Shoot apex

-   **Structural position:**

    -   It is present in sub terminal position of root.

        -   The terminal position of root contains root cap.

        -   Root cap is also termed as calyptrogen.

    -   It is present at the apical position in shoot.

-   **Function:**

    -   Apical meristem increases the length of the plant body.

Intercalary Meristem
====================

-   **Location:**

    -   between permanent tissues

    -   base of leaves

    -   nodal regions

-   **Function:**

    -   Intercalary meristem does the primary growth of the plant.

-   **Examples:**

    -   Intercalary meristem are found at the base of pinus leaves.

        -   The intercalary meristem at the base of pinus leaves is
            basal meristem.

    -   Above the sporophytes of anthoceros

    -   Above the nodes of bamboo, grasses

    -   Below nodes of mint

    -   Stem of all monocots

    -   Petiole of leaves

    -   Leaf sheath of monocot

Lateral Meristem 
================

-   **Location:**

    -   Lateral side of stem

    -   Lateral side of roots

-   Components of lateral meristem divide periclinally.

-   Periclinal division increases the diameter of an organ.

-   **Function:**

    -   Lateral meristem is responsible of growth in thickness of plant
        body.

        -   Growth in thickness of plant body is secondary growth.

Types of lateral meristem
-------------------------

The types of lateral meristem are:

-   Primary Lateral meristem

-   Secondary lateral meristem

    -   Secondary lateral meristem is also called marginal meristem.

### Primary Lateral Meristem

The examples of primary lateral meristem are:

-   Intra fasicular cambium

    -   Intra fasicular cambium is present within the vascular bundle.

    -   Intra fasicular cambium is present in dicot stem.

### Marginal Meristem

-   Location:

    -   Margins of leaf blade

-   **Function:**

    -   Marginal meristem expand the leaf surface.

    -   Marginal meristem do not expand the thickness.

Meristem on the basis of function 
=================================

-   The meristem were classified on the basis of function by
    Hanberlandt.

-   Hanberlandt calssified meristem on the basis of function in $1914$ .

Protoderm
---------

-   **Location:** Protoderm is the outermost meristematic tissue.

The function of protoderm are:

-   Protection from mechanical injury.

-   Protoderm gives rise to epidermis layer.

Procambium
----------

-   **Location:** Procambium is the innermost meristematic tissue.

The function of procambium are:

-   Procambium transports water and nutrition

-   Procambium gives rise to vascular tissues.

    -   Vascular tissues are xylem and phloem.

Ground meristem
---------------

The function of ground meristem are:

-   Ground meristem gives rise to the following components in monocot
    plants:

    -   Hypodermis

    -   Ground Parenchyma

-   Ground meristem gives rise to the following components in dicot
    plants:

    -   Cortex

    -   Endodermis

    -   Pericycle

    -   Pith

Plane of division
=================

The types of meristematic tissues on the basis of plane of division are:

-   Rib meristem

-   Mass meristem

-   Plate meristem

Rib Meristem
------------

-   These cells divide only in one plane.

    -   The division in only one plane is called anticlinal division.

### Function of rib meristem

The function of rib meristem are:

-   Rib meristem plays a role in young stem to develop:

    -   Pith

    -   Cortex

Mass meristem
-------------

-   These cells divide in three planes.

Examples of mass meristem are:

-   Early embryo

### Function of mass meristem

The function of mass meristem is:

-   Mass meristem plays a role in:

    -   early development of endosperm

    -   early development of cortex

    -   early development of pith

Plate meristem
--------------

-   Plate meristems divide in two planes.

### Function of plate meristem

The function of plate meristem are:

-   Plate meristem develops leaves.

    -   The development of leaves by plate meristem doesnot increase the
        thickness.

Root Apical Meristem $R.A.M.$ 
=============================

-   Root apical meristem is also known as root apex.

-   **Structure:** Root apical meristem has inverted cup like structure.

-   **Content:** Root apical meristem is a group of initial cells.

-   **Origin:** Root apical meristem is embryonic in origin.

-   **Location:**

    -   Root apical meristem is present at the sub terminal region of
        growing root tip.

    -   Root apical meristem lies below the root cap.

    **Function:**

    -   Root apical meristem produces the tissues of primary root.

Histogen theory for $R.A.M.$ 
----------------------------

-   Histogen theory was given by Hanstein.

-   Histogen theory was given in $1868$ .

### Histogens

-   The cell initiating region of root apical meristem are called
    histogens.

### Components of Histogen

-   The histogens are:

    -   dermatogen

    -   periblem

    -   plerome

### Role of histogens

-   Histogen forms components present in a mature root.

-   The role of histogen is described as:

    -   Dermatogen forms the epidermis.

    -   Periblem forms the ground tissue.

        -   cortex

        -   endodermis

        -   pith

        -   pericycle

    -   Plerome forms the vascular cylinder.

        -   xylem

        -   phloem

### Dermatogen of monocot

The following condition is seen in monocot plants:

-   Dermatogen generates only the root cap in monocots.

    -   The dermatogen of monocot is called calyptrogen.

### Dermatogen in dicot

The following components are generated by dermatogen in dicot plants:

-   Dermatogen generates protoderm in dicots.

-   Dermatogen generates root cap in dicots.

    -   The dermatogen of dicot is called dermatocalyptrogen.

### Features of monocot root

-   **Quantity of Xylem:** The quantity of xylem groups vary from 8
    to 20.

-   **Role of pericycle:** Pericycle gives rise to lateral roots only.

-   **Contents:** Cambium is absent.

-   **Nature of growth:** There is no secondary growth.

-   **Nature of pith:**

    -   The pith is larger.

    -   The pith is well developed.

Quiescent Center
----------------

-   **Location:** Quiescent center is found in root promeristem.

-   **Discovery:**

    -   Quiescent center was discovered by Clowes.

    -   Quiescent center was discovered in $1956-1958$ .

    -   Quiescent center was discovered in the root tip of *Zea Mays* .

-   **Shape:**

    -   Closed meristem

        -   Quiescent center is hemispherical in closed meristem.

    -   Open meristem

        -   Quiescent center is disc shaped in open meristem.

-   **Nature of cells:** Cells in quiescent center are inactive.

-   **Division of cells:** Cells in quiescent center do not divide.

Shoot Apical Meristem 
=====================

Function of shoot apical meristem
---------------------------------

-   Shoot apical meristem produces lateral organs.

-   The lateral organs produced by shoot apical meristem are:

    -   leaves

    -   branch

    -   flowers

Tissue Zones
------------

The tissue zones of shoot apical meristem are the meristematic tissues
divided on the basis of function.

Histogen Theory for shoot apical meristem
-----------------------------------------

-   The histogen theory was given by J. Hanstein.

-   The histogen theory was given in $1868$ .

### Statement of histogen theory

-   The plant body doesnot originate from a single superficial cells.

-   The plant body originates from mass of meristematic cells.

-   The meristematic region contain three distinct zones.

-   The distinct zones of meristematic region are called histogens.

### Histogen

The histogens proposed by Hanstein are:

-   **Dermatogen:**

    -   *Location:* Dermatogen is the outermost layer.

    -   *Function:* Dermatogen gives rise to the epidermis.

-   **Periblem:**

    -   **Location:** Periblem lies inner to dermatogen.

    -   **Function:** Periblem gives rise to cortex.

-   **Plerome:**

    -   *Location:* Plerome lies at the center.

    -   *Function:* Plerome gives rise to

        -   vascular tissue

        -   endodermis

### Rejection of histogen theory

The reasons for rejection of histogen theory are:

-   There is no strict zonal differentiation between histogens.

-   Dermatogen , Periblem and Plerome cannot be distinguished from each
    other.

Tunica Corpus Theory
--------------------

-   Tunica Corpus Theory was proposed by Schmidst.

-   Tunica Corpus Theory was proposed in $1924$ .

### Statement of tunica corpus theory

-   There are two distinct tissue zones in the apical region of the
    shoot.

-   The distinct tissue zones are:

    -   Tunica

    -   Corpus

### Tunica

-   **Size of cells:** The cells of tunica are smaller than corpus.

-   **Nature of division:** The cells of tunica show anticlinal
    division.

-   **Contents:** Tunica contains one or more peripheral layer of cells.

-   **Functions:**

    -   Tunica produces :

        -   cortex

        -   endodermis

        -   pericycle

        -   vascular tissue

        -   pith

    -   Tunica increases surface area.

### Corpus

-   **Size of cells:** The cells of corpus are larger than tunica.

-   **Contents:** Corpus contains inner mass of cells.

-   **Functions:**

    -   Corpus produces tissues that are not produced by tunica.

    -   Corpus increases the volume.








Introduction
============

Origin
------

-   **Ground Tissue System** originates from **ground meristem**

Structure
---------

-   **Ground Tissue System** *histologically* is made up of:

    -   *Parenchyma*

    -   *Collenchyma*

    -   *Sclerenchyma*

Structure in plant parts
------------------------

-   **Ground** tissue system is present in leaf as:

    -   **Pallisade**

    -   **Spongy Mesophyll Cells**

-   These **tissues** are *parenchymatous* .

Arrangement in dicot and monocot
--------------------------------

-   **Monocot** system consists of:

    -   **Hypodermis**

    -   **Ground Parenchyma**

-   **Dicot** system consists of:

    -   **Hypodermis**

    -   **Cortex**

    -   **Endodermis**

    -   **Pith**

    -   **Medullary rays**

Anatomical Location
-------------------

-   **Ground tissue system** is present *inner* to the **epidermis** and
    **stele** .

Function
--------

-   **Ground** tissue system:

    -   fills the **gap** in between.

    -   provides **mechanical** strength.

Types of Ground Tissue System
=============================

Extra Stelar Region
-------------------

-   In **extra** stelar region ground tissue are categorized into:

    -   Hypodermis

    -   Cortex

    -   Endodermis

    -   Pericycle

-   In **intra** stelar region ground tissue are categorized into:

    -   Pith

    -   Medullary rays

    -   Conjuctive Tissue

Description of Extra Stelar
===========================

Hypodermis
----------

-   **Hypodermis** lies inner to *epidermis* .

-   It is made up of cells of type:

    -   **collenchymatous**

    -   **sclerenchymatous**

-   Hypodermis provides **mechanical strength.**

Cortex
------

-   **Location:** It lies inner to *hypodermis* .

-   **Structure:** It is made up of **parenchymatous** cells.

-   **Content:**

    -   Starch grains

    -   Resin ducts

    -   Oils

    -   Tanins

    -   Lactiferous ducts

-   **Function:**

    -   Storage

    -   Photosynthesis

    -   Protection

    -   Water pumping to inner plant parts.

    Endodermis
    ----------

    -   **Structure:** It consists of cells of type :

        -   Single Layered

        -   Barell shaped

        -   **Living**

    -   **Anatomical Structure:**

        -   Only in **roots** there is presence of:

            -   Band of **suberin** .

            -   This band is present at the *lateral* and *tangential*
                wall.

            -   This band was discovered by **Caspary** .

            -   It was discovered on **1866** .

            -   This band is also called **casparian strips** .

    -   **Function:**

        -   Checks the entry of **water.**

        -   Checks the entry of **air** in parts of xylem.

        -   Acts as *internal* **protective tissue.**

Pericyle
--------

-   **Location:** Between **endodermis** and **vascular strands** .

-   **Type of cells:**

    -   *Stem:* **parenchymatous** or **sclerenchymatous** .

    -   *Root:* **parenchymatous**

-   **Function:** Acts as **outer vascular** strands.

Description of intra stelar
===========================

Pith
----

-   **Types of cells:** Parenchymatous cells without **chlorophyll** .

-   **Contents:** *Tanin* and *Crystals* .

-   **Function:** *Storage* of food.

Medullary Rays
--------------

-   **Location:** Between vascular bundles.

-   **Anatomical Structure:** *Narrow* parenchymatous *strips*.

-   **Function:**

    -   **Meristematic medullary rays:** Formation of interfasicular
        cambium.

    -   **Parenchymatous medullary rays:** Transportation of **food**
        and **water** from *pith* to *cortex* .

Conjuctive Tissue
-----------------

-   **Background:** *Xylem* and *Phloem* are present in separate
    *patches* .

-   **Location:** Between **patches** of **vascular tissue.**

-   **Structure of cells:** Parenchymatous


Vascular tissue
===============

-   The other name for vascular tissue is fasicular tissue.

-   Vascular tissue is defined as:

    -   central cylinder of root and shoot

    -   This system is surrounded by ground tissue system

    -   The ground tissue system may be pericycle pith

    -   Stele is the central cylinder of this structure.

Vascular bundles
================

-   Vascular bundles are tissue system

The location for vascular bundles is: - vascular bundles are distributed
in the stele.

Origin of vascular bundle
-------------------------

Vasuclar bundles are originated from procambium.

Location of procambium
----------------------

Procambium lies under primary meristem.

Parts of vascular bundles
=========================

The parts of vascular bundles are - Xylem - Pholem - Cambium

Function of vascular bundle
===========================

The function of vascular bundle are:

-   Vascular bundles conduct water from root to leaves

-   Vascular bundles conduct nutrients from root to leaves

-   These materials such as root and nutrients are carried by xylem

-   The vascular tissues translocate food prepared in the leaves

-   This translocation is done by pholem.

Open and closed vascular bundle
===============================

Open vascular bundles
---------------------

-   The cambium is present between the xylem and pholem.

-   This types of vascular bundle is called open vascular bundle.

-   The cambium of open vascular bundle forms the sencondary xylem and
    pholem tissue.

Closed vascular bundle
----------------------

-   There is the absence of cambium in closed vascular bundle

-   The plants containing closed vascular bundles don not form the
    seondary growth

### Examples of closed vascular bundles

-   Closed vascular bundles is present in monocots.

Arrangement of vascular bundles
-------------------------------

The types of vascular bundles on the basis of arrangement are - radial
vascular bundles - conjoint vascular bundles

Radial vascular bundles
=======================

-   There is the presence of xylem and pholem in radial vascular bundle

-   Xylem are arranged alternatively in different bundles.

-   Phloem are arranged alternatively in different bundles with xylem.

-   The arrangement of xylem and phloem is present on different radii.

Quantity of radial vascular bundle
----------------------------------

Dicots : - Dicot plants have 2-6 radial vascular bundle - They are also
called as di arch to hex arch

Monocots: - Monocot plants have 8-20 radial vascular bundles - They are
also called polyarch

Conjoint vascular bundles
-------------------------

Types of conjoint vascular bundles Conjoint vascular bundles are of
three type: - colateral - bicollateral - concentric

### Collateral

-   There is the presence of xylem and phloem.

-   Phloem is present in the outer side

-   Xylem is present in the inner side

### Example of collateral:

-   sunflower

Bicollateral
------------

-   There is the presence of phloem and xylem

-   Phloem is present on both sides of xylem

-   There is a presence of vascular cambium

-   The anatomy of the vascular cambium is a strip

-   The vascular cambium strip is present at the outer sides of the
    xylem

-   The vascular cambium strip is present at the inner sides of the
    xylem

Examples of bicollateral xylem
------------------------------

-   Stem of cucurbita

-   Solanaceace family

Concentric
==========

Concentric vascular bundle have xylem and phloem The phloem surrounds
the xylem or the xylem surrounds the phloem One of them forms the
central core The other of them surrounds the central core

Types of concentric vascular bundles
====================================

Concentric vascular bundles are of two types

-   Amphivasal

-   Amphicribal

Amphicribal
===========

-   The amphicribal vascular bundles are also called hadrocentric

-   The xylem forms the central core

-   The phloem surrounds the central core of the xylem

Examples of amphicribal vascular bundles
----------------------------------------

-   ferns

-   aquatic angiosperms

-   dicots

Amphivasal
==========

-   The other term for amphivasal is leptopcentric

-   the phloem forms the central core.

-   The xylem surrounds the phloem in amphivasal

Examples of amphivasaal
-----------------------

-   Dracena

-   Yucca

Types of xylem
==============

-   The xylem can be divided in two types.

-   This division can be done on the basis of development

-   The types of xylem are

-   Protoxylem

-   Metaxylem

Protoxylem
----------

-   Protoxylem is the first formed xylem

-   The protoxylem contains trachieds and vessles.

-   There is the presence of lumnes in trachieds and vessels of
    protoxylem.

-   The lumen of tracheids and vessels of proto xylem is narrow.

-   This narrowness results in the implication of smaller size of proto
    xylem

Character of Wall thickening in protoxylem
------------------------------------------

-   The wall thickening character in protoxylem may be

-   anuular

-   spiral

Metaxylem
---------

-   Metaxylem are formed later.

-   The metaxylem have presence of trachieds and vessels.

-   There is the presence of lumen in the trachieds and vessles.

-   The lumen in the trachieds and vessels of the meta xylem is larger
    in size.

-   This implies that the metaxylem have a large size

Character of wall thickneing of metaxylem
-----------------------------------------

-   The wall thickening character of metaxylem can be described as:

-   reticliulate

-   scaliform pitted

-   simple

-   bordered

Division on the basis of position
---------------------------------

Xylem are also divided on the basis of position

The types of xylem omn the basis of position are: - exarch - endarch -
mesarch - centarch

Exarch
------

-   There are protoxylems and metaxylems in exarch

-   The presence of protoxylem is towards the periphery in exarch.

-   The presence of metaxylem is towards the center in exacrch

Location of exacrch xylem
-------------------------

The exarch xylem are present in the - roots of monocot plants - roots of
dicot plants

Endacrh
-------

-   There is the presence of protoxylem and metaxylem at endaarch

-   The protoxylem are located at the center in endarch.

-   The metaxylem are located at the periphery in endarch

Loaction of endarch
-------------------

Endarch xylem s are present in - stem of monocot plants - stem of dicot
plants

Mesarch
-------

-   There is the presence of protoxylem and metaxylem in mesarch

-   The protoxylem is situated in between the metaxylem

Location of mesarch
-------------------

-   Mearch xylem is present in

-   Pteridophytes The pteridophytes which have mesarch xylem are ferns

Centacrh
--------

-   There is the presence of protoxylem and metaxylem in centrach

-   The protoxylem are located at the center

-   The metaxylem surrounds the protoxylem

Location of centarch
--------------------

-   Centarch is present in pteridophytes

-   The pteridophytes having the presence of centarch are sellagenaella




# Division of internal structure of monocot root

The division of internal structure of monocot root is 

1. Epidermis
2. Cortex
3. Stele


# Terms for epidermis

The other term for epidermis is expressed as

- Rhizodermis


The other term for epidermis is expressed as


- Epiblema


The other term for epidermis is expressed as


- Piliferous layer



## Function of epidermis

- Epidermis absorbs water from the soil.
- Epidermis absorbs minerals from the soil.


##  Anatomy of epidermis

- Epidermis is uniseriate.


## Histology of epidermis



The types of cells in epidermis are 


- Tubular



##  Limitations of epidermis

- There is no intercellular space in epidermis.
- There is no stomata in epidermis.


## Meaning of Multiseriate epidermis

-  Multiseriate epidermis stands for multi layered epidermis


## Location of multiseriate epidermis

Multiseriate epidermis is found in


- Aerial root of orchids
    + The location of multiseriate epidermis in aerial roots is the velamen.
- Epiphytic aroids




# Cortex

# Location of cortex

The location of cortex is


- Below the epidermis


## Histology of cortex

The types of cells in cortex are

- Parenchymatous

- There is no presence of intercellular space between cells of cortex.



## Division of cortex
The cortex is divided into


- Exodermis
- Endodermis



## Exodermis

### Location of exodermis

The exodermis is present in 

-   Old root of *Zea Mays*

### Formation of exodermis

The exodermis is formed by 

- Suberization

###  Function of exodermis

- Exodermis protects internal tissues.




## Endodermis

The cells of endodermis are

- Thin walled
- Living 
- Barrel shaped
- Single layer

### Location of casparian strips

- Caparian strips is located at the cells of endo dermis
- Casparian strips is located at the anticlinal wall of endo dermis.

### Location of passage cells

The passage cells are located 

- Outside the protoxylem

### Feature of passage cells

- Passage cells donot have casparian strips.

### Function of endodermis

- Endodermis controls the movement of fluid.
- Endodermis controls the movement of air.
- Endodermis transports water.
- The water is transported from epiblema to inner tissues.


# Stele

## Division of stele

The division of stele is expressed a

- Pericycle
- Vascular tissue
- Pith

## Pericycle

### Anatomy of pericycle

- Pericycle is uniseriate.

### Location of multiseriate pericycle

 The multiseriate pericycle are located at

- *Graminae*
- *Similax*
- *Agave*
- *Dracaena*
- *Palms*



### Histology of pericycle

- The cells of pericyle are parenchymatous.

### Functions of pericycle

- Pericycle stores food.
- Pericycle originates lateral roots.



## Vascular tissue



### Nature of vascular bundle in monocot roots

The vascular bundle in monocot roots is 
- Radial Vascular bundle

### Number of vascular bundle in monocot roots

The number of vascular bundle in monocot roots is

- \( 8 to 20 \) 

#### Term for number of vascular bundles in monocot roots

The other term for number of vascular bundles in monocot roots is


- Polyarch


### Type of xylem in monocot root
 The xylem in monocot root is

 
 - Exarch
 

### Content of xylem in monocot root

The xylem in monocot root contains


- Xylem Parenchyma
- Vessels
    + The shape of vessels in xylem of monocot root is
        + rounded
        + oval


### Function of xylem in monocot root

- Xylem provides mechanical strength.
- Xylem conducts water.
- Xylem conducts minerals.


### Location of phloem in monocot root

The location of phloem in monocot root is


- Periphery of vascular cylinder


### Content of phloem in monocot root
 
The contents of phloem in monocot root are

- Seieve Tubes
- Companion Cells
- Phloem Parenchyma

### Function of phloem in monocot root

- Phloem conducts food.


## Pith

### Histology of pith in monocot root
The cells present in the pith of monocot roots are


- Parenchymatous


### Shape of cells in pith in monocot root

The shape of cells in pith of monocot roots is 

- Angular
- Rounded   

### Location of sclerenchymatous cells in pith of monocot root

The sclerenchymatous cells in pith of monocot root are located at 
- Canna


### Function of pith

Pith stores food.




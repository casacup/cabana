# Permanent tissues

### Age of permanent tissues

Mature

### Process of origin of permanent tissues

Differentiation

### Source of origin of permanent tissues 

Meristematic tissues

### Divisibility of permanent tissues

No power of division at normal condition 

### Life of permanent tissues

Living or Dead

### Approximation of Thickness of walls of permanent tissues

Thick walled or thin walled

# Types of permanent tissues


- Simple
- Complex


### Number of Types of permanent tissues 

2

# Simple permanent tissues

### Number of division of simple permanent tissues

3

### Divisions of simple permanent tissues



- Parenchyma
- Collenchyma
- Sclerenchyma


# Complex permanent tissues

### Number of division of complex permanent tissues

2

### Divisions of complex permanent tissues


- Xylem
- Phloem



# Parenchyma 


### Parent of words "parenchyma" 


- Para
- En-chien


### Country of parent word of parenchyma  

Greece

### Meaning of "para" in parenchyma

Beside

### Meaning of "en- chien" in parenchyma 

To pour


### Spatial arrangement in parenchyma

Intercellular space present

### Approximation of Thickness of wall of parenchyma

Thin walled


### Presence of life in parenchyma

Living

### Compositing materials of cell wall of parenchyma


- Cellulose
- Calcium pectate


### Structure attaching parenchymatous cells


- Plasmodesmata


### Function of plasmodesmata in parenchymatous cells

Attach parenchymatous cells

### General representation of shape of parenchymatous cells

Polyhedral

### List of shapes exhibited by parenchymatous cells


- Spherical
- Oval
- Cylindrical
- Rectangular
- Stellate
- Spindle


# Types of parenchymatous tissues


### Number of divisions of parenchymatous tissues


5

### Divisions of parenchymatous tissues


- Prosenchyma
- Aerenchyma
- Stellate parenchyma
- Chlorenchyma
- Storage parenchyma


# Prosenchyma


### Approximation of Thickness of wall of prosenchyma 

Thick

### Cause of  thick wall of prosenchyma 


- Deposition of Cellulose




### Approximation of length of prosenchyma

Elongated

### Shape of ends of prosenchyma

Pointed

### Functions of prosenchyma 

- Mechanical support
- Protection
- Conduction


### Examples of prosenchyma 

Pericycle

# Aerenchyma


### Spatial arrangement in aerenchyma

Presence of intercellular spaces

### Contents present at the intercellular spaces of aerenchymatous cells 

Air

### Function of aerenchyma 


- Circulation of air
- Provide buoyancy


### Examples of aerenchyma 

Cortex of

- Hydrilla
- Nymphea
- Eichhornia


# Stellate parenchyma

### Shape of spatial arrangement in stellate parenchyma


Star

### Component in intercellular space in stellate parenchyma

- Short arms
- Long arms

- Air space



### Examples of stellate parenchyma 



- Petiole of Banana
- Petiole of Canna


# Chlorenchyma

### Distinct cellular organell present in chlorenchyma

Chloroplast

### Function of chlorenchyma 


- Photosynthesis


### Examples of chlorenchyma 


- Pallisade parenchyma
- Spongy parenchyma


# Storage parenchyma


### Function of storage parenchyma 


- Store reserve food material


### Substances stored by storage parenchyma 


- Sugar
- Amide
- Protein granules
- Oil drops


### Examples of storage parenchyma 


- Endosperm of Seeds
- Pulp of fruits


# Function of parenchymatous tissues 




### Function of idoblastic cells present in parenchymatous tissues


- Secretion


### Substances secreted by idioblastic cells present in parenchymatous tissues 


- Resin 
- Latex
- Tanin
- Oils


# Modification of parenchymatous tissues 


### List of modification of parenchymatous tissues


- Cuticle
- Xylem parenchyma



### Components of cuticle in parenchymatous tissues 

Cutin

### Location of deposition of cutin in cuticle of parenchymatous tissues


Tangential Wall

### Function of cuticle in parenchymatous tissues 



- Protect inner tissue
- Reduce Rate of Transpiration


# Collenchyma

### Parent words of collenchyma


- Colla
- enchyma


### Country of origin of parent word of collenchyma 

Greece

### Meaning of "colla" in parent word of collenchyma

Glue

### Meaning of "enchyma" in parent word of collenchyma

An infusion

### Presence of life in collenchyma


Living

### Approximation of thickness of cell walls in collenchyma

Thickened

### Cause of thickness of cell walls in collenchyma


- Presence of
    + Pectin
    + Cellulose
    + Hemi cellulose



### Function of thickening components in cell walls of collenchyma


- Hold Water


### Elasticity of collenchyma 

Plastic

### Spatial arrangement in collenchyma

Compact

### Shape of end of cells of collenchyma

Oblique

### Approximation of length of cells of  collenchyma

Elongated

### General location of collenchyma


- Hypodermis of dicotyledonous stem
- Leaves


### Part of the plant body exhibiting absence of collenchyma 

Roots

### Types of plant on the basis of primary division exhibiting absence of collenchyma 

Monocots



# Types of collenchyma

### Number of division of types of collenchyma

3


### Division of types of collenchyma 


- Plate
- Angular
- Lacunate


# Plate collenchyma 


### Term for plate collenchyma

Lamellae collenchyma

### Location of deposition of thicknening material at the cell wall of plate collenchyma

Tangential Wall

### Examples of plate collenchyma 

Hypodermis of sunflower stem


# Angular collenchyma



### Location of deposition of thickening material at the wall of angular collenchyma 

Angle of Cells

### Examples of angular collechyma 



- Stems of 
    + Tagetus
    + Tomato
    + Datura
    + Potato



# Lacunate collenchyma


### Location of deposition of thickening material at the wall of lacunate collenchyma 


- Wall bordering the intercellular space


### Examples of lacunate collenchyma


- Hypodermis of cucurbita stem



# Functions of collenchymatous tissues


- Tensility
- Support



# Sclerenchyma

### Parent words of sclerenchyma


- Sclerous


### Country of origin of  parent words of sclerenchyma

Greece


### Meaning of "sclerous" in parent word of sclerenchyma

Hard

### Meaning of "enchyma" in parent word of sclerenchyma

An infusion

### Approximation of thickness of cell walls of cells present in sclerenchyma

Extremely thick


### Cause of extreme thickening in cell walls of cells present in sclerenchyma

Uniform deposition of lignin


### Presence of life in cells of sclerenchyma

Dead


### Cause of death of cells of sclerenchyma

Deposition of impermeable secondary walls


### Sclerotic parenchyma

- Transitory form of sclerenchyma
- Have Protoplasm
- But Lignified Walls


### Presence of life in sclerotic parenchyma

Living

### Thickening material present at the walls of sclerotic parenchyma

Lignin

 
### Thickening material present at the walls of unlignified cells of sclerenchyma

Suberin


# Types of sclerenchyma

### Number of types division of sclerenchyma

2

### Division of types of sclerenchyma


- Fibres
- Sclerids


# Fibres

### Approximation of length of fibres

Long

### Shape of ends of fibres

- Pointed 
- Rounded

### Approximation of thickness of fibres

Thick walled

### Cause of thickness of fibres


- Deposition of 
    + Liginin
    + Cellulose
    + Gelatinous material


### Shape of fibres

Polygonal

### Cause of narrowness of lumen of fibres

Heavy deposition of secondary wall

### Types of pits present at the walls of fibres



- Simple
- Oblique


### List of plants yielding longest fibres


- Linum usitatissimum
- Corchorus
- Cannabis


### General location of fibres in parts of plants


- Hypodermis 
- Pericycle
- Secondary xylem
- Secondary phloem


### Sources of origin of cells of fibres 


- Procambium
- Cambium
- Ground Meristem



# Types of fibres

### Number of division of types of fibres

3


### Division of types of fibres 


- Surface 
- Wood 
- Bast



# Surface fibres


### Location of surface fibres


- Surface of plant organs


### Examples of surface fibres 


- Cotton fibres found in testa of seeds
- Mesocarp
- Fibres of coconut


# Wood fibres 


### Location of wood fibres


- Secondary xylem


### Source of origin of wood fibres


- Vascular cambium


## Types of wood fibres


### Number of division of types of wood fibres

2

### Division of types of wood fibres


- Libiform fibres
- Fibre tracheids 



## Libiform fibres


### Approximation of length of libiform fibres 

Long

### Approximation of thickness of libiform fibres

Thick

### Types of pits present at libiform fibres 

Simple

## Fibre tracheids 

### Approximation of length of fibre tracheids

Short

### Approximation of thickness of fibre tracheids

Thin

### Types of pits present at fibre tracheids 

Bordered 

# Bast fibres


### Location of bast fibres 


- Pericycle
- Phloem


### Term for bast fibres

Extraxylary fibres

### Examples of bast fibres 



- Cannabis sativa
- Linum usitatissiumum
- Corchorus capsularis
- Hibiscus cannabinus


# Function of fibres


- Mechanical strength


# Sclereids

### Approximation of thickness of wall of sclerids

Thick

### Cause of thickness of walls of sclerids



### Cells giving origin of sclerids  

Parenchymatous cells

### Process of origin of sclerids in parenchymatous cells 

### General shapes of sclerids  

### Approximation of length of sclerids compared to fibres 

Shorter than fibres

### General location of sclerids in plants 


- Hard endocarp of almond
- Hard endocarp of coconut
- Hard Seed coats
- Regions of cortex , pith as in Nymphea
- Pulp of fruits


# Types of sclerids


- Branchy sclerids
- Macrosclerids
- Osteosclerids
- Astrosclerids
- Trichosclerids



# Branchy sclerids


### Term for branchy sclerids


Stone cells

### Approximation of size of branchy sclerids 

Small

### Shape of branchy sclerids 

Isodiametric

### Location of branchy sclerids 


- Cortex
- Pith
- Phloem
- Pulp of fruits


# Macrosclerids

### Term for macrosclerids 

Rod cells

### Shape of macrosclerids

Rod 

### Approximation of length of macrosclerids

Elongated

### Location of macrosclerids 


- Leaves
- Cortex of stem
- Outer seed coat


# Osteosclerids

### Term for osteosclerids 

Bone cells

### Shape of osteosclerids


- Bone 
- Barrel


### Location of osteosclerids 


Hakea

# Term for  Astrosclerids

Stellate cells

### Shape of astrosclerids


- Stellate
- Star


### Location of astrosclerids 


Leaf of Nymphea

# Term for Trichosclerids

Internal Hairs

### Shape of trichosclerids

Hair

### General location of trichoslcerids 


- Intercellular space of leaves of some hydrophytes
- Intercellular space of stem of some hydrophytes


# Function of sclerids


- Mechanical support




# Complex Permanent Tissues


### Number of cells present in complex permanent tissues 


- More than one


# Types of complex permanent tissues 



# Xylem

### Parent word of xylem

Xylos

### Country of origin of parent word of xylem

Greece

### Meaning of "xylos" 

Wood

# Components of xylem


### Number of divisions of components of xylem
4


### Division of components of xylem


- Trachieds
- Vessels
- Xylem fibres
- Xylem parenchyma



# Absence of components of xylem 


## Types of plants exhibiting absence of vessels


- Pteridophytes
- Gymnosperms


### List of pteridophytes having the presence of vessels


- Selaginella
- Equisetum
- Pteridium


### List of gymnosperms having the presence of vessels 


- Gnetalls
- Ephedra
- Gnetum


## Types of plants exhibiting the absence of trachieds 


- Hydrophytic angiosperms



# Function of xylem


- Conduct water
- Provide mechanical support


# Trachieds

### Anatomy of trachieds of xylem

Tube

### Approximation of length of xylem

Elongated

### Presence of life in trachieds 

Dead

### Shape of ends of trachieds 

Tapering

### Substance present at the cell wall of cells of trachieds 

Lignin

### Source of development of primary xylem

Procambium

### Source of development of secondary xylem

Vascular cambium

## Wall thickening at trachieds


### Number of types of division of wall thickening characteristics of trachieds

5


### Division of wall thickening characteristics of trachieds


- Annular
- Spiral
- Sclariform
- Reticulate
- Pitted

### Shape of annular wall thickening characteristics of traachieds

Ring

### Shape of spiral wall thickening characteristics of trachieds

Helix

### Shape of sclariform wall thickening characteristics of trachieds

Ladder

### Shape of reticulate wall thickening characteristics of trachieds

Network

## Pits 

### Number of types of pitted wall thickening characteristics of trachieds

2

### Types of pitted wall thickening characteristics of trachieds


- Simple 
- Bordered

### Approximation of thickening of pits

Unthickened


### Location of pits in plant cells


Inner wall


### Condition of presence of thickening at simple pits

Absent


### Condition of presence of thickening at bordered pits

Present at borders 





# Vessels

### Approximation of length of vessels at xylem

Elongated

### Anatomy of vessels at xylem

Tube

### Term for cells of vessels of xylem

Vessel members


### Structure absent in vessels of xylem

Transverse septum

### Cause of absence of transverse septum in xylem

Dissolution

### Arrangement of cells in vessels of  xylem


- Rows of cells placed one over other


### Term for arrangement of cells in vessels of xylem

Syncytes

### Substances present at the thickening of vessels of xylem

Lignin

### Presence of life in vessels in xylem

Dead

### Approximation of width of lumen of vessels in xylem

Wide

### Cause of wide lumen of vessels in xylem 

Absence of transverse septum

### Structure present at the end walls of vessels of xylem

Perforation plate




# Xylem fibre


### Term for xylem fibres

Wood fibres

### Presence of life at xylem fibres

Dead

### Function of xylem fibres

-   Mechanical strength


### Substance present at the walls of xylem fibres

Lignin



### Shape of ends of xylem fibres


Pointed


### Source of permanent tissue for the formation of xylem fibres 

Sclerenchyma


# Types of xylem fibres



### Number of types of division of xylem fibres

2

### Division of types of xylem fibres


- Libiform fibre
- Fibre Trachieds


### Terms for libiform fibres of xylem fibres 


- True fibres
- Real fibres


### Approximation of length of libiform fibres of xylem fibres

Elongated

### Approximation of dimension of ends of libiform fibres of xylem fibres

Narrow

### Function of narrow ends of libiform fibres of xylem fibres

Durability of wood

### Type of pit present at the libiform fibres of xylem fibres

Simple

### Type of pit present at fibre trachied of xylem fibres

Bordered

### Function of fibre trachied of xylem fibres 

Support

# Xylem parenchyma


### Substance present at the wall of xylem parenchyma 

Cellulose

### Presence of life at xylem parenchyma

Living

### Function of xylem parenchyma  

Food storage


### Approximation of thickness of walls of xylem parenchyma in secondary xylem

Thick

### Substance present at the wall of xylem parenchyma of secondary xylem 

Lignin


# Types of xylem parenchyma


### Number of types of division of xylem parenchyma

2

### Division of types of xylem parenchyma

- Axial parenchyma
- Radial parenchyma
 

### Arrangement of  of axial parenchyma of xylem parenchyma in plane

Vertical

### Arrangement of radial parenchyma of xylem parenchyma in plane

Horizontal

### Term for radial parenchyma of xylem parenchyma 

Xylem ray

# Phloem


### Person coining the term phloem

Nagelli

### Function of phloem in plant


- Transport


### Substances transported by phloem in plant 


- Sugars
- Amino acids
- Micronutrients
- Hormones


### Directions of movement of food by phloem 


Bidirectional

### Presence of life at cells of phloem 


Living

# Components of phloem

### Number of division of components of phloem

4

### Division of components of phloem


- Sieve tubes
- Companion cells
- Phloem parenchyma
- Phloem fibres



# Sieve tubes





### Approximation of length of cells of sieve tube in phloem 


Elongated

### Approximation of thickness of cells of sieve tube of phloem

Thin wall

### Anatomy of cells of sieve tubes of phloem

Tube


### Pattern of arrangement of sieve tubes of phloem

Longitudinal

### Cell organell absent at the sieve tubes of phloem

Nucleus

### Perforation plate of sieve tubes of phloem 

Sieve plate

### Shape of transverse walls of sieve tubes of phloem

Oblique

### Number of sieve areas present at simple sieve plate of phloem

Single

### Number of sieve areas present at the compound sieve plate of phloem

Multiple


### Controller of activities of sieve tubes of phloem  

Nucleus of companion cells

### Arrangement of cells present at the sieve tubes of phloem

Fusion

### Time of formation of callose phloem

Winter

### Solubility of callose in winters

Insoluble

### Solubility of callose in spring of phloem

Solubule

### Callose in sieve tubes of phloem 

Carbohydrate pad

### Function of callose in phloem


- Protect seive tube


### Structure of sieve tubes absent in gymnosperms

Sieve plate


# Companion cells


### Term for companion cells in angiosperms of phloem

Companion cells

### Presence of life at companion cells of phloem

Living



### Approximation of thickness of walls of companion cells of phloem

Thin walled

### Location of companion cells of phloem


Sides of sieve tube

### Structure joining companion cells and sieve tubes 

Plasmodesmata

### Number of mother cells that originate companion cells and sieve tubes 

1

### Term for companion cells in gymnosperms of phloem 

Albuminous cells

### Type of permanent tissue at albuminous cells of phloem

Parenchymatous


### Term for albuminous cells of phloem 

Strasburger cells

# Phloem fibres

### Term for phloem fibres

Bast fibres

### Approximation of thickness of walls of phloem fibres

Thick walled

### Source of permanent tissue for the formation of phloem fibres

Sclerenchyma

### Nature of function of phloem fibres 

Mechanical


# Phloem parenchyma 

### Term for phloem parenchyma

Bast parenchyma

### Presence of life at the cells of phloem parenchyma

Living


### Approximation of thickness of wall of cells of phloem parenchyma

Thin walled

### List of materials present at phloem parenchyma


- Resin
- Latex
- Mucilage


### Function of phloem parenchyma


- Conduction

- Store food material


### Direction of travel of food in phloem parenchyma


Radial

### List of types of plants exhibiting the absence of phloem parenchyma 


- Monocots
- Herbaceous stem


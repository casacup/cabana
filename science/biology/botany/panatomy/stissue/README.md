# Special Permanent Tissues

### Aromatic plants

Produce aroma from stem , leaf

### List of aromatic plants


- Tulsi
- Lemon


### Scientific name of tulsi

Olimum tenuflorum

### Scientific name of lemon 

Citrus lemon

# Types of special permanent tissues

### Number of types of special permanent tissues

2

### Divisions of types of special permanent tissues 

- Glandular 
- Lactiferous


# Glandular tissue

### Nature of function of glandular tissues 

Secretory

# Lactiferous tissue 

### Homologousness of lactiferous tissue of plant with animals 

Blood vessels

# Lactifiers


### Substance secreted by lactifiers

Latex

### Substances present at latex

- Lipids
- Protein

### Source of origin of latex in lactifiers in cell organell

Endoplasmic reticulum

### Division of structures secreting latex



- Lactiferous cells
- Lactiferous vessels



### List of plants secreting latex from lactiferous cells

- Nerium
- Euphorbia
- Ficus

### List of plants secreting latex from lactiferous vessels 

- Papaver
- Poppy plant
- Opium

### Scientific name of opium 

Agremone mexicans

### List of colours of latex 

- Milky
- Transparent
- Yellow

# Uses of latex


### Industrial use of latex secreted by rubber plant 

Commercial rubber

### Scientific name of rubber plant

Hevea brasiliensis

### Industrial use of latex secreted by opium

Medicines

### Chemical property of morphine

Alkaloid

### Use of morphine  

Sedative

# Secretory tissues


### Function of secretory tissues

Secretion 


# Types of secretory tissues


### Number of types of secretory tissues in plants

2

### Division of types of secretory tissues in plants

- External glands
- Internal glands


# External glands


### Location of external glands

Surface of plants


# Types of external glands in secretory tissues



### Number of types of external glands in secretory tissues

5

### Division of types of external glands in secretory tissues



- Digestive 
- Nectar
- Salt
- Mucous
- Poison


# Digestive glands

### Types of plants having the presence of digestive glands 

Insectivorous plants
 
### Element lacking in plants having digestive glands 

Nitrogen

### List of plants having the presence of digestive glands


- Venous fly trap
- Pitcher plant
- Crimpsons pitcher plant
- Sundews
- Bladder worts

### Scientific name of Venus fly trap


Pionaea muscipula

### Scientific name of Pitcher plant


Napenthes alata

### Scientific name of Crimpsons Pitcher plant 


Sarracenia leucophylla

### Scientific name of Sundews


Dorosera capensis

### Scientific name of bladder worts 

Utricularia gibba



# Nectar glands


### Location of nectar glands in the plant body 


- Floral parts

### Terms for nectar glands

- Nectaries
- Nectarines

### Nectar in plants

Sugary solution



### Examples of nectar glands

- Kaner
- Lalupate

### Scientific name of Kaner


Nerium oleander

### Scientific name of Lalupate 


Euphorbia pulcherrima

# Salt glands


### Types of plants having salt glands

Halophytic

### Structure present at the salt glands

Trichomes

### List of plants having the presence of salt glands

- Bengal gram
- Mangrooves

### Location of salt glands the plant body of Bengal Gram

Seed coat

### Scientific name of Bengal gram 


Cicer arientinum

# Mucous glands


### List of plants having the presence of mucous glands 

- Hibiscus
- Okra

### Scientific name of Okra

Abelmoschus esculen

### Scientific name of Hibiscus 

Hibiscus rosa sinensis

### Part of plant body having the location of mucous glands in Hibiscus

Base of petals

### Part of plant body having the location of mucous glands in Okra 

Fruit


# Poison glands


### List of plants having poison glands 

- Scorpion grass


### Scientific name of scorpion grass

Myosotis Scorpioides

# Internal glands




# Types of internal glands

### Number of types of division of types of internal glands

5

### Division of types of internal glands


- Oil glands
- Chalk glands
- Essential oil glands
- Resin glands
- Gum  glands




# Oil glands


### List of plants having the presence of oil glands

- Lemon
- Orange

### Part of plant body having the location of oil glands in citrus fruits

- Peel of fruits

# Chalk glands


### Chemical compound present at chalks in chalk glands of plants 


- Calcium carbonate

### List of plants having the presence of chalk glands

- Tamarind

### Location of chalk glands at Tamarind

Cells in leaf

### Scientific name of Tamarind 

Tamarindus indica

# Essential oil glands



### List of plants having the presence of essential oil glands


- Eucalyptus


### Part of plant body having the location of essential oil glands in Eucalyptus 

Leaf

### Industrial use of essential oil of essential oil glands


- Manufacture of perfume



# Resin glands


### List of glands having the presence of resin glands


- Asafoetida ( Hing )
- Gymnosperms



### List of gymnosperms having the presence of resin glands


- Pinus
- Cedrus


### Scientific name of asafoetida 

Ferula assafoetida

# Gum glands


### List of plants having the presence of gum glands


- Babul
- Mango


### Scientific name of mango

Magnifera indica

### Scientific name of babul 

Acasia arebica

# Hyadathodes


### Term for hyadathodes

Water stomata

### Function of hyadathodes

Guttation

### Guttation in hyadathodes 

Secretion of water droplets

### State of hyadathodes in terms of closed and open 

Always open

### Location of hyadathodes at the parts of plant body 


- Margin of leaf
- Apex of leaf



### Types of plants having the presence of hyadathodes


- Herbaceous plant 
- Submerged aquatic plants 



### List of plants having the presence of hyadathodes



- Companula rotundifola 

-  Ranunculus fluitans


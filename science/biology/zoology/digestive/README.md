---
titlepage: true
title: "Digestive System"
date: 2022-02-26
author: "Biology"
---





Digestive system
================

-   The assemblage of organs associated with

    -   ingestion of food

    -   digestion

    -   absorption

    -   assimilation

    -   egestion




Components of digestive system
==============================

Digestive system consists of

-   alimentary canal

-   associated digestive glands

-   physiology of digeston.



Alimentary canal
================

-   Alimentary is a long

-   **Anatomy of alimentary canal**: Alimentary canal is complex coiled
    tube

-   **Location of alimentary canal**: Alimentary canal extends from
    mouth to anus.

-   **Length of alimentary canal:** The length of alimentary canal is

    -   ( 8-10 ) meters.



Part of alimentary canal
------------------------

Alimentary canal consists of following parts:

-   Buccal cavity

-   Pharynx

-   Oesophagus

-   Stomach

-   Small intestine

-   Large intestine

Mouth
=====



-   Mouth is a small and transverse aperture

-   **Location of mouth:**

    -   Mouth is present at face below the nose.



Contents of mouth
-----------------

Mouth consists of

-   vestibule

-   buccal cavity.

Vestibule
---------

-   Vestibule is slit like space .

-   Vestibule is externally bounded by lips  

-   Vestibule is laterally bounded by cheeks

-   Vestibule is internally the teeth and the gums.

Lips
----

-   There is the presence of two lips.

-   The depression in the upper lip is called philtrum.

Buccal cavity
=============

-   The another term for buccal cavity is oral cavity.

-   **Location of buccal cavity:**

    -   Buccal cavity is present between upper jaw and lower jaw.

Histology of buccal cavity
--------------------------

-   Buccal cavity is lined by mucus membrane.

Parts of buccal cavity
======================

-   Palate

-   Tonsils

-   Tounge

-   Teeth

Palate
======

Types of palate
---------------



The type of palate are:

-   anterior palate

-   posterior palate

Character of anterior palate
----------------------------

-   The anterior hard palate is bony

-   The anterior hard palate is made up of bones of:

    -   maxilla

    -   palatine bone

-   Anterior palate contains rugae.

Functions of anterior palate
----------------------------

-   Anterior palate separates buccal chamber from nasal chamber.

-   Rugae of anterior plate helps to hold food during mastication

Character of posterior palate
-----------------------------

-   Posterior palate is

    -   soft

    -   fleshy

    -   smooth

Functions of posterior palate
-----------------------------

-   The soft palate forms uvula

-   The other term for uvula is velum palatine.

-   **Function of uvula:**

    -   Uvula closes internal nostril during swallowing of food.

Tonsils
=======



Histology of tonsils
--------------------

-   Tonsils are lymphoid tissue.

General location of tonsils
---------------------------

-   Tonsils are located at

    -   pharynx  

    -   oral cavity

Types of tonsils
----------------

The types of tonsils are:

-   Nasopharyngeal tonsils

-   Tubal tonsils

-   Lingual tonsils

-   Palatine tonsils

Location of nasopharyngeal tonsils
----------------------------------

-   Nasopharyngeal tonsils are present in the nasopharynx.

Location of tubal tonsils
-------------------------

-   Tubal tonsils located posterior to the opening of Eustachian tube

Location of lingual tonsils
---------------------------

-   Lingual tonsils are situated on the posterior part of the tongue

Location of palatine tonsils
----------------------------

-   Palatine are present in the oropharynx.

### Tonsilitis

-   Tonsilitis is the inflammation of palatine tonsils.

Tongue:
=======



### Structure of tounge

The tongue is described as a structure featuring characteristics of :

-   large

-   muscular

-   highly mobile

### Location of tounge

Tongue is present in the floor of buccal cavity.

Terms related to tounge
-----------------------

-   **Frenulum Linguae** :

    -   The tongue is attached to the floor of mouth cavity by a fold
        called frenulum linguae.

-   **Ebners / Weber glands**:

    -   Ebners glands are the glands present on the surface of tongue.

-   **Sulcus terminalis**:

    -   Structure dividing tongue into two parts as:

        -   pharyngeal

        -   papillary part.

Papillae in tongue
------------------

### Location of papillae in tongue

-   The dorsal surface of tongue has the presence of lingual papillae.

Types of papipplae
------------------

-   The types of papillae are:

    -   vallate papillae

    -   filiform papillae

    -   fungiform papillae

Vallate papillae
----------------

### Location of vallate papillae

Vallate papillae is located at the base of the tounge.

### Structure of vallate papillae

-   Vallate papillae are the largest.

-   They are arranged in an inverted V manner.

### Number of vallate papillae

-   Vallate papillae are 8-12 in number.

Fungiform papillae
------------------

### Location of fungiform papillae

Fungiform papillae is situated at the + tip of the tongue + lateral
edges of tongue

### Structure of fungiform papillae

-   Fungiform papillae are

    -   mushroom shaped structures

Filiform papillae
-----------------

-   Filiform papillae are the smallest of all papillae.

### Quantity of filiform papillae

-   Filiform papillae are present on two third of tongue.

Functions of tongue
===================

-   The functions of tongue are:

    -   Tongue assists in mastication

    -   Tongue assists in swallowing

    -   Tongue assists in speech.

    -   Tongue helps to find taste.

Teeth
=====

Characteristics of human teeth
------------------------------

-   The human teeth is characterized as:

    -   Thecodont

        -   Thecodont is a term for teeths embedded in jaw socket.

    -   Bunodont

        -   Bunodont are teeths having low cups.

    -   Heterodont

        -   Heterodont are those having different types of teeth.

    -   Diphyodont

        -   Diphyodont are those having two sets of teeth.

Shape of teeth of humans
------------------------

-   The teeth of human are:

    -   incisors

        -   Incisors are characterized with with sharp, chisel-like
            cutting edges.

    -   canine

        -   Canine are characterized with with pointed dagger-shaped
            structures.

    -   premolar and molar

        -   Premolar and molar are structures with broad and grooved
            cusps.

Dental formula
--------------

Dental formula is the arrangement of teeth in respective jaw.



-   The dental formula for human milk teeth is:

    -   I 2/2

    -   C 1/1

    -   PM 0/0

    -   M 2/2

-   The dental formula for arrangement of human permanent teeth is:

    -   I 2/2

    -   C 1/1

    -   PM 2/2

    -   M 3/3

Structure of tooth
==================



Tooth consists of following three parts - Crown - Neck - Root

Crown
-----

-   Crown is the exposed part of tooth.

-   Crown is covered with hardest shining substance called enamel.

-   A layer of dentine is present below enamel.

Neck
----

Neck is the part surrounded by gum.

Root
----

-   Root is the part embedded in bony socket.

-   Root is covered by a protective and supportive hard layer called
    cementum.

-   There is the presence of pulp cavity

-   Pulp cavity is filled with nerve fibres, blood vessels .

-   Pulp cavity is lined by odontoblasts.

-   Enamel is secreted by ameloblasts

-   Ameloblasts are of ectodermal origin.

-   Dentine is secreted by odontoblasts.

-   Odontoblasts are of mesodermal origin.

### Facts about teeth

-   Third molar is called wisdom tooth.

-   Third molar erupts between 17-25 years.

Function of teeth
-----------------

-   Teeth help in mastication

-   Teeth help in speech production.

Pharynx
=======



Dimension of pharynx
--------------------

-   Pharynx is about 12 cm long.

-   **Location of pharynx** : Pharynx is a vertical canal beyond the
    soft palate.

Types of Pharynx
----------------

-   Nasopharynx

-   Oropharynx

-   Laryngopharynx

Nasopharynx
-----------

-   Nasopharynx is the upper part of pharynx.

-   Nasopharynx has internal nares in the roof.

-   Nasopharynx has a pair of Eustachian opening on lateral side.

Oropharynx
----------

-   Oropharynx is the lower part of the pharynx.

-   The function of oropharynx is for the passage for food.

Laryngopharynx
--------------

-   Laryngopharynx is the lowest part of the pharynx.

-   Laryngopharynx has two apertures-anterior slit-like glottis.

-   Laryngopharynx posterior gullet.

-   Glottis bears epiglottis.

-   Epiglottis is a leaf like cartilaginous flap.

Function of pharynx
-------------------

-   Pharynx passages the food from mouth cavity to the oesophagus.

Oesophagus:
===========



Structure of Oesophagus
-----------------------

-   Oesophagus is a

    -   muscular

    -   long

    -   narrow

    -   elastic tube

Dimensions of Oesophagus
------------------------

The oesophagus is about 25cm in length.

Location of Oesophagus
----------------------

-   The oesophagus passes through the diaphragm .

-   The oesophagus opens in the stomach in the abdomen.

-   Oesophagus pierces the diaphragm at hiatus.

Histology of Oesophagus
-----------------------

-   Oesophagus is lined with stratified squamous epithelium.

-   Oesophagus contains goblets cells.

-   Oesophagus does not contain digestive glands.

Anatomy of Oesophagus
---------------------

-   The upper end of oesophagus is closed by circo oesophageal
    sphincter.

-   The lower end of oesophagus id closed by oesophageal sphincter.

Functions of Oesophagus
-----------------------

-   Oesophagus passes food from the pharynx to the stomach.

-   The passage of food from pharynx to stomach is done by peristalsis.

Stomach
=======



Location of stomach
-------------------

Stomach is situated on the left side of abdominal cavity.

Structure of stomach
--------------------

-   Stomach is the broadest part of alimentary canal.

-   Stomach is J-shaped structure.

-   Stomach has lesser and greater curvatures.

Parts of stomach
----------------

-   Stomach is divided into two parts.

-   The parts of stomach are:

    -   cardiac

    -   pyloric

Cardiac
-------


Parts of cardiac portion
------------------------

-   The larger cardiac part is further subdivided into two parts.

-   The two parts of the cardiac portion are:

    -   fundus

    -   body

### Fundus

-   Fundus is the upper convex dome- shaped part.

-   Fundus is situated above the level of cardiac orifice.

### Body

Body lies between fundus and pyloric antrum.

Pyloric
-------

-   Pyloric part is divided into two parts.

-   The parts of the pyloric portion of the stomach are:

    -   pyloric antrum

    -   pyloric canal

### Pyloric canal

-   Pyloric canal is a narrow and tubular structure.

-   The right end of the pyloric canal ends at the pylorus.

### Pyloric antrum

Pyloric antrum is the right narrow lower portion of the stomach.


Anatomy of stomach
------------------

Stomach has two openings The openings of the stomach are : The cardiac
orifice The cardiac orifice is guarded by cardiac sphincter

and pyloric orifice guarded by pyloric sphincter. **Gastric Rugae**: +
Gastric rugae is the folds of the muscosa. + Gastric rugae is seen only
in empty stomach.

Histology of Stomach
--------------------

-   The mucosal lining of the stomach has gastric glands.

Functions of stomach
--------------------

-   Stomach acts as storage of food.

-   Stomach processes mechanical churning of food.

-   Stomach conducts partial digestion.

-   Stomach conducts the limited absorption of water, alcohol etc.

Intestine
=========

Location of intestine
---------------------

-   Intestine is the longest and coiled part of alimentary canal.

-   Intestine is located behind the stomach.

Parts of intestine
------------------

-   Intestine is divided into two parts .

-   The two parts of intestine are :

    -   small intestine

    -   large intestine.

Small intestine:
================



Structure of small intestine
----------------------------

Small intestine is the longest and coiled part of intestine

Parts of small intestine
------------------------

-   The small intestine is divisible into three parts

-   The three parts of the small intestine are:

    -   duodenum

    -   jejunum

    -   ileum

### Structure of Duodenum

Duodenum is C shaped curved tube.

### Anatomy of Duodenum

-   Duodenum receives bile from gall bladder

-   Duodenum receives pancreatic juice from pancreas

-   The bile and pancreatic juice are received through a common
    hepatopancreatic duct.

-   The opening of hepatopancreatic ampulla is guarded by sphincter of
    Oddi.

-   The wall of duodenum contains crypts of Lieberkuhn

-   The wall of duodenum contains branched Brunner’s glands.

### Structure of Jejunum

-   Jejunum is the middle part of small intestine.

-   Jejunum is narrower than duodenum.

### Anatomy of Jejunum

-   The jejunum has larger but thick walled villi.

-   The villi of jejunum does not absorb digested food.

### Structure of Ileum

-   The ileum is the longest part of the small intestine.

-   The ileum is the highly coiled part of small intestine

### Anatomy of Ileum

-   The wall of ileum is thinner and less vascular

-   The inner surface is provided with numerous villi.

-   Villi greatly increase the surface area for absorption.

-   Lamina propria of ileum contains granular masses of lymph nodules

-   The granular masses of lymph nodules in the lamina propria of ileum
    called Peyer’s patches.

Functions of small intestine
----------------------------

Functions: Small intestine serves two main functions; i) - Small
intestine completes the digestion of food. - Small intestine absorbs the
digested food.

Large intestine
===============



Dimesnsional properties of large intetsine
------------------------------------------

-   Large intestine is thicker small intestine .

-   Large intestine is wider than small intestine.

Location of large intestine
---------------------------



-   Large intestine surrounds the small intestine.

-   Large intestine is present in the lower abdominal cavity.

Shape of large intestine
------------------------

-   Large intestine has general shape of an inverted U.

Parts of large intestine
------------------------

-   Large intestine has three parts .

-   The parts of large intestine are:

    -   caecum

    -   colon

    -   rectum

Caecum:
-------

### Structure of caecum

-   Caecum is a dilated sac like structure.

### Ileo Caecal valve

-   The junction of ileum with caecum is called ileo-caecal junction.

-   The ileo caecal junction is guarded by ileo-caecal valve.

Vermiform appendix
------------------

-   Vermiform appendix is a short, slender worm -like projection.

-   Vermiform appendix is present in the caecum.

Colon:
------

Structure of colon
------------------

-   Colon is long and ucoiled tube.

Shape of colon
--------------

-   The shape of colon is inverted U shaped tube.

Parts of colon
--------------

-   Colon is divided into 4 parts.

-   The parts of colon are:

    -   ascending colon

    -   transverse colon

    -   descending colon

    -   sigmoid colon.

Histology of colon
------------------

-   The wall of colon has three longitudinal bands

-   The bands of wall of colon are called taeniae coli.

Rectum
------

Composition of rectum
---------------------

-   Rectum is made up of muscles.

Functions of rectum
-------------------

-   Rectum stores undigested food.

-   The time of storage of food is for short period.

Functions of large intestine
============================

-   The functions of large intestine are:

    -   absorption of water

    -   defaecation

    -   synthesis of vitamin K

    -   synthesis of vitamin B complex.

Anus:
=====



-   Anal sphincter guard the anus.

-   Anal sphincter are two in number.

Types of anal sphincter
-----------------------

-   The types of anal sphincter are:

    -   Internal anal sphincter

    -   External anal sphincter

-   Internal anal sphincter is made up of smooth muscles.

-   External anal sphincter is made up of striated muscles.





Digestive Glands
================

-   Based in the nature of action of digestive glands they are divsible
    into :

    -   associated digestive glands

        -   Associated digestive glands interact with digestion by
            producing ducts

    -   glands at the ailementary canal.

Associated digestive glands
---------------------------

-   The types of associated digestive glands are:

    -   salivary glands

    -   liver

    -   pancreas

Glands at the ailementary canal
-------------------------------

-   The glands at the ailementary canal are:

    -   intestinal glands

    -   gastric glands

Role of digestive glands
------------------------

-   Digestive glands secrete digestive juices.

-   Digestive juices contain enzymes.

-   Digestive enzymes are necessary for the chemical digestion of the
    food.

Salivary Glands
===============


Location of salivary glands
---------------------------

-   Salivary glands are located at the mouth cavity.

Types of salivary glands
------------------------

-   The types of salivary glands are:

    -   parotid glands

    -   submaxillary glands

    -   sublingual glands

Parotid glands
--------------

-   Parotid glands are the largest salivary glands.

-   The secretion of parotid glands is called Stensen’s duct.

-   Stensen’s duct opens in the mouth cavity.

-   Stensen’s duct opens in the upper second molar tooth.

### Location of parotid glands

-   Parotid glands are present on each side of the face.

-   Parotid glands are present just below and in front of the ears.

Submaxillary glands
-------------------

-   The secretion of submaxillary glands is Wharton’s duct.

-   Wharton’s duct is poured at the side of frenulum of the tounge.

Location of submaxillary glands
-------------------------------

-   Submaxillary glands are located at the angles of the lower jaw.

Sublingual glands
-----------------

-   Sublingual glands are the smallest salivary glands.

-   Sublingual glands secrete ducts.

-   The duct of sublingual glands is called duct of Rivinus.

### Location of sublingual glands

-   Sublingual glands are located beneath the tounge.

Saliva
======

-   Saliva is a fluid.

-   Saliva is viscous.

-   Saliva is colourless.

-   Saliva is cloudy.

### Contents of saliva

The contents of saliva are:

-   salivary amylase

-   lysozome

-   lingual lipase

Functions of saliva
-------------------

The functions of saliva are:

-   Saliva moistens dry food.

-   Saliva facilitates sallowing action by lubrication of food.

-   Saliva keeps the mouth clean.

-   Saliva keeps the teeth clean.

-   Saliva dissolves sugar.

-   Saliva dissolves salts.



Gastric glands
==============

### Location of gastric glands

-   The gastric glands are present at the wall of stomach.

Types of gastric glands
-----------------------


The types of gastric glands present at the wall of stomach are: - simple
branched - simple tubular

Types of cells present in gastric glands
----------------------------------------




The types of cells present in gastric glands are:

-   parietal cells

-   chief cells

-   mucous cells

Functions of parietal cells
---------------------------

-   Parietal cells secrete Castle’s intrinsic factor.

-   Parietal cells secrete

### Role of castle’s intrinsic factor

-   castle’s intrinsic factor absorbs Vitamin

Functions of chief cells.
-------------------------

-   Chief cells secrete proenzymes.

-   The proenzymes secreted by chief cells are:

    -   pro renin

    -   gastric lipase

Functions of mucous cells
-------------------------

-   Mucous cells secrete mucus.



Gastric Juice
-------------

-   All the secretion of gastric glands is called gastric juice.

-   There is the presence of Argentaffin cells.

-   Argentaffin cells are also called entero-endocrine cells.

### Argentaffin cells

-   Argentaffin cells consists of

    -   D - cells

    -   G Cells


### Contents of D-cells secretion

-   D-cells are composed of following secreted constituents:

    -   somatostatin

    -   enterocrinin

-   Somatostatin and enterocrinin cell secrete enzymes.

-   The enzymes / ducts secreted by somatostatin and enterocrinin are:

    -   serotonin

    -   histamine

### Contents of secretion of G-cells

-   G-cells secrete hormones.

-   The hormones secreted by G-cells is :

    -   gastrin



Liver
=====


-   Liver is the largest gland.

Location of liver
-----------------

-   Liver is located behind the right side of abdominal cavity

-   Liver is located behind the diaphgram

-   Colour of liver is reddish brown

Lobes of liver
--------------

-   Liver has two lobes:

-   Caudate lobe is a lobe of the liver

-   Quadrate lobe is a lobe of the liver

Gall bladder
============

-   Gall gladder is a pear shaped structure.

-   Gall bladder is located under the surface of right lob

Functions of gall bladder
-------------------------

-   Gall bladder acts as resorvior.

-   Gall bladder stores bile secreted by liver.

-   Gall bladder produces cystic duct

-   Cystic duct of gall bladder joins with bile ducts.

-   This combination of the ducts is a common hepatic duct.

-   The common hepatic duct opens in the duodenum

Histological structure of liver
-------------------------------

-   Liver lobes are formed by hexagonal lobules.

-   Hexagonal lobules are surrounded by sheath of connective tissue.

-   The sheath of connective tissue that surrounds the hexagonal lobules
    are called Glisson’s capsule.

### Histological structure of lobules of liver

-   Each lobule of liver is formed by hepatocytes

-   Hepatocytes are hepatic cells.

-   The shape of hepatocytes is polyhedral.

Anatomical structure of liver
-----------------------------

-   There is the presence of portal triad at the periphery of each
    lobule.

-   Portal triad consists of:

    -   portal vein

    -   hepatic artery

    -   bile duct

-   Bile canaliculi are the spaces between hepatic cells.

-   Bile canliculi unite to form bile duct.

-   Bile duct of lobules unite to form common hepatic duct.


Functions of liver
------------------

Secretion of bile
-----------------

-   The chemical property of bile is alkaline .

-   The colour of bile’ is dark green.

-   The physical state of bile is a fluid.

### Contents of bile

-   Bile contains bile salts

-   Bile contains bile pigments.

-   Bile contains some waste substances.

-   The contents of bile salts are:

    -   sodium bicarbonate c sodium glycolate

    -   sodium tauro chlorate

### Contents of bile pigment

-   The contents of bile pigments are:

    -   bilivirdin  

    -   bilirubin

Functions of bile
-----------------

-   Bile acts as antiseptic.

-   Bile stimulates peristalsis.

-   Bile neutralizes chyme.

Carbohydrate metabolism.
------------------------

-   Liver regulates sugar level.

-   Liver regulates sugar level by glycogenesis .and glycolygenesis.



### Glycogenesis

-   Glycogenesis is the conversion of excess glucose into glycogen.

-   Glycogenesis is done by liver cells.

-   Glycogenesis is done in the presence of insulin.

### Glycoly genesis

-   Glycoly genesis the conversion of glycogen into glucose.

-   Glycogenesis is done by liver cells.

-   Glycoly genesis is done in the presence of glucagon.

Protein metabolism.
-------------------

-   Liver decomposes amino acids into ammonia.

-   Ammonia is converted into urea.

-   The process of conversion of ammonia in to urea is deamination

Lipid metabolism
----------------

-   Liver controls lipogenesis.

-   Lipogenesis is the conversion of excess glucose and amino acids into
    fats.

Production of plasma proteins
-----------------------------

-   Liver produces fibrinogen liver produces prothrombin

Immunity
--------

-   Liver does phagocytosis

-   Phagocytosis is done by Kupffer cells.

-   Kupffer cells eat dead cells and bacteria.

Storage
-------

-   Liver stores minerals.

-   Liver stores vitamins .

-   The minerals stored by liver are. copper and iron.

-   The vitamins stored by liver are $A$, $D$ $E$, $K$, and

Production
----------

-   Liver produces anticougulatory chemical

-   The anticoagulatry chemical is heparin.

-   Liver acts as erythropoetic organ in foetus.

-   Liver produces RBC in foetus.

Pancreas
========


-   Pancreas is the second largest organ .

-   Structure of pancreas is a long and lobe like.

-   The colour of pancreas is yellow.

-   The mass of pancreas is ( 60gm ).

-   **Location of pnacreas:**Pancreas is located at the loop of
    duodenum. Pancreas extends upto the spleen behind the stomach

-   **Parts of Pancreas:** Pancreas contains head , body and tail.

-   **Type of Gland:** Pancreas is a heterocrine gland.

Exocrine part of pancreas
-------------------------

-   The exocrine part of pancreas is formed by pancreatic lobules.

-   The pancreatic lobules contain aciner glandular cells.

-   The aciner glandular cells are triangular.

-   **Functions of exocrine cells**:

    -   Exocrine cells of pancreas secrete pancreatic juice.

    -   Pancreatic juice is poured in the duodenum.

    -   The pouring of pancreatic juice is done through hepatopancreatic
        ampulla.



Endocrine part of pancreas
--------------------------


-   The endocrine part of pancreas contains a group of cells.

-   The group of cells on the endocrine part of pancreas are called
    islets of Langerhans.

### Islets of langerhans

-   The islets of langerhans contain four types of cells.

    -   ($\alpha$ ) cells.

    -   ( $\beta$)cells.

    -   ($\delta$) cells.

    -   ( F ) cells.

### Secretion of cells in islets of langerhans

-   Alpha cells secrete glucagon.

-   Beta cells secrete insulin.

-   Delta cells secrete somatostatin.

-   F cells secrete pancreatic polypeptide.



### Functions of secretion of cells of islets of langerhans

-   Insulin helps in carbohydrate metabolism.

-   Glucagon helps in carbohydrate metabolism.

-   Somatostatin inhibit the secretion of insulin and glucagon.

-   Pancreatic polypeptide inhibit the secretion of pancreatic juice.






Intestinal glands
=================


### Location of intestinal glands

-   Intestinal glands are located at the mucosa of small intestine.

-   The intestinal glands are of two types.

-   The types of intestinal glands are:

    -   crypts of lieberkhun

    -   brunner’s gland

Crypts of lieberkhun
====================

-   Crypts of lieberkhun are located at the villi of small intestine.

-   Crypts of lieberkhun are simple tubular glands.

Function of crypts of lieberkhun
--------------------------------

-   Crypts of lieberkhun collectively secrete succus entericus.

-   The enzymes present at the succus entericus are:

    -   amino peptidase

    -   dipeptidase

    -   intestinal lipase

    -   enterokinase

    -   maltase

    -   iso maltase

    -   dextrinase

    -   lactase

    -   sucrase

    -   nucleotidase

    -   nucleosidase

Brunners gland
==============





-   Brunners gland are branched tubular glands.

-   Brunners gland are present at the submucosa of duodenum.

Functions of brunners glands
----------------------------

-   Brunners glands secrete thick mucus.

-   The thick mucus secreted by brunners gland protects the duodenal
    mucosa from acidic chyme.

Parts of digestion 
=============================================

The parts of digestion are:

-   Ingestion

-   Digestion

-   Absorption

-   Assimilation

-   Egestion




Ingestion
=========

-   Ingestion is the process of taking food into oral cavity.

Digestion
=========

-   Digestion is:

    -   The hydrolysis of complex food substances to simplest compounds.

    -   The simplest compounds can be absorbed in the ailementary canal.

    -   The simplest compounds are absorbed for their utilization.

The location of digestion are:

-   Buccal cavity

-   Stomach

-   Intestine

Buccal digestion 
=========================================

-   **Mastication:**

    -   The breaking down of ingested food into smaller pieces is
        mastication.

    -   **Location of mastication:** Mastication is done in the buccal
        cavity.

-   **Role of tounge:**

    -   Tounge mixes the small food particles with the saliva.

Contents of Saliva
------------------

The saliva consists of the following components:

-   salivary amylase

-   lingual lipase

Functions of Saliva 
-----------------------------------------------

-   **Salivary amylase**

    -   Salivary amylase converts starch

    -   It converts starch into:

        -   maltose

        -   isomaltose

        -   dextrin

        $$\ce{ Starch ->[Ptyalin] Maltose + Isomaltose + Dextrin    }\\
        $$

-   **Lingual lipase**

    -   Lingual lipase converts lipids

    -   It converts lipids into:

        -   lipase Fatty acids

        -   glycerol

        $$\ce{Lipids ->[Lingual Lipase] Lipase fatty acids + Glycerol}$$

Post buccal digestion 
---------------------------------------------------

-   **Bolus:**

    -   Bolus is the food passed from buccal cavity to pharynx.

-   **Peristalsis:**

    -   Peristalsis is a wave like motion.

    -   **Cause of peristalsis:** Peristalsis occurs by the alternating
        contraction and relaxation.

        -   Longitudinal muscles relax and contract.

        -   Circular muscles relax and contract.

Gastric digestion 
===========================================

-   **Site for gastric digestion:**

    -   Gastric digestion occurs in the stomach.

    **Component of gastric digestion:**

    -   Gastric digestion contains gastric juice.

        -   **Contents of gastric juice:**

            -   Gastric juice contains enzymes.

The components of gastric enzymes are:

-   pepsin

-   renin

-   gastric lipase



Pepsin
------

-   **Secretion:** Pepsin is secreted as pepsinogen.

-   **Conversion:**

    -   Pepsinogen is converted into pepsin.

    -   Pepsinogen is converted into pepsin by

    $$\ce{Pepsinogen ->[HCl] Pepsin}$$

Function of pepsin 
---------------------------------------------

-   **Pepsin:**

    -   Pepsin hydrolyses protein.

    -   Pepsin hydrolyses protein into:

        -   proteoses

        -   peptones

    $$\ce{Protein ->[Pepsin] Protesoses + Peptones}$$

Renin
-----

-   **Secretion:**

    -   Renin is secreted as prorenin.

-   **Conversion:**

    -   Prorenin is converted into renin.

    -   Prorenin is converted into renin by .

    $$\ce{Proenin ->[HCl] Renin}$$

Function of renin 
-------------------------------------------

-   **Renin:**

    -   Renin converts caesin of milk.

    -   Renin converts casein of milk only in the presence of calcium.

    -   Renin converts casein of milk into:

        -   Paracaesin $$\ce{Casein ->[Renin , Ca++] Paracasein}$$

        -   **Post paracasein digestion:**

            -   Pepsin hydrolyses paracasein.

        -   Pepsin hydrolyses paracasein into:

            -   proteoses

            -   peptones

            $$\ce{Paracasein ->[Pepsin] Peptones + Proteoses}$$

Function of Gastric lipase 
-------------------------------------------------

-   **Gastric lipase:**

    -   Gastric lipase converts little of fats.

        -   This limitation is imposed by the lack of emulsifying
            chemicals.

    -   Gastric lipase converts fats into :

        -   fatty acids

        -   glycerol

        $$\ce{Fats ->[Gastric Lipase] Fatty acids + Glycerol}$$



Post Gastric Digestion: 
=======================================================

-   **Chyme:**

    -   Chyme is the partially digested food of the stomach.

-   **Peristalsis:**

    -   Peristalsis passes food from stomach to pyloric sphincter.

    -   Pyloric sphincter is present in the duodenum.

Internal digestion 
=============================================

Pre Internal Digestion 
-----------------------------------------------------

-   Bile is pored into the duodenum.

    -   Bile is pored by the contraction of gall bladder.

The components of internal digestion are:

-   bile

-   pancreatic juice

-   intestinal juice

Function of Bile
================


-   **Bile:**

    -   Bile salts emulsify fats.

        -   The emulsification of fats make an environment for action of
            pancreatic lipase.

Pancreatic Enzymes 
=============================================

The pancreatic enzymes are:

-   trypsin

-   chymotrypsin

-   carboxypeptidase

-   pancreatic amylase

-   pancreatic lipase

-   ribonuclease

-   deoxyribonuclease

Trypsin
-------

-   **Secretion:** Trypsin is secreted as inactive trypsinogen.

-   **Conversion:**

    -   The inactive trypsinogen is converted into trypsin.

    -   The inactive trypsinogen is converted into trypsin by
        enterokinase.

        -   Enterokinase is a component of succus entericus.

$$\ce{Trypsinogen ->[Enterokinase] Trypsin}$$

Function of trypsin 
-----------------------------------------------

-   **Trypsin:**

    -   Trypsin acts on protein.

    -   Trypsin hydrolyses protein into:

        -   proteoses

        -   peptones

Chymotrypsin
------------

-   **Secretion:** Chymotrypsin is secreted as inactive
    chemotrypsinogen.

-   **Conversion:**

    -   Chemotrypsinogen is converted into chymotrypsin.

    -   Chemotrypsinogen is converted into chymotrypsin by the action of
        trypsin.

$$\ce{Chymotrypsinogen ->[Trypsin] Chymotrypsin}$$

Function of chymotrypsin 
---------------------------------------------------------

-   **Chymotrypsin**

    -   Chymotrypsin acts on protein.

    -   Chymotrypsin hydrolyses proteins into:

        -   peptides

    $$\ce{Protein ->[Chymotrypsin] Peptones + Proteoses}$$
    $$\ce{Protesose + Peptones ->[Chymotrypsin] Dipeptides}$$

Carboxypeptidase
================

-   **Secretion:** Carboxypeptidase is secreted as procarboxypeptidase.

    -   Procarboxypeptidase is converted into carboxypeptidase

    -   Procarboxypeptidase is converted into carboxypeptidase by the
        action of trypsin.

$$\ce{Procarboxypeptidase ->[Trypsin] Carboxypeptidase}$$

Function of carboxypeptidase 
------------------------------------------------------------------

-   **Carboxypeptidase:**

    -   Carboxypeptdase acts on peptides.

    -   Carboxypeptidase reduces peptides into:

        -   dipeptides

        -   amino acids

$$\ce{Peptides ->[Carboxypeptidase] Dipeptides + Amino acids}$$

Function of Pancreatic amylase 
---------------------------------------------------------------------

-   Pancreatic amylase acts on:

    -   starch

    -   glycogen

-   Pancreatic amylase converts carbohydrate into :

    -   maltose

    -   isomaltose

    -   limit dextrin

$$\ce{Carbohydrates ->[Pancreatic amylase] Maltose + Isomaltose + Dextrin}$$

Function of Pancreatic lipase 
-------------------------------------------------------------------

-   **Pancreatic lipase**

    -   Pancreatic lipase acts on dietary fats.

    -   Panceatic lipase converts dietary fats into :

        -   fatty acids

        -   glycerol

$$\ce{Fats ->[Pancreatic lipase] Fatty acids + Glycerol }$$


Function of Ribonuclease
------------------------

-   **Ribonuclease:**

    -   Ribonuclease acts on $RNA$.

    -   Ribonuclease converts $RNA$ into

        -   Nucleotides

$$\ce{RNA ->[Ribonuclease] Nucleotides}$$

Function of deoxyribonuclease 
-------------------------------------------------------------------

-   **Deoxyribonuclease:**

    -   Deoxyribonuclease acts on $DNA$ .

    -   Deocyribonuclease converts $DNA$ into:

        -   Nucleotides

$$\ce{DNA ->[Deoxyribonuclease] Nucleotides}$$

Intestinal Juice 
=========================================

-   **Secretion:**

    -   Intetstinal juice is also termed as succus entericus.

The enzymes present in the intestinal juice are:

-   enterokinase

-   aminopepidase

-   dipeptidase

-   intestinal lipase

-   maltase

-   isomaltase

-   dextrinase

-   sucrase

-   lactase

-   nucleotidase

-   nucleosidase

Enterokinase
------------

-   Enterokinase is a non digestive enzyme.

Function of Enterokinase 
---------------------------------------------------------

-   **Enterokinase**

    -   Enterokinase acts on trypsinogen.

    -   Enterokinase converts on inactive trypsinogen into trypsin.

Function of aminopeptidase 
-------------------------------------------------------------

-   **Aminopeptidase:**

    -   Aminopeptidase acts on peptides.

    -   Aminopeptidase acts on amino group of peptides.

    -   Aminopeptidase peptides into :

        -   dipeptides

        -   amino acids

$$\ce{Peptides ->[Aminopeptidase] Dipeptides + Amino acids}$$



Function of dipeptidase 
-------------------------------------------------------

-   **Dipeptidase**

    -   Dipeptidase acts on dipeptides.

    -   Dipeptidase converts dipeptides into :

        -   Amino acids

$$\ce{Dipeptides ->[Dipeptidase] Amino Acids}$$

Function of intestinal lipase 
-------------------------------------------------------------------

-   **Intestinal Lipase**

    -   Intestinal Lipase acts on emulsified fats.

    -   Intestinal Lipase hydrolyses emulsified fats into:

        -   fatty acids

        -   glycerol

$$\ce{Emulsified fats ->[Intestinal Lipase] Fatty acids + Glycerol}$$



Function of Maltase 
-----------------------------------------------

-   **Maltase**

    -   Maltase acts on maltose.

    -   Maltase splits maltose into:

        -   glucose molecules

$$\ce{Maltose ->[Maltase] Glucose}$$

Function of Isomaltase 
-----------------------------------------------------

-   **Isomaltase**

    -   Isomaltase acts on isomaltose.

    -   Isomaltase converts isomaltose into:

        -   glucose

$$\ce{Isomaltose ->[Isomaltase] Glucose}$$

Function of Dextrinase 
-----------------------------------------------------

-   **Dextrinase:**

    -   Dextrinase acts on dextrins.

    -   Dextrinase hydrolyses dextrins into:

        -   glucose

$$\ce{Limit Dextrin ->[Dextrinase] Glucose }$$

Function of sucrase 
-----------------------------------------------

-   **Sucrase**

-   Sucrase acts on Sucrose.

-   Sucrase converts sucrose into:

    -   glucose

    -   fructose

$$\ce{Sucrose ->[Sucrase] Glucose + Fructose}$$

Function of Lactase 
-----------------------------------------------


-   **Lactase:**

    -   Lactase acts on lactose.

    -   Lactase converts lactose into:

        -   Glucose

        -   Galactose

$$\ce{Lactose ->[Lactase] Glucose + Galacose}$$

Function of nucleotidase 
---------------------------------------------------------

-   **Nucleotidase:**

    -   Nucleotidase acts on nucleotides.

    -   Nucleotidase hydrolyses nucleotides into:

        -   nucleosides

        -   inorganic phosphates

$$\ce{Nucleotides ->[Nucleotidase] Nucleosides + Inorganic Phosphate}$$

Function of nucleosidase 
---------------------------------------------------------

-   **Nucleosidase**

    -   Nucleosidase acts on nucleosides.

    -   Nucleosidase hydrolyses nucleosides into:

        -   nitrogen base

        -   pentose sugar

$$\ce{Nucleosides ->[Nucleosidase] Nitrogen Base + Pentose Sugar}$$

Post Intestinal Digestion 
-----------------------------------------------------------

-   **Chyle:** Chyle is the fully digested alkaline food.

    -   Chyle is present in small intestine.

    -   **Contents of chyle:**

        -   glucose

        -   fructose

        -   galactose

        -   amino acid

        -   fatty acid

        -   glycerol



Absorption
==========

Definition
----------

-   Absorption is a process.

-   There is transfer of food in absorption.

-   The food transfers from lumen of gut into:

    -   blood

    -   lymph

-   **Cause:** Absorption occurs by phisycochemical processes.

Sites of absorption 
-----------------------------------------------

-   **Buccal cavity:**

    -   There is no absorption in buccal cavity.

-   **Stomach:**

    -   There is very little absorption in stomach.

    -   The components absorbed are:

        -   alcohol

        -   aspirin

-   **Small intestine:**

    -   There is maximum absorption in small intestine.

    -   **Structure of small intestine:**

        -   The outermost lining of small intestine is mucosa.

        -   Mucosa is provided with villus.

        -   Each villus has a network of:

            -   blood capillaries

            -   lymphatic vessels

        -   Lymphatic vessels are also termed as lacteals.



Types of Absorption 
===============================================

The types of absorption are:

-   Active absorption

-   Passive absorption

Passive absorption 
---------------------------------------------

-   **Location:** Passive absorption occurs in between

    -   mucosal epithelial cells

    -   intestinal lumen

-   Passive absorption occurs along the concentration gradient.

-   The concentration gradient is from

    -   higher concentration

    -   to lower concentration

-   The higher concentration is present at the intestinal lumen.

-   The lower concentration is present at mucosal epithelial cells

-   **Rate of absorption:** Passive absorption is very slow.

-   **Requirement of Energy:** Passive absorption doesnot require
    energy.

-   **Process of passive absorption:** Passive absorption occurs by:

    -   Simple diffusion

    -   Facilitated diffusion

    -   Endocytosis

    **Limitations:**

    -   Passive absorption cannot absorb:

        -   nutrients from intestinal lumen

-   **Examples:**

    -   Passive absorption absorbs monosachharaides.

    -   Passive absorption absorbs amino acids.



Active Absorption 
-------------------------------------------

-   **Location:** Active absorption occurs between

    -   mucosal cells

    -   blood


    -   Active absorption occurs against the concentration gradient.

    -   Active absorption occurs from low concentration gradient into
        high concentration gradient.

-   The low concentration gradient is at intestinal lumen.

-   The high concentration gradient is at blood.

-   **Requirement of Energy:**

    -   Active absorption requires energy.

    -   Energy for active absorption is provided by the hydrolysis of .

-   **Absorption of Fats**:

    -   Fatty acids are incoporated into micelles.

    -   Micelles are:

        -   small

        -   spherical

        -   water soluble

        -   droplets

    -   Micelles are formed by the help of bile salts.

    -   Micelles are absorbed by facilitated diffusion.

    -   The components incoporated along with the absorption of Micelles
        are:

        -   fatty acids

        -   glycerol

        -   fat soluble vitamins

-   **Examples:** Active absorption absorbs:

    -   

    -   glucose

    -   galactose

    -   amino acids




Assimilation
============

-   Assimilation is the process of utilization of absorbed nutrients.

-   The nutrients are utilized into:

    -   body cells

    -   energy

    -   growth

    -   repair

Egestion
========

-   Egestion is the process of removal of food stuffs that are:

    -   undigested

    -   unabsorbed

-   Egestion is passed in the form of feaces.

-   The colour of feaces is due to stercobilin.

-   Stercobilin is a derivative of bilirubin.

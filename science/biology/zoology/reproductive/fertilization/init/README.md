### Fertilization in reproduction

Fusion of male and female pronucleus

### Zygote in reproduction

Fertilized ovum

### Magnitude of number of sperms deposited in vagina in insemination


200 - 400 million

### Location of fertilization in human beings


Ampullary isthmic junction

###  Magnitude of time period of fertility of sperm in female reproductive tract

12 - 24 hours

### Capacitation in fertilization

Change of sperm in membrane covering acrosome

### Type of medium female reproductive tract exhibits in terms of three categories

Strongly acidic

### Approximation of number of sperms required to dissolve corona radiata 

Thousands

### List of process of fertilization

- Penetration of sperm in ovum
- Activation of ovum
- Fusion of the sperm and the egg nucleus





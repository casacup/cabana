### Activities in changes in sperms in the penetration of sperm into the ovum 


- Breaking of acrosomal region
- Release of sperm lysin
- Disolution of vitelline
- Entry of one sperm
- Prevention of entry
- Hardening of vitelline
- Seperation of vitelline from plasma membrane

### Source of origin of sperm lysin in fertilization


- Acrosome


### List of chemical present at sperm lysin 


- Hyaluronidase
- Corona penetrating enzyme
- Acrosin
- Fertilin


### Function of hyaluronidase in fertilization

Dissolve follicle binding cells

### Chemical that glues follicle cells in ovum

Hyaluronic acid

### Term for acrosin in fertilization

Zona lysine

### Function of zona lysine in fertilization


- Digest zona pellucida


### Function of fertilin in fertilization

Recognize gamete of same species

### Monospermy in fertilization


- Fertilization of egg with only one sperm



 




﻿# Reproductive System

The process of producing young ones of their own  kinds is called reproduction.

### Purpose of reproduction

Reproduction is done to maintain the continuity of their races. 


## Sexual Characteristic of Human

- Human beings are unisexual.
- The other term for unisexual is dioecious.
    + Male and  Female reproductive systems are found in separate  individuals. 

## Primary sex organs

- The primary sex organs are
    + gonads
 

# Male Reproductive System 

The parts of male reproductive system are


- Scrotum
- Testes
    - The number of testes is two. 
- Epididymis
    - The number of epididymis is two. 
- Vasa  deferentia, 
    - The number of vasa deferentia is two. 
- Ejaculatory ducts, 
    - The number of ejaculatory ducts is two. 
- Urethra 
- Penis and 
- Accessory sex glands.

## Scrotum

Scrotum is a pouch of pigmented skin.


### Division of scrotum

- Scrotum has internal division.
- Scrotum is divided internally into 
    - Right scrotal sac
    - Left scrotal sac
- The partition is a muscular partition.
- The name for the partition for scrotum is 
    - Septum scroti.  
   

### Type of muscle in scrotum

Scrotum contains dartos muscles.


### Temperature in scrotum

- The temperature of scrotum  is lower than the normal body temperature.
- The temperature of scrotum is 
    + Normal Temperature - 2 \( ^{\circ}C \)  

- The temperature of scrotum is congenial for sperm  production.



## Testes

Testes are primary sex organs in humans.


## Anatomy of Testes

- The size of testis is small. 
- The colour of testes is pink.
- The shape of testes is 
    - oval 

### Location of testes

- Testes are located  in scrotal sac.
- The supporting structure of testes is
    - spermatic cord. 

### Covering of testes

- The testis is covered by three coats.
- The three coverings of testis are
    - tunica vaginalis
    - tunica albugenia
    - tunica vasculosa

- The outer covering of testes is
    - Tunica vaginalis
- The middle covering of testes is
    - Tunica albuginea
    - Tunica albugenia is a fibrous covering.
- The inner covering of testes is 
    - Tunica vasculosa.  

  

## Histology of Testes

### Lobules

- The number of lobules in a testes is
    - 200-300

### Quantity of lobules

Testis consists 
- 200-300 lobules. 

### Contents of testicular lobules

The contents of testicular lobules are

- Seminiferous tubules
    - The quantity of seminiferous tubules is
        - 1-4
- Blood vessels
- Nerves

The contents of testicular lobules are embedded in

- loose connective tissue


## Lining of seminiferous tubules


- The seminiferous tubule is lined by
    - germinal  epithelium. 


## Number  of types germinal epithelium


- The number of types of germinal epithelium are 2

## Types  of germinal epithelium

- The types of germinal epithelium cells are
    - Sertoli cells
    - Spermatogenic cells


## Functions of spermatogenic cells

- Spermatogenic cells produce spermatoza.

## Functions of sertoli cells

- Sertoli cells nourish the developing spermatogenic cells.  


## Location of Leydig cells

- Leydig cells are located in between seminiferous tubules.

### Term for leydig cells

- The other term for leydig cells is interstitial cells.

### Functions of leydig cells

- Leydig cells secrete testosterone.


## Structure of Epididymis


The epididymis is a mass of highly coiled  tubes. 

## Term for highly coiled tubes of epididymis

The other term for highly coiled tubes of epididymis is vasa recta.


## Location of epididymis

Epididymis lies outside and partially encircling  the testis.  


## Function of epididymis

 Epididymis  is a reservoir of spermatozoa.  


## Number of division of epididymis

The number of division of epididymis is 3.


## Region of Division of epididymis

The regions  of division of epididymis are

- caput epididymis
- corpus epididymis
- cauda epididymis


### Location of caput epididymis

Caput epididymis is present at the upper part of epididymis.

### Structure of caput epididymis

Caput epididymis is wider.

### Function of caput epididymis

Caput epididymis recieves vasa effrentia.

### Location of corpus epididymis

Corpus epididymis is present at the middle part of epididymis.

### Structure of corpus epididymis

Corpus epididymis is narrow.

### Location of cauda epididymis

Cauda epididymis is  present at the lower part of epididymis.

### Functions of cauda epididymis

Cauda epididymis  opens into the vas deferens.



## Location of vasa differtia

- Vasa differentia ascends into abdominal cavity.
- Vasa differentia forms a loop around the urinary bladder.


## Inguinal canal

Inguinal canal is the canal through which vasa differentia ascends to the abdominal canal.. 


## Anatomy Vasa Deferentia

Vasa differentia is a  tube.

## Histology of vasa deferentia

Vasa differentia contains thick walled muscles.

## Length of vasa differentia

The length of vasa differentia is 40cm.



## Function of vasa differentia

Vasa differentia transmits sperms from epididymis to ejaculatory duct.

## Length of Ejaculatory Ducts

The length of ejaculatory ducts is 2cm.

### Anatomy of ejaculatory ducts

Ejaculatory ducts are thin walled tubes.

### Formation of ejaculatory ducts

Ejaculatory ducts are formed by the union of seminal vesicle and a vas deferens.  

## Function of ejaculatory ducts.

- Ejaculatory ducts carries sperm.
- Ejaculatory ducts carries the secretion of  seminal vesicles. 

## Length of  urethra

The length of male urethra is 18-20cm.  

## Location of urethra

Urethra arises from the neck of urinary bladder.  

## Function of urethra

Urethra is a discharge tube of urine and semen. 

### Number of regions of urethra

The number of regions of urethra is 3.

### Regions of the urethra

The regions of urethra are 

- Prostatic urethra
- Membranous urethra
- Penile urethra.

## Penis
Penis is an erectile copulatory organ. 

## Contents of penis

The contents of penis are

- The long shaft
- The glans penis. 

## Location of prepuce

Prepuce covers the glans penis.

## Prepuce

Prepuce is a fold of skin.

## Contents of penis


The contents of penis are 

- Erectile tissues 
- Corpora cavernosa 
- Corpus spongiosum. 

### Number of erectile tissues

The number of erectile tissues is

- 3 column

### Number of corpora cavernosa

The number of corpora cavernosa is

- 2


### Number of corpus spongiosum

The number of corpus spongiosum is
- 1


## Cause Erection of penis
 The erection of penis is due to the rush of arterial blood.

## Destination of arterial blood in erection of penis

The arterial blood rushes into 

- sinuses of erectile tissues of penis 

## Function of penis

The function of penis is to deposit semen into vagina.

## Number of  Accessory sex glands in males

The number of accessory sex glands is 3.


## Types of accessory glands in males

The accessory sex glands are

- Seminal vesicles
- Prostrate gland
- Cowper's gland



## Number of Seminal Vesicles

The number of seminal vesicles present is 

- a pair.

## Structure of seminal vesicle

- Seminal vesicle is lobulated
- Seminal vesicle is elongated.

## Anatomy of a seminal vesicle

Seminal vesicle is a sac.

## Location of seminal vesicle

The seminal vesicle is located 

- near the base of urinary bladder.

## Functions of seminal vesicles

The seminal vesicles secrete seminal fluid.

## Colour of seminal fluid

The colour of seminal fluid is yellowish.

## Chemical property of seminal  fluid

The seminal fluid is alkaline.

## Contents of seminal fluid

The contents of seminal fluid are

- Fructose
- Citrate
- Proteins
- Prostaglandins


  



## Prostate gland
Prostrate gland is the largest accessory gland of male reproductive system.

## Location of prostrate gland

The prostrate gland lies below the neck of urinary bladder.

## Structure of secretion of prostrate gland

The secretion of prostrate gland is 

- Thin
- Milky  

## Chemical calssificaion of secretion of prostrate gland

The secretion of  prostrate gland is 

- slightly acidic 


## Function of secretion of prostrate gland

- The secretion of prostrate glands makes sperm mobile.
- The secretion of prostrate gland nourishes sperms.

  




## Number of Cowper’s glands

The number of copwer's gland is a pair.

### Term for cowpers gland

The other term for cowper's gland is bulbourethral gland.


## Size of Cowper's gland

The size of cowper's gland is pea sized. 


## Location of cowper's gland

The cowper's gland is located

- Below the prostate gland 
- At the base of penis. 

## Function of cowper's gland

Cowper's gland secrete mucus like fluid.

## Function of mucus like fluid of cowper's gland

The mucus like secretion of cowper's gland acts as lubricant for vagina. 

## Semen
Semen is the collective composition of fluids.

## Contents in the fluid of semen
The contents in the fluid of semen are

- Products of the testes
- Products of the prostrate gland
- Fluid from seminal vesicles

## pH of Semen

The value of pH of Semen is

- 7.35-7.5





## Organs of female reproductive system

The organs of female reproductive system are


- Ovaries
- Fallopian tube
- Uterus
- Vagina
- External genitalia
- Accessory genital glands
- Mammary glands



## Number of ovaries 

The number of ovaries in female reproductive system is

- a pair

## Primary sex organ of female

The are primary sex organ of female is

- ovary


## Colour of ovary

The colour of ovary is 

- greyish-pink 


## Shape of ovary

The shape of ovary is

- almond 

## Location of ovary

- The ovary is located in the posterior abdominal cavity.
- The ovary is located on either side of vertebral column 
- The ovary is located behind the kidney.


## Mesovarium

Mesovarium is a fold of peritoneum.

## Function of mesovarium

Mesovarium attaches ovaries to the dorsal body wall.


## Number of histological structure of ovary

The number of histological structure of ovary is 

- 3

## Histological layers of the ovary

The histological layers of the  ovary are

- Germinal epithelium
- Cortex 
- Medulla

## Location of tunica albugenia  

Tunica albugenia is present in between the germinal epithelium and cortex.

## Structure if tunica albugenia

Tunica albugenia is a thickened stomal layer.


## Contents of cortex

The cortex contains ovarian follicles of different stages of development. 


## Graafian follicle

Graafian follicle is a fully matured ovarian follicle.



## Membrana granulosa

Membrana granulosa is the outer covering sheath of graffian follicle.


## Function of membrana granulosa


Membrana granulosa encloses follicular cavity. 


## Contents of membrana granulosa

The contents of membrana granulosa are

- Colorless fluid
- Ovum

## Membranes surrounding ovum

The membranes that surround ovum are

- Zona pellucida
- Zona radiata


## Corpus luteum

Corpus luteum is the structure of follicular cavity after ovulation.

## Function of corpus luteum

Corpus luteum secretes progesterone.

## Corpus albicans

Corpus albicans is a structure  formed by corpus luteum.  














## Term for Fallopian tubes 

The term for fallopian tubes is oviducts.

## Number of fallopian tubes

The number  of fallopian tubes is 

- a pair

## Anatomy of fallopian tube

- Fallopian tube is a tube.

## Histology of fallopian tube

The fallopian tube is

- Muscular 
- Ciliated 

## Length of fallopian tube

The length of fallopian tube is 

- 10-12  cm

## Location of fallopian tube

Fallopian  tube  arise near ovary and extends up to uterus . 


## Number of layers of fallopian tube

The number of layers of fallopian tubes is
- 3

## Layers of fallopian tube
The layers of fallopian tube are

- Serosa
- Muscularis
- Mucosa

## Types of cells of epithelial mucosa

The types of cells of epithelial mucosa  is

- Simple columnar cells
- Secretory cells

## Number of division of oviduct

The number of division of oviduct is

- 3

## Division of oviducts

The  parts of oviducts are

- Infundibulum
- Ampulla
- Isthmus
- Uterine


## Shape of infundibulum

The infundibulum is funnel shaped.

## Ostium 

Ostium is an aperture opened by infundibulum. 

## Fimbriae

Fimbriae are the finger like processes of infundibulum.

## Function of fimbriae

Fimbriae collect ovum.


## Function of ampulla

Fertilization takes place in ampulla.

## Anatomy of ampulla

Ampulla is dilated. 


## Histology of ampulla

Ampulla is  thin walled .

## Location of ampulla

The location of ampulla is 

- next to infundibulum





## Anatomy of isthmus

Isthmus is 

- narrow
- short

## Location of isthmus

The location of isthmus is 


- next to ampulla


## Histology of isthmus

Isthmus is thick walled.


## Location of uterine 

Uterine is located near the uterus. 


## Function of oviducts

Oviducts carry ovum from.
The ovum is carried from ovary to uterus.






# Uterus

## Shape of uterus 

The shape of uterus is

- Pear 

## Anatomy of uterus

Uterus is hollow.



## Compositional histology of uterus

- The uterus is muscular.
- The uterus is thick walled.

## Divisional histology of uterus

The uterus is divided into


- Perimetrium
- Myometrium
- Endometrium

### Location of perimetrium 

The location of perimetrium is outside the uterus.

### Location of myometrium

The location of myometrium is in between perimetrium and endometrium

### Location of endometrium

The location of endometrium is inside the uterus.


### Anatomy of endometrium

Endometrium contains tubular glands.

### Histology of endometrium

The cells of endometrium are


- simple columnar epithelium




## Mesometrium

Mesometrium is a double fold of pertitoneum.

## Function of mesometrium

Mesometrium attaches uterus to the body wall.


  


# Parts of uterus

The parts of uterus are


- Fundus
- Body
- Cervix





## Location of fundus

The fundus lies at the upper part.

## Size of fundus

The size of fundus is wide.

## Shape of fundus

The shape of fundus is


- dome


## Location of body 

The location of body is


- at the middle



## Size of body

- The body is large.


## Location of cervix

- The location of cervix is the lower part of the uterus.


## Size  of cervix

- The size of cervix is narrow.




## Function of cervix

- Cervix projects into vagina

### Structure for communication of cervix with uterus


The communication of cervix with uterus is done through

- Internal orifice


### Structure for communication of cervix with vagina

The communication of cervix with vagina is done through 

- External orifice




iv.Vagina: It is fibro-muscular and 

tubular female copulatory organ.

### Location of vagina

The vagina is located at

- From cervix to uterus
- Outside the body


### Location of hyman in anatomical strtuture

The anatomical structure for the location of hyman is

- The opening of vagina

### Location of hyman in social class of structure

The presence of hyman in social classs of structure is


- Virgins

### Function of hyman


Hyman covers the vagina.


### Size of hyman

The size of hyman is

- Thin 

### Shape of Hyman

The shape of hyman is 

- Ring

### Function of vagina

The functions of vagina are

- Vagina serves as birth canal.
- Vagina recieves penis.  
    + The penis is recieved by vagina during copulation.
- Vagina allows menstrual flow.



### Term for external genitalia 
 
The other  term for external genitalia is


- Vulva

## Parts of external genitalia

The parts of external genitalia are


- Mons Pubis
- Labia Majora
- Labia Minora
- Clitoris




### Location of mons pubis

The location of mons pubis is

-  Above labia majora 

### Histology of mons pubis

- Mons pubis is fleshy

### Contents of mons pubis

The contents of mons pubis are

- Pubic hair


### Number of labia majora

The number of labia majora is

- 2


### Size of labia majora

The labia majora is 

- thic
- large

### Anatomy of labia majora

The anatomy of labia majora is 

- Folds of skin 


### Functions of labia majora

Labia majora forms the boundary of vulva.

### Contents of labia majora

The contents of labia majora are

- Pubic Hair
- Sebaceous Glands


  


### Number of labia minora

The number of labia  minora is

- 2

### Size of labia minora

The size of labia minora is

- Small

### Anatomy of labia minora

The structure of labia minora is

- Folds of skin 
- Thin

### Material structure of labia minora

The material structure of labia minora is

- moist 


### Histology of labia minora 

Labia minora is

- Fleshy



### Location of labia minora

The location of labia minora is
- Between labia majora 



### Vestibule

Vestibule is the space between labia minora.


### Size of clitoris

Clitoris is small.

### Function of clitoris

Clitoris is erectile organ.

### Location of clitoris

The location of clitoris is

- Anterior junction of labia minora 

### Homologousness of clitoris

The clitoris is homologous to

- Penis


  



## Accessory genital gland 

The accessory genital gland in female reproductive system is

- Bartholin’s gland

### Term for bartholin's gland

The other term for bartholin's gland is

- Vestibular gland

### Location of bartholin's gland

They occur one on each side of vaginal orifice. 


### Nature of secretion of bartholin's gland

The secretion of bartholin's gland is
- viscid


### Function of secretion of bartholin's gland

- The secretion of bartholin's gland lubricates the vulva. 
- The lubrication of vulva by  bartholin's gland occurs on sexual excitement.


# Breasts 

### Term for breasts

The other term for breasts is
 
 
 - Mammary glands
 
### Number of mammary glands in female reproductive system

The number of mammary glands in female reproductive system is


- Pair

### Shape of mammary glands

The shape of mammary glands is

- Rounded
- Swollen



### Material nature of mammary glands

The material nature of mammary glands is 

- Spongy 
- Tender 
- Smooth



### Function of mammary glands in female 


- Mammary glands produce milk.




### Location of nipple

The nipple is located at 


- Middle of breasts


Each breast has a nipple in its middle  


### Areola

Areola is a pigmented area.

### Shape of areola

The shape of areola is


- Circular


### Modification of sweat glands

The structure present in female reproductive system as modification of sweat glands is

- Mammary gland


### Number of lobes of lactiferous tubules in mammary glands

The number of lobes of lactiferous tubules in mammary glands is


- \( 15 - 20 \) 


### Histology of mammary glands

- Mammary glands contain lobes.
- The lobes which are contained in mammary glands is lactiferous tubules.

### Location of lactiferous tubules in mammary glands

The location of lactiferous tubules mammary glands is 


- Fatty tissue 



### Time of development of mammary glands

They are developed in  girls at the onset of puberty by the activity of  

### Role of hormones in  the development of mammary glands

The hormones having the role in development of mammary glands are

- Oestrogen
- Pitutiary Gland

# Menstrual Cycle

- The other term for menstrual cycle is 
    - ovarian cycle.  

- Menstrual cycle is a series of cyclic changes.





# Location of menstrual cycle


- Menstrual cycle occurs in the
    - reproductive tract of human female.

# Time of menstrual cycle


- Menstrual cycle is periodic .
- Menstrual cycle occurs with a periodicity of
    - 28 days
- Menstrual cycle starts from
    -  13 years
- Menstrual cycle ends on
   - 40-50 years  


# Characteristics of menstrual cycle

- There is loss of blood in menstrual cycle.
- The blood lost on menstrual cycle is vaginal.``

# Hormones of menstrual cycle


- Menstrual cycle is influenced by hormones from
    + pituitary gland
    + ovary




# Phases of menstrual cycle

The phases of menstrual cycle are


- Menstrual phase
- Proliferative phase
- Ovulatory phase
- Luteal phase



## Menstrual phase

- The other name for menstrual phase is
    - weeping of uterus 

### Time for menstrual phase
- Menstrual phase occurs for
    - 3-5 days


### Process of menstrual phase

- The mucosal lining of endometrium sheds.
- The shedding passes along with the blood.
- The shedding passes along with the connective tissue.

### Conditions for menstrual phase

- The amount of progesterone in blood is very low.
- The amount of oestrogen in blood is very low.
- The ovum is unfertilized.

### Amount of blood loss in menstrual phase

The amount of blood lost in menstrual phase is

- 50-100 ml



# Proliferative phase


- The other name for proliferative phase is
    + follicular phase
- The other name for proliferative phase is
    + oestrogenic phase

## Activities in proliferative phase

The activities in proliferative phase are

- Secretion of follicle stimulating hormone.
- Secretion of oestrogen.


### Secretion of follicle stimulating hormone

 The follicle  stimulating hormone is secreted by 

 - Anterior pituitary gland


### Function of follicle stimulating hormone in proliferative phase

Follicle stimulating hormone develops primordial follicle into mature follicle

### Term for mature follicle

The other term for mature follicle is


- Graafian follicle


### Function of graafian follicle

 Graafian follicle  secretes oestrogen. 


### Function of oestrogen in proliferative phase of menstrual cycle
 
- Oestrogen stimulates the proliferation of  endometrial epithelium. 

### Function of proliferation of endometrial epithelium

The function of proliferation of endometrial epithelium is

- To receive fertilized ovum

### Time period for proliferative phase in menstrual cycle in range

The time period for proliferative phase in menstrual cycle in range is

- End of menstruation to ovulation

### Time period for proliferative phase in number of days

The time period for proliferative phase in number of days is


- 10



# Ovulatory phase

In about middle of the menstrual cycle, usually in the 14th day of 28 day cycle,

## Activities in ovulatory phase

The activities in ovulatory phase are

- Secretion of luteinizing hormone



### Secretion of luteinizing hormone

The secretion of luteinizing hormone is done by

- Anterior region of pituitary gland

### Function of luteinizing hormone in ovulatory phase of menstrual cycle

Luteinizing hormone releases  ovum.
- The ovum taht luteinzing hormone releases is from  graafian follicle. 




#### Term for luteal phase

The other term for luteal phase is

- Progestational phase



### Function of corpus luteum in luteal phase of menstrual cycle

The function of corpus luteum in luteal phase of menstrual cycle is

- Secretion of progesterone
- Secretion of oestrogen

### Time of formation of corpus luteum

The formation of corpus luteum occurs 

- After ovulation 


### Composition for the formation of corpus luteum

The compostition for the formation of corpus luteum is

- Cells of ruptured follicle.


### Function of progesterone

The functions of corpus luteum are

- Corpus luteum prevents the maturation of follicles.
- Corpus luteum increases vascularity of endometrium.


## Activities in unfertilization

The activities in menstrual phase in unfertilization 

- Degeneration of corpus luteum
- Fall of oestrogen and progesterone in blood.
- Shedding of endometrial epithelium

### Corpus albicans in luteal phase of menstrual cycle


- Corpus albicans is degenarated structure of corpus lutem.


### Condition for formation of corpus albicans

Corpus albicans forms in

- unfetilization



### Cause of rupturing of blood vessels of endometrium

The cause of rupturing of blood vessels of endometrium is

- Fall of oestrogen in blood
- Fall of progesterone in blood


### Cause of shedding of endometrial epithelium in luteal phase of menstrual cycle

The cause of shedding of endometrial epithelium in luteal phase of menstrual cycle is


- Rupturing of blood vessels of  endometrium




### Cause of menstruation

The cause of menstruation in menstrual cycle is

- Shedding of endometrial epithelium


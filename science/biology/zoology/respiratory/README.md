Respiratory System
==================

-   Respiratory system is an organ system .

Functions of respiratory system
===============================

-   The functions of respiratory system are:

    -   Exchange of gases

    -   Oxidation of food

    -   The oxidation of food is done inside the tissues.

Purpose of oxidation of food
----------------------------

-   Oxidation of food is done to release energy.

Process of respiration
----------------------

-   The process of respiration are divided into:

    -   mechanical

    -   chemical

Mechanical process of respiration
=================================

-   There is transport of fresh air.

-   The fresh is transported from outside the lungs.

-   There is transport of .

-   The is transported from lungs to tissues.

-   There is transport of .

-   The is transported from tissues to lungs.

Chemical process of respiration
===============================

-   There is release of energy in tissues.

-   The energy is released by the oxidation of food.

Types of respiration
====================

The types of respiration are: - Aerobic - Anaerobic

Aerobic Respiration
===================

-   Aerobic respiration is done by almost all of the organisms.

Requirement for aerobic respiration
-----------------------------------

-   There is the involvement of oxygen in aerobic respiration.

Location for aerobic respiration:
---------------------------------

-   Aerobic respiration occurs at:

    -   cytoplasm

    -   mitochondria

Amount of energy in aerobic respiration
---------------------------------------

-   There is the release of large amount of energy in aerobic
    respiration.

Quantity in aerobic motion
--------------------------

-   The reactant is glucose.

-   The products are ATP molecules.

-   1 molecule of glucose releases 38 ATP molecules.

Chemical expression for aerobic respiration:
--------------------------------------------

$$\ce{ C6H12O6 + O2 -> CO2 + H2O + Energy }$$

Anaerobic Respiration
=====================

Requirement of Anaerobic respiration
------------------------------------

-   Anaerobic respiration occurs in the presence of certain enzymes.

Organisms conducting aerobic respiration:
-----------------------------------------

-   The organisms conducting aerobic respiration are:

    -   Taniea

    -   Ascaris

    -   Bacteria

    -   Fruits

    -   Germinating seeds

    -   Yeast

Limitation of Anaerobic respiration in terms of process
-------------------------------------------------------

-   Anaerobic respiration is such where oxygen is not required.

Quantity of Anaerobic Respiration
---------------------------------

-   The reactant in Anaerobic respiration is glucose.

-   The product in anaerobic respiration is ATP molecules.

-   One molecule of glucose yields 2 ATP molecules.

Equation for Anaerobic respiration
----------------------------------

-   The equation for anaerobic respiration is expressed as:

$$\ce{ C6H12O6 -> C3H6O3 + Energy }$$
$$\ce{ C6H12O6 -> C2H5OH + CO2 + Energy }$$

Feature of aerobic respiration in muscles
-----------------------------------------

-   Striped Muscles can do anaerobic respiration.

-   The anaerobic respiration can be done in the absence of oxygen.

-   Oxygen may be absent in emergency condition.

-   The other name for striped muscles is skeletal muscles.

Respiratory organs
==================

-   The respiratory organs are

    -   Nose and nasal chamber

    -   Pharynx

    -   Larynx

    -   Trachea

    -   Bronchi

    -   Lungs

    -   Respiratory muscles

Nose and nasal chamber
======================

Background
----------

### Shape of nose

-   Nose is a triangular structure.

### Backbone of nose

-   Nose is made up of muscles

### Supportive structure of nose

-   Nose is supported by

    -   bones

    -   cartilage

### Anatomy of Nose

-   The parts of nose are:

    -   external nostrils

    -   nasal chamber

    -   internal nostrils

External nostrils
-----------------

-   External nostrils are paired apertures.

### Location of external nostrils

-   External nostrils open outside the nose.

Nasal chamber
-------------

### Types of nasal chamber

-   The types of nasal chamber are:

    -   left nasal chamber

    -   right nasal chamber

### Division of nasal chamber

-   The division of nasal chamber is done by a septum.

-   The arrangement of the septum that divides the nasal chamber is
    vertical.

### Histology of nasal chamber

-   Nasal chamber is lined internally by ciliated epithelium.

Nasal conchae
-------------

-   The another term for nasal conchae is nasal turbinates.

### Location of nasal conchae

-   Nasal conchae are present in the respiratory region of nasal chamber

### Shape of nasal conchae

-   The shape of nasal conchae is that of a convoluted scroll.

### Quantity of nasal conchae

-   There are three pairs of nasal conchae present at the respiratory
    region of nasal chamber.

### Material structure of nasal conchae

-   Nasal conchae are bony projections.

Paranasal sinuses
-----------------

### Anatomy of paranasal sinuses

-   Paranasal sinuses are air filled cavity.

### Types of paranasal sinuses

-   The types of paranasal sinuses are

    -   frontal

    -   ethmoidal

    -   maxillary

    -   sphenoid

### Hisology of paranasal sinuses

-   Paranasal sinuses are lined by mucus cells.

Internal nostrils
-----------------

-   location of internal nostrils

-   internal nostrils open inside the pharynx.

Functions of nose
=================

-   Nose prevents the entrance of dust into the lungs.

-   Nose makes the air moist and warm.

-   Nose detects the smell by olfactory receptors.

Role of vibrissae
-----------------

-   The other term for vibrissae is nasal hairs.

-   Nasal hairs trap

    -   dust particles

    -   microorganism

-   This is done with the help of mucus.

Pharynx
=======

Types of pharynx
----------------

-   The types of pharynx are

    -   nasopharynx

    -   oropharynx

    -   laryngopharynx

### Role of nasopharynx

-   Nasopharynx leads to eustachian tube

### Role of Laryngopharynx

-   Laryngopharynx leads to larynx.

    -   This is done through an opening.

    -   The opening is called epiglottis.

Function of pharynx
-------------------

-   Pharynx passes food.

-   Pharynx passes air.

Larynx
======

Anatomy of larynx
-----------------

-   larynx is a hollow cartiligionous box.

Location of larynx
------------------

-   Larynx is located at

    -   the top of trachea

    -   the range of the location is from ( c\_{3} )to ( c\_{6} )

Composition of larynx
---------------------

-   Larynx is made up of cartilages.

-   The number of cartilages that make up the larynx are 9.

-   The types of cartilages are single and paired.

-   The single cartilages making the larynx are:

    -   thryroid cartilage

    -   circoid cartilage

    -   epiglottis

-   Each cartilage in pair cartilage consitutues for a quantity of
    cartilage.

-   The types of paired cartilage are

    -   arytenoid

    -   corniculate

    -   cuneiform

### Largest cartilage in larynx

-   The largest cartilage in the larynx is the thyroid cartilage.

-   Thyroid cartilage gets enlarged in male.

-   This enlargement is the result of puberty.

-   The enlargement of thyroid is coordinated by a hormone.

-   The hormone responsible for enlargement of thyroid cartilage is
    testosterone.

Epiglottis
----------

### Structure of epiglottis

-   Epiglottis is a leaf like structure.

### Composition of epiglottis

-   Epiglottis is made up of fibro elastic cartilage.

### Role of epiglottis

-   Epiglottis close the glottis.

-   The closing of glottis by the epiglottis is done during sallowing.

Vocal cords
===========

### Location of vocal cords

-   Vocal cords are present among

    -   thyroid cartilage

    -   arytenoid cartilage

Types of vocal cords
--------------------

-   There are two types of vocal cords

    -   Superior pair

    -   Inferior pair

Superior Pair
-------------

-   Superior pair is a false vocal cord.

    -   This is because superior pair have no role in producing sound

Inferior Pair
-------------

-   Inferior pair is true vocal cord.

    -   This is because inferior pair have the main role in producing
        sound.

Mechanism of production of sound
--------------------------------

-   The sound is produced by the passage of air.

-   The air passes through the true vocal cord.

-   Sound is produced by the vibration of true vocal cord.

Trachea
=======

Anatomy of trachea
==================

-   Trachea is a hollow tube.

Location of trachea
-------------------

-   Trachea is located from

    -   larynx

    -   upper part of thoracic cavity

-   The range for the location of trachea are

    -   ( c\_{6} ) to ( t\_{5} )

-   Trachea lies in parallel to oesophagus.

-   Trachea lies in ventral to oesophagus.

Dimesnsions of trachea
======================

Length of trachea
-----------------

-   The length of is :

    -   ( 12cm )

Diameter of trachea
-------------------

-   The diameter of trachea is:

    -   ( 2.5cm )

Histological structure of trachea
---------------------------------

-   There is the presence of internal lining in trachea.

-   The internal lining is done by pseudo stratified epithelium.

-   The pseudo stratified epithelium have the presence of goblet cell.

Anatomical structure of trachea
-------------------------------

### Contents of tracheal rings

-   Tracheal rings are made up of cartiliginous rings.

-   The cartiliginous rings are c shaped.

-   The cartiliginous rings are incomplete.

Function of tracheal rings
--------------------------

-   The functions of tracheal rings are:

    -   Tracheal rings prevent the collapsing of trachea.

    -   The prevention of collapsing of trachea is done during
        breathing.

Bronchi and Bronchioles
=======================

Types of bronchus
-----------------

-   The trachea divides into:

    -   right bronchus

    -   left bronchus

-   The division occurs at ( t\_{5} )

Structure of right bronchus
---------------------------

-   Right bronchus is shorter.

-   Right bronchus is wider.

-   The length of right bronchus is ( 2.5cm ).

Structural arrangement of right bronchus
----------------------------------------

-   Right bronchus divides into 3 branches.

-   Each branch of right bronchus passes into each lobes of the lungs.

Structure of left bronchus
--------------------------

-   Left bronchus is longer.

-   Left bronchus is narrower.

-   The length of left bronchus is ( 5cm ).

Structural arrangement of left bronchus
---------------------------------------

-   Left bronchus is divided into ( 2 )branches.

-   Each branch of the left bronchus passes into the respective lobe.

Division of lobular branches
----------------------------

-   The division of lobular branches occur in the order of:

    -   tertiary branches

    -   bronchioles

    -   terminal bronchioles

    -   respiratory bonchioles

    -   alvelolar duct

    -   alveoli

Anatomy of bronchi
------------------

-   Bronchi are supported by cartiliginious rings.

-   The cartiliginous rings are incomplete.

Histology of bronchi
--------------------

-   Bronchi are lined by pseudo stratified epithelium.

Histology of bronchioles
------------------------

-   Bronchioles are line by two types of tissues.

-   The two types of tissues that line the bronchioles are

    -   ciliated columnar

    -   cuboidal epithelium

Lungs and Alveoli
=================

Lungs
-----

-   There is a pair of lungs present at the human body.

-   The shape of the lungs is conical.

    -   Lung has a basal part.

    -   Lung has an apical part.

-   The material nature of lungs is soft.

-   The lungs are spongy.

### Basal part of lung

-   The basal part of lung in broad.

-   The basal part of lung is concave.

### Location of basal part of the lung

-   The basal part of the lung rests on the diaphgram.

### Apical part of lung

-   The apical part of the lungs is narrower.

Appearance of lungs
-------------------

-   The lungs are pinkish in nature.

Location of lungs
-----------------

-   The lungs are present at the either side of the heart.

-   The lungs occupy the greater region of the thoracic cavity.

Outer Histology of lung
-----------------------

-   The lung is enclosed in a double layered membrane.

-   The double layered membrane in which the lungs is enclosed is called
    pleura.

Description of double layered membrane of lung
----------------------------------------------

### Outer membrane

-   The outer membrane of the lung is parietal membrane.i

### Role of parietal membrane

-   The parietal membrane encloses the pleural space.

### Inner membrane

-   The inner membrane of the lung is the visceral membrane.

### Left lung

-   Left lung is smaller.

-   Left lung has cardiac notch.

### Right lung

-   Right lung is bigger.

### Lobes of right lung

-   The right lung has three lobes.

-   The lobes of the right lung are marked by fissures.

### Lobes of left lung

-   The left lung has two lobes.

-   The lobes of the left lung are marked by fissures.

### Internal structure of the lungs

-   The internal structure of lungs is structured as:

    -   tree of bronchi

    -   bronchioles

    -   alveoli

Alveoli
=======

-   The anatomical structure of alveoli is a

    -   sac

-   The alveoli are richly vascularized.

### Quantity of alveoli

-   The quantity of alveoli present in the lungs are ( 300 million. )

Anatomy of alveoli
------------------

-   The wall of alveoli consists of two types of cells.

    -   Type I pneumocytes

    -   Type II pnuemocytes

Role of Type I pneumocytes
--------------------------

-   Type I pneumocytes exchange gases.

Role Type II penumocytes
------------------------

-   Type II peumocytes produce surfactant.

Role of surfactant in alveoli
-----------------------------

-   There is the presence of surfactant in alveoli.

-   Surfactant is a compostion of:

    -   phospholipid

    -   protein

-   The surfactant reduce tension in alveoli.

-   The surfactant prevent alveolar collapse during expiration.

Arteries of lungs
-----------------

-   There are two arteries present in the lungs.

-   The arteries present in the lungs are:

    -   pulmonary artery

    -   bronchial artery

### Role of pulmonary artery in lungs

-   The role of pulmonary artery is to provide deoxygenated blood to the
    lungs.

### Role of bronchial artery in lungs

-   The role of bronchial artery is to provide oxygenated blood in the
    lungs.

Respiratory muscles
===================

Types of respiratory muscles
----------------------------

-   The types of respiratory muscles are:

    -   Main muscles

    -   Accessory muscles

Types of main muscles
---------------------

-   The types of main muscles are:

    -   Diaphgram

    -   Intercostal muscles

Structure of diaphgram
----------------------

-   The diaphgram is a thin structure.

-   The diaphgram is a muscular structure.

Role of diphgram in anatomy
---------------------------

-   Diaphgram separates two cavities.

-   The cavities that diaphgram separates are:

    -   thoracic

    -   abdominal

Role of diaphgram in respiration
--------------------------------

-   Diaphgram lowers down during contraction.

-   Diaphgram exhibits the following characters in contraction:

    -   Diaphgram becomes flat.

    -   Diaphgram becomes straight.

Location of intercostal muscles
-------------------------------

-   Intercostal muscles are present :

    -   in between the ribs.

Types of intercostal muscles
----------------------------

-   The intercostal muscles are categorized in terms of their
    arrangement.

-   The types of intercostal muscles in the basis of their arrangement
    are:

    -   external intercostal muscles

    -   internal intercostal muscles

Physiology of respiration
=========================

-   The physiology of respiration consists of the following steps:

    1.  Breathing

    2.  Exchange of gases

    3.  Transport of Oxygen

    4.  Internal Respiration

    5.  Transport of Carbon dioxide

Breathing
=========

-   Breathing is the process of :

    -   inhalation of oxygen from external environment to the lungs.

    -   exhalation of carbon dioxide from lungs to the external
        environment

Steps of breathing
------------------

-   The steps of breathing are:

    -   Inspiration

    -   Expiration

Rate of breathing
-----------------

-   The normal breathing rate is:

    -   ( 12-16 ) times per minute

Inspiration
===========

-   Inspiration is the process of intake of from external environment to
    the lungs.

Condition for inspiration
-------------------------

-   Inspiration occurs when pulmonary air pressure is lower than
    atmospheric pressure.

Process of inspiration
----------------------

### External Intercostal Muscles

-   External intercostal muscles contract.

-   External intercostal muscles are pulled outward .

-   External intercostal muscles are pulled upward.

### Diaphgram

-   Muscles of diaphgram contract.

-   Muscles of diaphgram becomes wide.

-   Muscles of diaphgram becomes straight.

### Thoracic Cavity

-   The size of thoracic cavity increases.

    -   The increase in size of thoracic cavity expands the lungs.

Prepost Inspiration
-------------------

-   The air pressure decreases in the lungs.

    -   The air pressure decreases in the lungs due to increase in
        volume of the lungs.

-   The air from atmosphere rushes towards the lungs.

    -   The air from atmosphere rushes towards the lungs to maintain

        -   inner air pressure

        -   outer atmospheric pressure

Expiration
==========

-   Expiration is the process of exhaling from lungs.

-   gas is exhaled in expiration.

-   The exhalation occurs from the lungs to the external environment.

Condition for expiration
------------------------

-   Expiration occurs when intra pulmonary air pressure is more than
    atmospheric pressure.

Process of Expiration
---------------------

### Internal Intercostal Muscles

-   Internal intercostal muscles contract.

    -   Contraction of internal intercostal muscles moves ribs inward.

    -   Contraction of internal intercostal muscles moves ribs downward.

### Diaphgram

-   The relaxation of diaphgram occurs.

-   Diaphgram moves to it's original position.

-   The original shape of diaphgram is dome shape.

### Thoracic cavity

-   Size of thoracic cavity decreases.

    -   The decrease in size of thoracic cavity decreases the volume of
        the lungs.

Prepost expiration
------------------

-   There is increase of air pressure in the lungs.

    -   The increase in air pressure of the lungs pushes the air form
        lungs to the external environment.

Exchange of gases in the lungs
==============================

-   Oxygen comes down to the alveolar surface.

-   Blood comes up to the alveolar surface.

-   The alveolar membrane is extremely thin.

-   There is close contact of air and blood.

Diffusion Membrane
------------------

-   The diffusion membrane of alveoli is made up of layers.

-   The layers that make up the diffusion membrane are three in number.

### Layers of diffusion membrane

-   The layers of diffusion membrane are:

    -   Squamous Epithelium

        -   The squamous epithelium is that of the alveoli.

    -   Endothelium

        -   Endothelium is that of the alveolar capillaries.

    -   Basement substance

        -   Basement substance is present between squamous epithelium
            and endothelium.

Method of exchange of gases in lungs
------------------------------------

-   Exchange in gases of lungs occurs by:

    -   simple diffusion method.

Mechanism of exchange of gases in lungs
---------------------------------------

-   Diffusion of gases occurs along the concentration gradient.

    -   Diffusion of gases occurs from higher partial pressure to lower
        partial pressure.

Pressure in venous blood
------------------------

-   Venous blood has lower partial pressure of oxygen.

-   Venous blood has higher partial pressure of carbon dioxide.

### Magnitude of pressure in venous blood

-   The magnitude of partial pressure of oxygen in venous blood is:

    -   ( P() = 40mm Hg )

-   The magnitude of partial pressure of carbon dioxide in venous blood
    is:

    -   ( P() = 44mm Hg )

Pressure in alveolar air
------------------------

-   Alveolar air has higher partial pressure of oxygen.

-   Alveolar air has lower partial pressure of carbon dioxide.

### Magnitude of pressure in alveolar air

-   The magnitude of partial pressure of oxygen in alveolar air is:

    -   ( P() = 100 mm Hg )

-   The magnitude of partial pressure of carbon dioxide in alveolar air
    is:

    -   ( P() = 40 mm Hg )

Process of diffusion in alveoli
-------------------------------

-   diffuses from the alveolar air to the blood capillaries.

-   diffuses from the blood capillaries into the alveolar air.

Transport of oxygen
===================

Medium for transport of oxygen
------------------------------

-   Oxygen is transported by blood.

-   Blood transports oxygen from lungs to tissues.

### Quantity of oxygen in blood

-   ( 100ml \_) of arterial blood can carry ( 20ml )of oxygen.

Transport in the form of solution
---------------------------------

-   Oxygen dissolves in the plasma of the blood.

-   The quantity of oxygen that dissolves in the plasma of the blood is:

    -   ( 1-3 % )

Transport of oxygen in the form of oxyhaemoglobin
-------------------------------------------------

### Amount of oxygen in oxyhaemoglobin

-   The quantity of oxygen transported in the form of oxyhaemoglobin is:

    -   ( 97-99 % )

### Process of formation of oxyhaemoglobin

-   The reactants in the formation of oxyhaemoglobin are:

    -   Oxygen

    -   Haemoglobin

-   Oxygen combines with haemoglobin of red blood cells.

-   The combination of oxygen and haemoglobin forms oxyhaemoglobin.

### Chemical equation for the formation of oxyhaemoglobin

$$\ce{ Hb + 4O2 -> Hb.(4O2) }$$

### Condition for formation of oxyhaemoglobin

-   The conditions for the formation of oxyhaemoglobin are:

    -   There is the decrement of partial pressure of .

    -   There is the decrement of temperature.

    -   There is the presence of high blood ( pH ) in lungs.

### Nature of oxyhaemoglobin

-   Oxyhaemoglobin is unstable compound.

### Disassociation of oxyhaemoglobin

-   Oxyhaemoglobin disassociates to release .

-   The disassociation of oxyhaemoglobin occurs in certain conditions.

### Quantity of oxygen in haemoglobin

-   One molecule of can carry ( 4 ) molecules of oxygen.

Internal Respiration
====================

Steps in internal respiration
-----------------------------

-   The steps in internal respiration are:

    -   disassociation of oxyhaemoglobin

    -   oxidation of food

Disassociation of oxyhaemoglobin
--------------------------------

### Condition for disassociation of oxyhaemoglobin

-   The conditions for disassociation of oxyhaemoglobin are:

    -   There is the presence of high partial pressure of .

    -   There is the presence of high temperature.

### Chemical equation for disassociation of oxyhaemoglobin

$$\ce{ Hb(4O2) -> Hb + 4O2 }$$

Oxidation of Food
-----------------

-   There is the breakdown of glucose.

-   The breakdown of glucose yields:

    -   carbon dioxide

    -   energy

    -   water

### Equation for oxidation of food

$$\ce{ C6H12O6 + O2 -> 6CO2 + 6H20 + Energy }$$

### Requirement for oxidation of food

-   Oxygen is needed for the oxidation of food.

Transport of carbon dioxide
===========================

Need for expulsion of carbon dioxide
------------------------------------

-   Carbon dioxide should be expulsed out of the body.

-   The need for the expulsion of carbon dioxide is because:

    -   Carbon dioxide is toxic to the body.

Transport of carbon dioxide as carbonic acid
--------------------------------------------

### Amount of carbon dioxide transported as carbonic acid.

-   The amount of carbon dioxide transported as carbon dioxide is:

    -   ( 7 % )

### Process of formation of carbonic acid

-   The reactants for the formation of carbonic acid are:

    -   carbon dioxide

    -   water

-   Carbon dioxide reacts with water to form carbonic acid.

### Chemical equation for formation of carbonic acid

$$\ce{ CO2 + H2O -> H2CO3 }$$

Transport of carbon dioxide as bicarbonates
-------------------------------------------

### Amount of carbon dioxide transferred as bicarbonates

-   The amount of carbon dioxide transported by bicarbonates is:

    -   ( 7% )

Pre Process of formation of bicarbonates
----------------------------------------

-   Carbon dioxide diffuses to red blood cells.

-   Carbon dioxide reacts with water.

-   The product from the reaction of carbon dioxide and water is
    carbonic acid.

### Condition for the formation of Carbonic acid

-   Carbonic acid only forms in the presence of :

    -   carbonic anhydrase

### Nature of carbonic acid

-   Carbonic acid is unstable.

-   Carbonic acid disassociates due to it's unstability.

-   The dissociation of carbonic acid results the formation of:

    -   
    -   

### Chloride shift

-   The other name for chloride shift is hamburger phenomena.

-   The diffusion of chloride ions from plasma to red blood cells is
    called chloride shift.

### Purpose of chloride shift

-   Chloride shift occurs for the need to maintain ionic balance.

Process of formation of bicarbonates
------------------------------------

-   Bicarbonate ions diffuse into the plasma.

-   Bicarbonate ions diffuse into the plasma from RBC.

-   Bicarbonate ions form metallic bicarbonate by combining either with:

    -   
    -   

### Chemical equations for the formation of bicarbonates

$$\begin{aligned}
\ce{ CO2 + H2O ->[carbonic anhydrase] H2CO3 & <-> {H+} + {HCO3-} }\\
\ce{ {K+} + {HCO3-} & -> KHCO3} \\
\ce{ {Na+} + {HCO3-} & -> NaHCO3 }\end{aligned}$$

Transport of carbon dioxide as carbamino compound
-------------------------------------------------

### Amount of carbamino compound

-   The amount of carbon dioxide transported in the form of carbamino
    compounds is:

    -   ( 23% )

Process of formation of carbamino compound
------------------------------------------

### Reactants in the formation of carbamino compound

-   The reactants for the formation of carbamino compound are:

    -   carbon dioxide

    -   amnio group of haemoglobin

-   Carbon dioxide reacts with amino group of haemoglobin to form
    carbamino haemoglobin compound.

### Chemical equation for formation of carbamino compound

$$ \ce{ CO2 + Hb.NH2 -> HbNHCOOH } $$



# Bohr's effect

- Bohr's effect is the effect of \ce{CO2} concentration on disassociation of oxyhaemoglobin.

## Discovery

- Bohr's effect was discovered by
    - Christian Bohr  


### High Partial Pressure of Carbon Dioxide

- High partial pressure of carbon dioxide enhances the
    - disassociation of oxyhaemoglobin 


### Low Partial Pressure of Carbon Dioxide

- Low partial pressure of carbon dioxide enhances the
    - formation of oxyhaemoglobin




## Bohr's Curve

- The other name for Bohr's curve is
    - oxygen disasscociation curev


### Graph of Bohr's Curve

- Bohr's curve is plotted among
    - percentage saturation of haemoglobin with oxygen
    - partial pressure of oxygen  

- The nature of Bohr's curve is
    - sigmoid 


## Analysis of Bohr's curve

### Increment of partial pressure of carbon dioxide

- The curve moves downward towards the right.
- The percentage of saturation of haemoglobin with oxygen decreases.


### Decrement of partial pressure of carbon dioxide

- The percentage of saturation of haemoglobin with oxygen increases.


## Factors affecting affinity of haemoglobin and oxygen

### Increment of Temperature

- The affinity of haemoglobin with oxygen **decreases** in *rise in temperature*.
- The percentage saturation of haemoglobin with oxygen decreases with increase in temperature.


### Decrement of Temperature

- The affinity of haemoglobin with oxygen **increases** in *fall in temperature*.
- The percentage saturation of haemoglobin with oxygen increases with decrease in temperature.


###  Increment of pH

- The affinity of haemoglobin with oxygen **increases** with *rise in pH* .
- The percentage saturation of haemoglobin with oxygen increases with rise in pH.


###  Decrement of pH

- The affinity of haemoglobin with oxygen **decreases** with *fall in pH* .
- The percentage saturation of haemoglobin with oxygen decreases with fall in pH.





# Terms related to respiration

## Spirometry

- Spirometry is the process of evaluating
    - pulmonary volumes
    - pulmonary capacities


## Tidal volume

- Tidal volume is the amount of air exchanged during normal breathing.
- The magnitude of tidal volume is
    - 500ml 

## Inspiratory Reserve Volume ##

- The short form for inspiratory reserve volume is  
    + IRV
- Inspiratory Reserve Volume is the additional volume of air, a person can inspire by forceful inspiration.
- The magnitude of inspiratory reserve volume is
    + 3000 ml


## Expiratory Reserve Volume ##


- The short form for expiratory reserve volume is 
    + ERV

- Expiratory reserve volume is the additional volume of air, a person can expire by forceful expiration.
- The magnitude of expiratory reserve volume is
    + 1100 ml
 




## Residual volume

The short form for residual volume is RV
- Residual volume is the  volume of air remaining in lungs even after a forcible expiration.
- The magnitude of residual volume is
- 1200 ml


 


## Vital capacity 

- The short form for vital capacity is  VC.
- Vital capacity is the amount of air which can be maximum inspired and also maximum expired.
- The magnitude of vital capacity is 
- $$ VC= IRV+ERV+TV $$



## Total lungs capacity 

- The short form for total lungs volume is 
    - TLC
 
 Total lungs capacity is the total volume of air accommodated in the lungs at the end of a forced inspiration. 
- The magnitude of total lungs capacity is 
- $$ TLC=VC+RV $$

## Dead space: 
during the normal respiration, the air remaining in area like trachea and bronchial tree do not participate in gaseous exchange is called dead space. Out of 500 ml dead space volume is about 150 ml.

## Respiratory quotient

Respiratory quotient  is the ratio of carbon dioxide formed and oxygen utilized during complete combustion of a fuel substance.

### Magnitude of respiratory quotient for glucose

- The magnitude for respiratory quotient  for glucose is
- $$ RQ=1 $$

### Measurement of respiratory quotient

The measurement of respiratory quotient is done by Ganong’s respirometer.

## Haldane effect

Haldane effect is  unloading of CO2 in high oxygen concentration.

# Respiratory disorders

## Hypoxia

- Hypoxia is the lack of oxygen  in tissues 

### Cause of Hyppoxia 

- Hypoxia is caused due to 
    - decreased Hb content in blood 
    - carbon monoxide poisoning.



## Emphysema

- Emphysema are air pollutants.

### Harmful effects of emphysema

- Emphysema cause chronic bronchitis.

### Harmful effects of chronic bronchitis

- Chronic bronchitis  breaks down the alveoli of lungs.

### Harmful effect of breakdown of alveoli of lungs.


- The breakdown of alveoli of lungs reducing surface area of gaseous exchange.

## Hypercapnia 

- Hypercapnia is the excess concentration of carbon dioxide in blood. 

## Dispnoea

- Dipsnoea is the condition of difficulty in breathing.

## Pneumonia

- Pneumonia is the infection of lungs.

### Causative agent of pneumonia

 The causative agent of pneumonia is Diplococus pneumonia .

### Symptoms of pneumonia

- Pneumonia  accumulates  mucous  in alveoli.
- Pneumonia accumulates lymph in alveoli.
- Pneumonia impairs gaseous exchange.

## Asphyxia

- Asphysxia is the condition of fall of oxygen in blood abd rise of carbon dioxide. 

## Results of asphyxia
- Asphyxia cause  paralysis of respiratory centre of brain.
- Asphyxia causes death. 


Animal Tissues
==============

Tissue
======

-   Tissue is the group of cells.
-   Tissues are
    -   similar in origin
    -   similar in structure
    -   similar in function

Terms related to tissues
------------------------

### Histology

-   The branch of science that deals with the study of tissues is
    histology.
-   The term histology was coined by Mayer.
-   Mayer was a German histologist.

### Term tissue

-   The term ‘Tissue’ was coined by Bichat.
-   Bichat was an French Anatomist and Physiologist.

### Father of modern histology

-   The father of Modern Histology is Xavier Bichat

### Father of histology

-   The father of histology is Marcello Malpighi.
-   Marcello Malpighi was an Italian Biologist.

### Histogenesis

-   Histogenesis is the study of development and differentiation of
    tissues

Types of animal tissues
=======================

-   Animal tissues are classified on the basis of
    -   structure
    -   function
-   The types of tissues on the basis of structure and function are
    -   Epithelial tissue
    -   Connective tissue
    -   Muscular tissue
    -   Nervous tissue

Epithelial Tissue
=================

Structure of epithelial tissue
------------------------------

-   Epithelial tissue is the covering tissue.
-   The cells in epithelial tissues are compactly arranged.
-   The cells of epithelial tissue form a sheet.
-   Epithelial tissue covers
    -   the body
    -   external hollow surfaces of organs
    -   internal hollow surfaces of organs

Development of epithelial tissue
--------------------------------

-   Epithelial tissues develop from all three layers.
-   Epidermis of skin develops from
    -   Ectoderm
-   The Lining of coelom develops from
    -   Mesoderm
-   The Lining of alimentary canal develops from
    -   Endoderm

Anatomy of epithelial tissues
-----------------------------

-   The cells of epithelial tissues are closely packed.
-   The cells of epithelial tissue lack the intercellular spaces.
-   The cells of epithelial tissue are connected together by a cementing
    substance.
-   The cementing substance is made up of carbohydrate derivatives.
-   The cementing substance of carbohydrate derivatives connecting the
    cells of epithelial tissue is called
    -   desmosome
-   The cells of epithelial tissue rest on the basement membrane.
-   The basement membrane is also termed as basal lamina.
-   The basal lamina has the following properties
    -   The basal lamina is non cellular.
    -   The basal lamina is formed by
        -   collagenous substance
        -   glycoproteinous substance
-   Epithelial tissue is avascular tissue.
-   Epithelial tissue receives the nourishments through diffusion.
-   The nourishment is received from underlying connective tissues.

### Basement membrane in epithelial tissues

-   Basement membrane is structure exhibiting
    -   Basement membrane is thin.
    -   The basement membrane is non cellular.
    -   Basement membrane is devoid of blood vessels.

Functions of Epithelial Tissue
==============================



-   Protection
-   Secretion
-   Filtration
-   Formation of gametes
    -   The gametes are formed from germinal epithelium.

Types of Epithelial Tissues
---------------------------

-   Epithelial tissue are divided into three types.
-   The division is done on the basis of
    -   shape of the tissues
    -   structure of the tissues
    -   function of the tissues
-   The types of epithelial tissues on the basis of shape structure and
    function are
    -   Simple epithelium
    -   Compound epithelium
    -   Specialized epithelium

Simple epithelial tissue
========================

Anatomy of simple epithelial tissue
-----------------------------------

-   Simple epithelial tissue is composed of a single layer of cells.
-   The cells of simple epithelial tissue rest on the basement membrane.

Types of Simple Epithelial Tissue
---------------------------------

-   The types of simple epithelial tissues are
    -   Simple Squamous epithelium
    -   Simple Cuboidal epithelium
    -   Simple Columnar epithelium
    -   Pseudo-stratified epithelium



Simple Squamous epithelium
--------------------------

### Structure of simple squamous epithelium

-   The cells of simple squamous epithelium are
    -   Large
    -   Flat
    -   Polygonal
-   The cells of simple squamous epithelium rest on the basement
    membrane.
-   The cells of simple squamous epithelium have large central nucleus.
-   The intercellular spaces are absent in cells of simple squamous
    epithelium.
-   The other term for simple squamous epithelium is pavement
    epithelium.
-   The term for simple squamous epithelium is pavement epithelium
    because
    -   The cells are flat.
    -   The cells are hexagonal .
    -   The cells lack the intercellular space.
    -   The cells appear like the tightly fitted mosaic tiles on the
        floor.

### Location of simple squamous epithelium

The simple squamous epithelium is found in

-   The lining of the coelom
-   The lining of alimentary canal
-   The lining of nasal cavity
-   The endothelium of blood vessel
-   The endo cardium of heart
-   The alveoli of lungs
-   The nephrons
-   The lining of buccal cavity
-   The Tympanic cavity

Functions of simple squamous epithelium
=======================================

-   Protection
-   Exchange of gases
-   Absorption
-   Filtration

Simple Cuboidal epithelium
==========================

### Structure of simple cuboidal epithelium

-   Simple cuboidal epithelium consists of cells that are of shape
    -   cubical
-   The cells of simple cuboidal epithelium rest on the basement
    membrane.
-   The cells of simple cuboidal epithelium have no intercellular.
-   There is the presence of a centrally located nucleus.
-   The cuboidal cells may have cilia.
-   The cilia in simple cuboidal epithelium may be located at their free
    surface.
-   The functions of cilia in simple cuboidal epithelium are
    -   Cilia of simple cuboidal epithelium conduct the mucus.
    -   Cilia of simple cuboidal epithelium conduct other substances.
-   The location of ciliated cuboidal epithelium is
    -   ducts of nephrons.
-   The cuboidal cells may be brush bordered.
-   The brush bordered cuboidal cells may have microvilli.
-   The functions of microvilli in cuboidal cells is for
    -   The microvilli of simple cuboidal epithelium reabsorb.
-   The location of simple cuboidal epithelium having microvilli is
    -   Proximal Convoluted Tubules of nephrons

### Location of simple cuboidal epithelium

The location of simple cuboidal epithelium are

-   The thyroid gland
-   The lining of gonads
-   The sweat gland
-   The salivary glands
-   The pancreatic glands
-   The female urethra
-   The lining of convoluted tubules of nephrons
-   The frontal surface of lens
-   The back surface of pancreatic duct

### Functions of simple cuboidal epithelium

-   Secretion
-   Excretion
-   Absorption

Simple Columnar Epithelium
==========================

### Structure of simple columnar epithelium

-   The cells of columnar epithelium are tall
-   The cells of columnar epithelium are pillar like.
-   The cells of columnar epithelium are attached to the basement
    membrane.
-   The intercellular space is absent in the cells of columnar
    epithelium.
-   The nucleus are located at the basal regions in the cells of
    columnar epithelium.
-   The free ends of columnar epithelium may have cilia.
-   The free ends of columnar epithelium may be brush bordered.

### Location of simple columnar epithelium

The location of simple columnar epithelium are

-   The lining of the stomach
-   The lining of the intestine
-   The gastric glands
-   The intestinal glands
-   The gall bladder
-   The ureter
-   The uterine wall

The location of ciliated columnar epithelium are

-   The respiratory tracts
-   The bronchioles
-   The oviducts

The location of brush bordered columnar epithelium are

-   The intestinal mucosa.

### Functions of simple columnar epithelium

-   Secretion
-   Absorption
-   Protection



Pseudo stratified Epithelium
============================

Structure of pseudo stratified epithelium
-----------------------------------------

-   Some of the cells of pseudo stratified epithelium are taller.
-   The taller cells of pseudo stratified epithelium have cilia at their
    free ends
-   Some of the cells of pseudo stratified epithelium are shorter.
-   The shorter cells of pseudo stratified epithelium have no cilia.
-   The cells of pseudo stratified epithelium rest on the basement
    membrane.
-   The basement membrane on which the pseudo stratified epithelium rest
    is the same.
-   The cells of pseudo stratified epithelium have no intercellular
    spaces.

Location of pseudo stratified epithelium
----------------------------------------

The location of pseudo stratified epithelium are

-   The lining of trachea
-   The large bronchi
-   The ducts of some glands,
-   The fallopian tubes

Functions of pseudo stratified epithelium
-----------------------------------------

-   The pseudo stratified epithelium propel of mucus in the lumen.
-   The pseudo stratified epithelium propel the particles in the lumen.

Compound Epithelium
===================

### Structure of compound epithelium

-   The cells in compound epithelium are arranged in multiple layers.
-   The lowermost cells form the germinative layer.
-   The other term for germinative layer is stratum germinativum.
-   The germinative layer rest on the basement membrane
-   The cells of compound epithelium divide and redivide.
    -   The division of cells at the germinating layer forms the upper
        cells.
-   The cells of compound epithelium are found in areas of
    -   wear
    -   tear

Types of Compound epithelium
----------------------------

-   Stratified epithelium
-   Stratified squamous epithelium
-   Stratified cuboidal epithelium
-   Stratified columnar epithelium
-   Transitional epithelium

Stratified epithelium
=====================

Structure of stratified epithelium
----------------------------------

-   Stratified epithelium has several layers of cells.
-   The lowermost cells cells of stratified epithelium rest on the
    basement membrane.

Stratified Squamous epithelium
==============================

Structure of stratified squamous epithelium
-------------------------------------------

-   The upper layer of cells in stratified epithelium are
    -   flat
    -   polygonal
    -   squamous
-   The lower layer of cells in stratified epithelium are
    -   germinative
    -   cuboidal or columnar
-   The lower layer cells in stratified epithelium lie on the basement
    membrane.

Location of stratified squamous epithelium
------------------------------------------

-   The skin
-   The buccal cavity
-   The tongue
-   The vagina
-   The uterus
-   The urethra

Function of stratified squamous epithelium
------------------------------------------

-   Stratified squamous epithelium protects areas exposed to friction
-   Stratified squamous epithelium regenerates areas exposed to
    friction.

Types of Stratified Squamous Epithelium based on Keratin
--------------------------------------------------------

-   Keratinized squamous epithelium
-   Non Keratinized squamous epithelium

Keratinized Stratified Epithelium
---------------------------------

### Structure of keratinized stratified epithelium

-   The other term for keratinized stratified epithelium is
    -   water proof epithelium
-   Keratinized stratified epithelium has keratin.
-   The keratin in keratinized startified epithelium is present on the
    outer surface.
    -   Keratin is a sclero protein.
-   The outer layer of cells in keratinized stratified epithelium become
    -   flattened
    -   horny
    -   cornified
    -   dead
-   The transformation in the outer surface occurs by Keratinization.
-   Keratinization is the process of formation of keratin

### Location of keratinized stratified squamous epithelium

-   The upper layer of skin
-   The hair
-   The nails
-   The horns
-   The hooves

### Function of keratinized stratified squamous epithelium

-   Keratinized squamous epithelium checks the loss of water.
-   Keratinized squamous epithelium protects from bacterial invasion.

Non Keratinized Stratified Epithelium
-------------------------------------

### Structure of non keratinized stratified epithelium

-   Non keratinized strtified epithelium lacks Keratin.
-   Non keratinized stratified epithelium is found in
    -   soft part of the body
    -   moist part of the body
-   Non keratinized stratified epithelium has no power of checking the
    loss of water.

### Location of non keratinized stratified epithelium

-   The buccal cavity
-   The pharynx
-   The vagina
-   The inner layer of rectum
-   The anus

### Function of non keratinized stratified epithelium

-   Non keratinized stratified epithelium protects from wear and tear.
-   Non keratinized stratified epithelium protects from from drying.

Stratified Cuboidal Epithelium
==============================

Structure of stratified cuboidal epithelium
-------------------------------------------

The upper layer of cells in stratified cuboidal epithelium are cuboidal.
The basal cells in stratified cuboidal epithelium are columnar.

### Location of stratified cuboidal epithelium

-   The mammary glands
-   The ducts of sweat glands
-   The urethra of female
-   The conjunctiva

### Function of stratified cuboidal epithelium

-   The stratified cuboidal epithelium protects.

Stratified Columnar epithelium
==============================

Structure of stratified columnar epithelium
-------------------------------------------

-   Stratified columnar epithelium has columnar cells at the upper
    layer.
-   Stratified columnar epithelium has cuboidal cells at the lower
    layer.

### Location of stratified columnar epithelium

-   The vasa deferentia
-   The respiratory tracts

### Function of stratified columnar epithelium

-   Stratified columnar epithelium protects

Transitional Epithelium
=======================


Structure of transitional epithelium
------------------------------------

-   Transitional epithelium tissue is made up of 5 to 6 layers of cells.
-   The cells of transitional epithelium have the ability of stretching.
-   The basal cells of transitional epithelium are smaller.
-   The middle cells of transitional epithelium are larger.
-   The middle cells are
    -   pear shaped
    -   club shaped and 
- The upper cells are 
    - dome shaped
- The other term for transitional epithelium is water proof epithelium.

Location of transitional epithelium
-----------------------------------

-   The urinary bladder
-   The ureter
-   The uterus

Function of transitional epithelium
-----------------------------------

-   Transitional epithelium provides elasticity for the stretching of
    the organs.
-   Transitional epithelium is water proof.

Specialized Epithelium
======================

Composition of specialized epithelium
-------------------------------------

-   Specialized epithelium is composed of cuboidal cells
-   Specialized epithelium is composed of columnar cells
-   The cells in specialized epithelium are modified to perform
    specialized functions.

Types of specialized epithelium
-------------------------------

-   Ciliated epithelium
-   Sensory epithelium  
-   Germinal epithelium
-   Glandular epithelium

Ciliated epithelium
===================

Structure of ciliated epithelium
--------------------------------

-   Ciliated epithelium is composed of cuboidal or columnar cells
-   Ciliated epithelium has cilia at it’s free ends.

Location of ciliated epithelium
-------------------------------

-   The lining of trachea
-   The bronchi
-   The nephrons

Function of ciliated epithelium
-------------------------------

-   Ciliated epithelium conducts mucus in the lumen.
-   Ciliated epithelium conducts other substances in the lumen.

Sensory epithelium
==================

### Structure of sensory epithelium

-   The sensory epithelium has modified columnar cells.
-   The sensory epithelium has sensory hairs at the free surface.
-   The sensory epithelium has nerve endings at the lower end.

### Location of sensory epithelium

-   The tongue
-   The nasal cavities
-   The retina of eyes
-   The cochlea of internal ear

### Function of sensory epithelium

-   Sensory epithelium perceives external stimuli.
-   Sensory epithelium perceives internal stimuli.
-   Sensory epithelium conducts the impulses.

Germinal epithelium
===================


Structure of germinal epithelium
--------------------------------

-   Germinal epithelium are modified cubical epithelial cells.
-   Germinal epithelium lines the gonads.
-   Germinal epithelium has the power of gametogenesis.

Location of germinal epithelium
-------------------------------

-   The lining of seminiferous tubules
-   The lining of ovary

Function of germinal epithelium
-------------------------------

-   Germinal epithelium perform gametogenesis.

Glandular epithelium
====================

Structure of glandular epithelium
---------------------------------

-   Glandular epithelium tissue may have modified cubical or columnar
    cells.
-   Glandular epithelium is secretary.

Types of glands
---------------

Number of cells
---------------

### Unicellular gland

-   Unicellular glands are one celled glands that secretes mucus

### Examples of unicellular gland

-   The Goblet cells

### Multicellular gland

-   Multicellular glands are formed of many cuboidal cells.

### Examples of multicellular glands

-   The sweat glands
-   The gastric glands

Presence or absence of ducts
----------------------------

### Exocrine gland

-   Exocrine glands are ducted glands.
-   Exocrine glands secrete enzymes.

### Examples of exocrine glands

-   The Salivary glands
-   The Tear glands
-   The lacrimal glands
-   The gastric glands
-   The intestinal glands

### Endocrine gland

-   Endocrine glands are ductless glands.
-   Endocrine glands secrete hormones.

### Examples of endocrine glands

-   The thyroid gland
-   The pituitary gland
-   The adrenal gland

### Heterocrine gland

-   Heterocrine glands are both exocrine and endocrine in function.
-   Heterocrine glands secrete both
    -   hormones
    -   enzymes

### Examples of heterocrine glands

-   The Pancreas
-   The enzymes secreted by pancreas are
    -   TAL
    -   Somatostatin
    -   PP
-   The hormones secreted by pancreas are
    -   Insulin
    -   Glucagon,
-   The enzymes secreted by testis are
    -   Sperm lysin
-   The hormones secreted by testis are
    -   Testesterone
-   The enzymes secreted by ovaries are
    -   Fertilizin
-   The hormones secreted by ovaries are
    -   Oestrogen
    -   Progesterone

Shape and complexity
--------------------

-   There are two types of glands on the basis of shape and complexity.
-   The type of glands on the basis of shape and complexity are
    -   Simple glands
    -   Compound glands

Types of Simple Glands
----------------------


-   Simple glands have single unbranched duct.
-   Simple glands may be tubular or alveolar.
-   Simple glands may be coiled or uncoiled.
-   Simple glands may branched or unbranched.

### Examples of Simple tubular glands

-   The Crypts of Lieberkuhn

### Examples of Simple coiled tubular glands

-   The Sweat glands

### Examples of Simple branched tubular glands

-   The gastric glands
-   The Brunner’s glands

### Examples of Simple alveolar glands

-   The mucus gland in frog
-   The seminal vesicles

### Examples of Simple branched alveolar glands

-   The sebaceous glands
-   The oil glands

Compound glands
---------------

### Examples of Compound tubular glands

-   The liver
-   The testes
-   The kidneys

### Examples of Compound Alveolar glands

-   The mammary glands
-   The pancreatic glands

### Examples of Compound tubulo-alveolar glands

-   The salivary glands
-   The Bartholin’s gland
-   The Cowper’s gland

Mode of secretion
-----------------

-   Merocrine gland
-   Apocrine gland
-   Holocrine gland

### Nature of Merocrine gland

-   The secretions in merocrine gland are released from the cell
    surface.
-   The secretions are released by diffusion.
-   The secretions are released without losing any of its cytoplasm

### Examples of merocrine gland

-   The goblet cells
-   The salivary glands
-   The intestinal glands
-   The sweat glands

### Nature of Apocrine glands

-   The secretions get collected in the apical part of the cells.
-   The secretions are released by bursting along with some apical
    cytoplasm.

### Examples of apocrine gland

-   The mammary glands

### Nature of Holocrine glands

The entire cell breaks down in order to release the secretions in
holocrine glands.

### Examples of holocrine glands

-   The Sebaceous glands

Nature of secretion
-------------------

### Nature of Mucus glands

-   Mucus glands secrete mucus
-   Mucus is a
    -   proteinous substance
    -   slimy substances

### Examples of mucus glands

-   The goblet cells

### Nature of Serous glands

-   The serous glands secrete clear watery fluids.

### Examples of serous glands

-   The salivary glands
-   The intestinal glands
-   The sweat glands

### Nature of mixed glands

-   The mixed glands secrete
    -   mucus substance
    -   serous substance

### Examples of mixed glands

-   The gastric glands
-   The pancreatic glands

Connective Tissue
=================

-   Connective tissue
    -   connects other tissues
    -   binds other tissues
    -   holds other tissues
-   Connective tissue originates from the mesodermal layer.

Characters of connective tissues
--------------------------------

-   There is the presence of intercellular space in connective tissue.
-   The connective tissue has non-living fibres.
-   There connective tissues have no basement membrane.
-   The connective tissues may be + vascular + avascular

Matrix
------

-   Matrix is a intercellular substance.
-   Matrix is clear.
-   Matrix is jelly like.
-   The contents of matrix are
    -   cells
    -   fibres

Functions of connective tissues
-------------------------------

-   Connective tissue binds other tissues and organs.
-   Connective tissues connects other tissues and organs.
-   Connective tissues hold other tissues and organs.
-   Connective tissues make supporting framework for the body
-   Connective tissues store fat .
-   Connective tissues acts as shock absorber.
-   Connective tissues protects vital organs.
-   Connective tissues pack organs
-   Connective tissues transports substances across the body.
-   Connective tissues fight with foreign toxins.

Types of connective tissues
---------------------------


-   Connective tissues are divided on the basis of
    -   type of matrix
-   The types of connective tissues are
    -   Connective Tissue Proper
    -   Loose Connective Tissue
    -   Dense Connective Tissue
    -   Hard Connective Tissue
    -   Fluid Connective Tissue

Connective Tissue Proper
========================

-   Connective tissue proper has a soft matrix.

Loose Connective Tissue
-----------------------

Areolar Tissue
--------------


-   Areolar tissue is a loose connective tissue.
-   Areolar tissue has a matrix chracterizing
    -   soft
    -   transparent
    -   jelly like  
-   The other term for areolar tissue is packing tissue.
-   Areolar tissue has non living fibres .
-   The fibres present in areolar tissue are loosely arranged.
-   The fibres present in areolar tissue are arranged in random manners.
-   **Areolae**
    -   The space between the fibres is called areolae.

### White Collagen fibres

-   White collagen fibres are white fibres.
-   White collagen fibres are arranged in bundles
-   White collagen fibres are arranged in a wavy manner.
-   White collagen fibres are unbranched.
-   White collagen fibres are tough
-   White collagen fibres are inelastic.
-   The protein in the white collagen fibres is collagen protein.

### Yellow elastic fibres

-   Yellow elastic fibres are long.
-   Yellow elastic fibres are branched.
-   Yellow elastic fibres are present singly.
-   Yellow elastic fibres are flexible.
-   Yellow elastic fibres are elastic.
-   The protein in the yellow elastic fibres is elastin protein.

### Reticular fibres

-   Reticular fibres are delicate fibres.
-   Reticular fibres are short fibres.
-   Reticular fibres are fine fibres.
-   Reticular fibres are thread like fibres
-   Reticular fibres form networks.
-   Reticular fibres have reticulin protein.

Types of cells in areolar tissue
================================

The cells in the areolar tissues are

-   Fibroblasts
-   Macrophages
-   Plasma Cell
-   Mast cell
-   Lymphocytes

Fibroblasts
-----------

-   Fibroblasts are fibre secreting cells
-   Fibroblasts are large in size
-   Fibroblasts have elongated protoplasmic processes
-   Fibroblasts have oval nucleus.
-   Fibroblasts secrete proteins.
-   The proteins secreted by fibroblasts are
    -   collagen
    -   elastin
    -   reticulin

Macrophages
-----------

-   The other term for macrophages is histocytes.
-   Histocytes are
    -   large
    -   irregular
    -   amoeboid shape
-   Macrophages have kidney shaped nucleus.
-   Macrophages are phagocytic.

Plasma cell
-----------

Small round cells having large cart wheel nucleus , hence called as
â€˜Cart wheel cellâ€™ They produce antibodies

Mast Cell
---------

Large oval cells which have granular cytoplasm Mast cell secretes
Heparin, Histamine and Serotonnin

### Heparin

-   Heparin is an anti coagulant.

### Histamine

-   Histamine is a vasodilator.
-   Vasodilator decreases blood pressure.
-   Histamine is secreted in allergic conditions.

### Serotonnin

-   Serotonin is a vasoconstrictor.
-   Vasoconstrictor increases blood pressure.

Lymphocytes
-----------

-   Lymphocytes are small cells.
-   Lymphocytes are amoeboid cells.
-   Lymphocytes act as scavangers
-   Lymphocytes by eat up the debris.
-   Lymphocytes eat up the foreign bodies

Location of areolar tissues
---------------------------

-   Beneath the dermis of skin
-   Between and around muscles
-   The blood vessels
-   The nerve fibres
-   The mesenteries in gastrointestinal tracts
-   The Peritoneum

Functions of areolar tissues
----------------------------

-   Areolar tissues are supportive.
-   Areolar tissues are packing tissues.
-   Areolar tissues heal wounds.
-   Areolar tissues heal inflammations.
-   Heparin prevents blood clotting.
-   Areolar tissues bind and connect tissues.
-   Areolar tissues destroy microbes.
-   Areolar tissues engulf foreign bodies.

Adipose tissue
==============



-   Adipose tissue is a modified areolar tissue.
-   Adipose tissue is also termed as a fat storing tissue.
-   Adipose tissue consists of large number of fat storing cells.
-   The fat storing cells of adipose tissue are called adipocytes.
-   The other term for adipocytes is lipocytes.
-   Adipocytes are modified fibrocytes.
-   Adipocytes are
    -   large
    -   oval
    -   spherical
    -   fat

Types of adipocytes
-------------------

-   White adipocytes
-   Brown adipocytes

### White adipocytes

-   White adipocytes contain single large fat droplet.
-   White adipocytes contain
    -   peripheral cytoplasm
    -   peripheral nucleus

### Brown adipocytes

-   Brown adipocytes contain several small fat droplets.
-   Brown adipocytes contain
    -   peripheral nucleus
    -   peripheral cytoplasm
-   Brown adipocytes contain fat from from excess food.

Location of adipose tissues
---------------------------

-   Beneath the skin as subcutaneous fat
-   Around kidneys and eyeballs
-   On the surface of heart
-   Mesenteries
-   Soles of feet
-   Buttocks
-   Hump of camel
-   Blubber of whales

Functions of adipose tissues
----------------------------

-   Adipose tissue is a reservoir of fat.
-   Adipose tissue gives mechanical protection.
-   Adipose tissue acts as shock absorber around
    -   kidneys
    -   heart
    -   soles of the feet
    -   buttocks
-   Adipose tissue prevents heat loss.
-   Adipose tissue acts as cushion
    -   in eye socket

Facts on fats
-------------

-   The fat is yellow in colour due to
    -   lipochrome pigment
-   The brown fat is brown in colour due to
    -   iron rich mitochondria
    -   containing cytochrome pigment
-   The adipose tissue are not found in
    -   lungs
    -   eyelids
    -   ear penis
    -   and dorsum of hand
-   Brown fat can yield 20 times more energy than white fats.

Dense Connective Tissue
=======================

-   Dense connective tissue has compactly arranged fibres.

Types of dense connective tissues
---------------------------------


-   White fibrous tissue
-   Yellow Elastic Tissue
-   Reticular Tissue

White Fibrous Tissue
--------------------

-   White fibrous tissue is made up of the white collagen fibres.
-   White collagen fibres are tough.
-   White collagen fibres are inelastic.
-   White fibrous tissue forms tendon.
-   Tendon connects the muscles with bones.

Location of white fibrous tissues
---------------------------------

-   The Sclera of the eyeball
-   The Cornea of eyeball
-   The perichondrium of Cartilage
-   The periosteum of bone
-   Between the skull bones
-   Duramater of the brain
-   The spinal cord
-   The pericardium of heart
-   The kidney capsule
-   The dermis of skin
-   The lymph nodes

Functions of white fibrous tissues
----------------------------------

-   Tendon connects the muscles to bones
-   White fibrous tissue provides mechanical protection.
-   White fibrous tissue protects vital organs like
    -   brain
    -   spinal cord
    -   heart
    -   kidney

Yellow Elastic Tissue
---------------------

-   Yellow elastic tissue is made up of yellow elastic fibres
-   Yellow elastic fibres are elastic.
-   Yellow elastic tissue forms ligaments.

Location of yellow elastic tissue
---------------------------------

-   The pinna
-   The alveoli
-   The arterial wall
-   The epiglottis
-   The dermis of skin

Functions of yellow elastic tissues
-----------------------------------

-   Yellow elastic tissue stretches the body organs.
-   Ligament connects bone to bone
-   Ligament connects cartilage to cartilage

Structure of Reticular Tissue
=============================


-   Reticular tissue is a modified areolar tissue.
-   Reticular tissue is made up of reticular fibres.

Location of reticular tissues
-----------------------------

-   Around the muscle fibres
    -   The term for reticular tissues around the muscle fibres is
        Sarcolemma.
-   Around nerve fibres
    -   The term for reticular fibres around the muscle fibres is
        Neurilemma.
-   The lymph glands
-   Tonsils
-   Liver
-   Kidney

Function of reticular tissues
-----------------------------

-   Reticular tissue act as delicate supporting network

Hard Connective Tissue
======================

-   Hard connective tissue has a hard matrix.
-   The other term for hard connective tissue is skeletal tissue.
-   The types of hard connective tissue are
    -   cartilage
    -   bones

Cartilage
=========

-   Cartilage is a hard connective tissue .

Terminologies related to cartilage
----------------------------------

### Chondrology

-   Chondrology is the study of cartilages.

### Chondrogenesis

-   Chondrogenesis is the process of formation of cartilage.

### Perichondrium

-   The perichondrium is the outer covering of the cartilage.
-   The perichondrium is made up of white fibrous tissue.

### Chondrin

-   Chondrin is a protein.
-   Chondrin is present in the matrix of cartilage.

### Chondroblast

-   Chondroblast cells form the cartilage.

### Chondrocyte

-   Chondrocytes are inactive and mature cells.
-   Chondrocytes form the cartilages.
-   Chondrocytes are enclosed in the lacuna.

### Lacuna

-   Lacuna is a fluid filled cavity.
-   The number of chondrocytes in the lacuna is :
    -   2-6

Characteristics of Cartilage
----------------------------

-   Cartilage has cheese like matrix.
-   The cells of the cartilage are scattered in the matrix.
-   The cartilage has
    -   collagen fibres in the matrix
    -   the elastin fibres in the matrix.
-   The cartilage is surrounded by a sheath of white fibrous tissue.
-   The cartilage is avascular.
-   The nutrients in the cartilage are obtained in the cells by
    diffusion.
-   The blood vessels do not grow into cartilage
    -   The chondrocytes in the cartilages produce a chemical
        anti-angiogenesis factor.
-   The direction of growth in cartilage is uni directional.

Types of Cartilage
==================


-   The types of cartilages are divided on the proportion of fibres
    present in the matrix.

-   The types of cartilages are divided on kinds of fibres present.

-   The types of cartilages are:

    -   Calcified cartilage
    -   Hyaline cartilage
    -   Elastin cartilage
    -   White fibrous cartilage

### Nature of Calcified Cartilage

-   Calcified cartilage is formed by the calcification of hyaline
    cartilage.
-   The calcified cartilage is
    -   hard
    -   inelastic
-   The calcified cartilage is hard and inelastic because
    -   The calcium in calcified cartilage deposits in the matrix.

### Examples of calcified cartilage

-   The head of humerus of frog
-   The head of femur of frog

### Nature of Hyaline Cartilage

-   Hyaline cartilage is termed as transparent tissue.
    -   The hyaline cartilage is termed as transparent tissue because it
        has glass like matrix.
-   Hyaline cartilage has no fibres in it.
-   Hyaline cartilage is the most common type of cartilage.

### Location of Hyaline Cartilgae

-   The vertebrate embryos
-   The cartilageneous fishes
-   The ends of long bones,
-   The nasal bones
-   The ribs
-   The larynx
-   The trachea
-   The knee cap

### Nature of Elastic Cartilage

-   Elastic cartilage has elastic fibres.
-   Elastic cartilage is elastic in nature.

### Location of elastic cartilage

-   Ear Pinna
-   Epiglottis
-   Eustachian tube

### Nature of White Fibrous Cartilage

-   White fibrous cartilage lacks perichondrium.
-   White fibrous cartilage has collagen fibres.
-   White fibrous cartilage is the most strongest cartilage.

### Examples of white fibrous cartilage

-   The pubic symphysis
-   The intervertebral discs
-   The acetabulum
-   The glenoid cavity

Introduction to Bone
====================

-   **Bone** is a hard connective tissue.

-   **Bone** has hard *matrix* .

Constitution of bone
--------------------

-   The **constituting** components of bone are

    -   **Inorganic Salts**

        -   *Calcium* *Magnesium* and *Phosphate*

        -   **Calcium Hydrooxypatite**

            -   This causes **hardness** of *bone* .

    -   **Collagen Fibers** $33\%$

        -   This **provides** **tensile strength** to the *bone* .

    -   **Cells**

        -   **Osteocytes**

        -   **Osteoblasts**

        -   **Osteoclasts**

    -   **Protein**

        -   Bone contains protein called **ossein** .

Features of bone
----------------

-   **Bone** shows **bidirectional** growth.

-   **Bone** grows **brittle** with increasing *age* .

    -   This occurs due to *decrease* of *protein* in the matrix.

Bone Factors
============

Hormones
--------

-   **Parathormone**

    -   This is secreted by *parathyroid* gland.

    -   This **increases** blood *calcium* .

    -   **Calcium** is drawn from *bone* to *plasma* .

-   **Calcitonin**

    -   This is secreted by *thyroid* gland.

    -   This **decreases** blood **calcium** .

    -   **Calcium** is drawn from **blood** to **bone** .

Vitamins
--------

-   **Vitamin D**

    -   This vitamin is also known as **Calciferol** .

    -   This vitamin is needed for **normal** growth and **development**
        of *bone* .

    -   This vitamin has a role in **calcium phosphate** metabolism.

    -   **Difiecieny** of this vitamin causes **rickets** .

Functions of bone
=================

-   The **functions** of *bone* are *illustrated* below

    -   **Support**

    -   **Framework**

    -   **Movement of body**

    -   **Protection**

    -   **Calcium resorvior**

    -   **Blood Formation**

    -   **Fat storage**

Bone Terminologies
==================

-   **Osteology**

    -   **Osteology** is the *study* of *bones* .

-   **Ossification**

    -   **Ossification** is also termed as **Osteogenesis** .

    -   **Ossification** is the **process** of *formation* of *bone* .

-   **Periosteum**

    -   **Periosteum** is the outer **covering** of the bone.

    -   **Histological Structure**

        -   **Periosteum** is made up of **white fibrous tissue.**

    -   **Contents** **Periosteum** contains

        -   **Osteogenic** cells

        -   **Osteoclast** cells

-   **Lamellae**

    -   **Lamellae** is the **matrix** of *bone* .

    -   **Location** Between **Periosteum** and **Endosteum**

    -   **Arrangement** *Concentric* rings

-   **Ossein**

    -   **Ossein** is *protein* .

    -   **Location** **Matrix** of the *bone* .

-   **Osteoblast cells**

    -   **Type** **Osteoblast** cells are *active* .

    -   **Location** **Osteoblasts** are **located** *below*
        **periosteum**

    -   **Function** **Osteoblast** cells form **bones** .

-   **Osteocyte cells**

    -   **Type** **Inactive**

    -   **Age** **Mature**

    -   **Location** **Lacuna**

    -   **Function** **Osteocyte** cells form **bones** .

-   **Osteoclast cells**

    -   **Size** **Osteoclast cells** are **large** .

    -   **Type of nucleus** **Multinucleated**

    -   **Cell type** **Phagocytotic** **Location** **Osteoclast** cells
        are located in the *periosteum* . **Function**

        -   

        -   **Osteoclast** cells **reabsorb** *matrix* of the bone.

        -   **Osteoclast** cells **remould** *bone* .

        -   **Osteoclast** cells produce *enzymes* .

            -   **Enzymes** of *osteoclast* cells *demineralize* the
                **matrix** .

-   **Lacuna**

    -   **Anatomical structure** **Lacuna** is a *cavity* .

    -   **Contents**

        -   **Lacuna** contains *Single* **osteocyte** cell

        -   **Osteocyte** cells have **cytoplasmic processes** .

        -   The **cytoplasmic** processes of **osteocytes** are
            *fingerlike* .

-   **Canliculi**


    -   **Anatomical Structure** **Canaliculi** is a *canal* .

    -   **Function**

        -   **Passes** *fingerlike* processes of *osteocyte cells* .

            -   **Fingerlike** processes drive *nutrients*.

            -   **Nutrients** are derived from *neighbouring cells* /

-   **Endosteum**


    -   **Location** **Endosteum** lines the *layer* of **marrow
        cavity** .

    -   **Contents**

        -   **Endosteum** is made up of *layer* of *osteoblast cells* .

-   **Haversian canal**


    -   **Location**

        -   **Haversian** canal is present in *mammalian* *compact*
            **bone** .

    -   **Anatomical Structure** **Haversian** canal is a **canal** .

    -   **Shape**

        -   Haversian canal is *longitudinal* .

        -   Haversian canal is *cylindrical* .

    -   **Contents**

        -   **Blood Vessels**

        -   **Lymph vessels**

        -   **Nerves**

-   **Compact Bone**


    -   **Location**



        -   **Compact Bone** is located at the **shaft** of *long*
            bones.

    -   **Contents**

        -   **Compact bone** has *yellow* bone marrow.

            -   **Yellow** bone marrow produces **WBC’s** .

            -   **Yellow** bone marrow **stores** fat.

    -   **Feature** **Compact** bone is

        -   *Hard*

        -   *Compact*

        -   *Strongy*

-   **Spongy Bone**


    -   **Spongy Bone** is also *called* **Cancellous bone** .

    -   **Shape**

        -   **Spongy Bone** has the appearance of a **honey comb** .

    -   **Location** **Spongy bones** is found at *end of*

        -   *Long* bones

        -   *Flat* bones

    -   **Contents**

        -   **Spongy bone** contains *red bone marrow.*

            -   **Red bone marrow** produces

                1.  **Red** blood *cells*

                2.  **White** blood cells

    -   **Feature** **Spongy bone** is

        -   **Hard**

        -   **Spongy**

-   **Haversian System**



-   **Haversian** system is also called **osteon** .

    -   **Anatomical Structure**

        -   **Center** **Haversian system** has **central** haversian
            canal.

        -   **Peripheriry** **Haversian system** has *peripheral*
            *lamella* .

-   **Volkman’s canal**


    -   **Anatomical structure** **Volkman’s** canal is a **canal** .

    -   **Arrangement in plane** **Volkman’s** canal is a **horizontal**
        canal. **Function**

        -   **Volkman’s canal** connects two **haversian** canals.

-   **Bone Marrow**

    -   **Material nature**

        -   **Bone** marrow is a substance which is

            -   **Soft**

            -   **Pulpy**

    -   **Location**

        -   **Bone marrow** is located at the **marrow cavity** .

    -   **Types** The types of *bone* marrow are

        -   **Red** bone marrow

        -   **Yellow** bone marrow

-   **Decalcification**

    -   **Decalcification** is the *removal* of *hardness* of bone.

        -   **Decalcification** is done by treating with $HCl$ .

-   **Decalcified bone**

    -   **Decalcified** bone is a *bone* without hardness.

Structure of $T.S.$ of a decalcified bone
=========================================


-   The structure if $T.S.$ of a decalcified bone is

    -   **Periosteum**

    -   **Outer** layer of **osteoblast** cells

    -   **Lamella**

    -   **Marrow Cavity**

Structure of trabeculae
-----------------------

-   The spongy bone is formed up of
    -   The networks of trabeculae
-   The trabeculae is
    -   cluster of structures
-   The trabeculae is surrounded by the marrow cavity.

Ossification
============

-   Ossification is the process of formation of bone.

Types of ossification
---------------------

The types of ossification are

-   Endochondral ossification
-   Intramembranous ossification

Nature of Endochondral ossification
-----------------------------------



-   Endochondral ossification is the transformation of a cartilage into
    a bone.
-   The cartilage transforming to a bone is called replacing bone.

Examples of Endochondral ossification
-------------------------------------

-   The long bones
-   The ribs
-   The vertebra

Nature of Intramembranous ossification
--------------------------------------


-   Intramembranous ossification is the ossification in the connective
    tissue.

Examples of Intramembranous ossification
----------------------------------------

-   The facial bones
-   The skull bones
-   The clavicle

Ossification in tendon
----------------------

-   Sesamoid bone is formed by ossification in tendon.

### Examples of ossification in tendon

-   Patella

Fluid connective tissue
=======================

-   Fluid connective tissue circulates within the vessels.
-   The other term for fluid connective tissue is circulating tissue.

Contents of fluid connective tissue
-----------------------------------

-   Fluid matrix
-   Scattered cells
-   No visible fibers
    -   Fibres are only seen when blood clots.

Types of fluid connective tissue
--------------------------------

-   Blood
-   Lymph

Blood
=====


-   The other term for blood is pseudo connective tissue
    -   Blood lacks fibres
    -   The matrix of the blood is formed by liver.
    -   The cells of blood are formed by
        -   Yolk sac
        -   Liver
        -   Spleen
        -   Red bone marrow

Haemopoiesis
------------

-   The other term for haemopoiesis isi haematopoiesis.
-   Haematopoiesis is the process of formation of blood cells.

Chemical property of blood
--------------------------

-   Blood is slightly alkaline.
-   The pH value of blood is
    -   7.4
-   The pH of blood is maintained by balancing the ratio of
    -   sodium bicarbonate
    -   carbonic acid

Amount of blood
---------------

-   An adult human has
    -   5 to 5.5 litres of blood.
-   Blood constitutes about
    -   8% of total body weight.

Functions of Blood
------------------

-   Blood transports
    -   nutrients
    -   oxygen
    -   carbon dioxide
    -   hormones
    -   unwanted waste products
-   Blood protects.
-   Blood conducts thermo regulation

Composition of blood:
---------------------

### Blood Plasma

-   Blood plasma are
    -   straw colored
    -   fluid
-   Blood cells are suspended in the plasma.

### Amount of blood plasma

-   Blood plasma occupies a blood volume of
    -   55

Contents of blood plasma
------------------------

### Water

-   The amount of water present in blood plasma is
    -   90 – 92

### Inorganic salts

-   Sodium chloride
    -   Sodium chloride is the primary salt of blood.
    -   Amount of sodium chloride present is
        -   0.9
-   Sodium bicarbonate
-   Potassium
-   Magnesium
-   Phosphorus
-   Iron
-   Calcium
-   Copper
-   Chlorine
-   Iodine

### Organic waste materials

-   Organic waste materials are also non protein nitrogenous substances
-   The organic waste materials in blood are ;
    -   uric acid
    -   creatinine
    -   hippuric acids

Plasma proteins
---------------


-   The quantity of plasma proteins present in blood is
    -   7
-   The types of plasma proteins in blood are
    -   Albumin
    -   Globulin
    -   Fibrinogen

### Albumins

-   Albumins are the most abundant plasma proteins
-   Albumins are responsible for
    -   Collidal Osmotic Pressure
-   Albumins are the only protein reserve of body
-   Albumins are hydrophilic in nature

### Globulins

-   Globulins are of three types.
-   The types of globulin are
    -   alpha
        -   Alpha globulin is synthesized in liver.
    -   beta
        -   Beta globulin is synthesized in liver.
    -   gamma
        -   Gamma globulins are formed by the plasma cells.

### Antibodies

-   Antibodies in plasma are immune-globulins.
-   The antibodies are produced by
    -   lymph nodes
    -   spleen

### Hormones and respiratory gases

### Clotting factors

-   Fibrinogen
    -   Fibrinogen is formed in liver.
-   Prothrombin
    -   Prothrombin is formed in liver.

Functions of Plasma:
--------------------

-   Plasma transports
    -   nutrients
    -   respiratory gases
    -   excretory wastes
    -   hormones
-   Immuno globulins in plasma provide immunity.
-   Plasma conducts thermoregulation.
-   Plasma maintains osmotic pressure.
    -   Albumin maintains osmotic pressure holding waters
-   Plasma maintains blood pH
    -   Plasma proteins neutralize
        -   strong acids
        -   strong bases

Blood Corpuscles:
=================

RBC
---

-   The other term for RBC is Erythrocytes.

Structure of RBC
----------------

-   RBCs are
    -   circular
    -   biconcave
    -   non nucleated
        -   The absence of nucleus increase the surface area.
        -   The increased surface area is beneficial for oxygen
            transportation.
        -   The increased surface area can accommodate maximum number of
            Haemoglobin.
-   The diameter of RBC is
    -   7.5 micrometers

Contents of RBC
---------------


-   RBC contains Haemoglobin pigment
    -   The amount of space consumed by haemoglobin is
        -   33
    -   The amount of haemoglobin molecules present per RBC molecules is
        -   280 millions
    -   Iron is present in Haemoglobin
    -   The amount of RBC in 100 ml of blood is
        -   15 gms of Hb.

### Quantity of RBC

-   The number of RBC per cubic mm of blood in female is
    -   4.5 to 5 millions per cubic mm blood
-   The number of RBC per cubic mm of blood in male is
    -   5 to 5.5 millions per cubic mm blood

### Rate of production of RBC

-   The rate of production of RBC is
    -   2 millions per second

### Lifespan

-   The average lifespan of RBC is
    -   120 days

### Erythropoiesis

-   Erythropoietin is a hormone.
-   Erythropoietin is secreted by liver in foetus.
-   Erythropoietin is secreted by kidney in adults.
-   Erythropoietin begins erythropoiesis.

Formation of RBC
----------------


-   RBCs are formed in Red Bone marrow.
-   The RBCs are formed from haemopoietic tissue.
-   The RBCs are formed at early foetal life.
-   The RBCs are formed in the yolk sac in foetal life.
-   The RBCs are formed later in liver.
-   The major site of haemopoietic activity from third to seventh months
    is
    -   spleen
-   The major site of haemopoiesis from birth to whole life is
    -   red bone marrow

Destruction of RBC
------------------

-   Haemolysis is the process of destruction of RBC.
-   Haemolysis occurs in liver.
-   RBC in broken into
    -   plasma membrane
    -   haemoglobin

### Haemoglobin

Haemoglobin is broken into 

- iron 
    + Iron is retained by liver.

- protein
        + Incomplete metabolism of protein forms 
            + bilirubin
            + bilivirdin
            + stercobilin
            + urochrome
        
        + Bilirubin and bilivirdin are bile pigments. 
        + Strcobilin give color to feaces.
        + Urochrome gives color to urine.

### Plasma membrane

-   Plasma membrane of RBC is destroyed in
    -   spleen.
-   Spleen is the graveyard of RBC.

Disorders related to RBC
------------------------

### Anaemia

-   Anaemia is the lack of abundant RBCs in blood .

### Polycythemia

-   Polycythemia is the presence of
    -   abnormally large number of RBCs in blood
-   Polycythemia increases the blood viscosity.
-   Polycythemia increases the risk of intravascular clotting

### Pernicious anaemia

-   Pernicious anaemia is the development of immature RBC.
-   Pernicious anaemia occurs due to the deficiency of
    -   Vitamin B12

Facts about RBC
---------------

### Rouleaux formation


-   Rouleaux formation occurs when blood is mixed with anti coagulant.
-   RBCs join together by their concave surfaces.
-   The appearance is like the piles of coins.

### Haemolysis


-   Haemolysis is the bursting of RBC.
-   Haemolysis occurs when blood is mixed with distilled water.
-   Distilled water is also called as hypnotic solution.

### Shrinking of RBC

-   RBCs shrink when blood is kept in NaCl solution.
-   NaCl solution is also called as hypertonic solution.
-   The amount of NaCl for RBCs to shrink is
    -   8%

Formation of RBCs
-----------------


-   RBCs are followed after the formation of reticulocytes.
-   Vitamin B12 is essential for maturation of reticulocytes into RBC.
-   Reticulocytes
    -   are nucleated.
    -   posses cell organelles.
-   RBC lacks mitochondria.
-   RBC undergoes anaerobic respiration.
-   Anaerobic respiration releases lactic acid.
-   Release of Lactic acid is the cause of fatigue.

Facts about RBC
---------------

-   The maximum number of RBC count per unit volume of blood is in
    -   aves
-   The largest RBCs are present in
    -   Amphibians
    -   The size of RBC in amphibians is
        -   80 micro meters
-   The smallest RBCs are present in
    -   Musk deer
-   Animals having nucleated RBCs are
    -   Camel
    -   Llamas

White Blood Cells
-----------------

-   The other term for white blood cells is leucocytes.

-   WBCs are the largest blood corpuscles.

-   The diameter of WBCs is

    -   8 to 15 micrometers

-   WBCs are nucleated.

-   WBCs are amoeboid.

-   WBCs may posses shape of

    -   round  
    -   irregular

-   WBCs are non pigmented.

-   WBCs have the power of amoeboid movement

### Amount of WBC

-   The amount of RBC per cubic mm of blood is
    -   8,000 - 10,000 per cubic mm of blood

### Average lifespan

-   The average lifespan of WBCs is
    -   10 to 13 days

Terms related to white blood cells
----------------------------------

### Diapedesis

Diapedesis is the movement of WBC across the blood vessels.

### Formation of WBC

-   WBCs are formed in
    -   bone marrows
    -   lymph glands

Types of WBC
------------


-   There are two types of WBC.
-   The WBC are characterized on
    -   presence of granules
    -   type of nucleus
-   The types of WBC are
    -   Agranulocytes
    -   Granulocytes

Granulocytes
------------

-   Granulocytes have cytoplasmic granules
-   Granulocytes have multilobed nucleus. .
-   The types of granulocytes are
    -   neutrophils
    -   eosinophils
    -   basophils
-   Granulocytes respond to the dyes in laboratory.

### Eosinophils



-   The dye taken by eosinophils is
    -   red acidic dye
    -   The other term for red acidic dye is eosin.
    -   Eosinophils take red acidic dye due to
        -   detoxification

### Basophils


-   The dye taken by basophils is
    -   alkaline methylene blue
    -   Basophils take alkaline methylene blue due to
        -   heparin
        -   histamine
        -   serotonin

### Neutrophils


-   The dye taken by neutrophils are

    -   purple
        -   red acidic eosin
        -   alkaline methylene blue

-   Neutrophils are phagocytic

-   Neutrophils are the most abundant WBC.

-   The amount of WBC formed form neutrophils is

    -   60-70 %

Agranulocytes
-------------


-   Agranulocytes lack granules.
-   Agranulocytes donot have
    -   multi lobulated nucleus.
-   The types of agranulocytes are
    -   lymphocyte
    -   monocyte

### Monocytes


-   Monocytes are the largest WBC.
-   The size of monocytes is
    -   20 micrometers
-   Monocytes are phagocytotic.

### Lymphocytes


-   Lymphocytes are the smallest WBCs.
-   The size of lymphocytes is
    -   7 micrometers
-   Lymphocytes are phagocytic.

Disorders of WBC
----------------

### Leukemia

-   Leukemia is also called blood cancer.
-   Leukemia occurs due to
    -   excessive formation of WBC

### Leucopenia

-   Leucopenia is the condition of
    -   abnormally low number of WBC

Platelets
=========


-   The other term for platelets is thrombrocytes.
-   The role of platelets is
    -   to clot blood

### Amount of platelets

-   The number of platelets per cubic mm of blood is
    -   2 to 4 lakhs per cubic mm.

### Dimensions of platelets

-   The diameter of platelets is
    -   2 - 3 micrometers
-   Platelets are the smallest blood corpuscles


### Lifespan of platelets

-   The lifespan of platelets is
    -   about a week.

### Formation of Thrombocytes

-   The process of formation of thrombocytes is
    -   Thrombopoiesis

Disorders of thrombocytes
-------------------------

### Thrombocytosis

-   The increase in the number of platelets is Thrombocytosis .
-   Thrombocytosis causes intravascular clots.

### Thrombocytopenia

-   The decrease in the number of platelets is Thrombocytopenia.
-   Thrombocytopenia causes internal bleeding.

Contents of thrombocytes
------------------------

-   Thrombocytes contain
    -   clotting factors
-   The clotting factors of thrombocytes promote blood clotting.
-   The clotting factors are
    -   Thromboplastin
        -   The other term for thromboplastin is thrombokinase.
    -   Prothrombin
    -   Fibrinogen
    -   Calcium ions

Blood coagulation
-----------------


-   Blood coagulation is the mechanism of prevention of blood loss.
-   Blood coagulation occurs when
    -   a blood vessel is ruptured
-   Blood clotting stops haemorrhage.

### Time for blood coagulation

-   The time required for blood clot is from
    -   2 minutes
    -   8 minutes

Blood clotting
==============


The process of clotting of blood is


- release of enzymes
- formation of thrombin 
- formation of fibrin
- clotting of blood

Release of enzymes
------------------

-   The damaged platelets releases enzymes.
-   The enzymes released by damaged platelets are
    -   Thromboplastin
    -   Thrombokinase.

Formation of thrombin
---------------------


-   Prothombrin is converted into thrombin.
-   The conversion of prothrombin into thrombin takes place in the
    presence of
    -   thrombokinase
    -   calcium ions

Formation of fibrin
-------------------

-   Fibrinogen is converted into fibrin.
    -   Fibrin is a fibrous material
    -   Fibrin is converted as an insoluble networks.
-   The conversion is done by
    -   thrombin

Blood clot
----------

-   Fibrin traps the blood cells.

-   Fibrin forms a red solid mass of blood cells.

-   The red solid mass of blood cells is called blood clot.

-   The blood clot acts like a seal in

    -   the ruptured blood vessel

Terms related to blood clotting
===============================

### Serum

-   Serum is the blood plasma minus clot.
-   Serum is a pale yellow fluid.


### Thrombosis

Thrombosis is the clotting of blood in an unbroken blood vessel.


### Thrombus


Thrombus is the clot lodged in a vessel.


### Minerals

-   The mineral necessary for coagulation of blood is
    -   Calcium
-   The vitamin necessary for synthesis of clotting factors is
    -   Vitamin K

### Heparin


-   Coagulation of blood in vessels is prevented during the normal
    circulation by
    -   heparin
-   Heparin inhibits conversion of
    -   prothrombin into thrombin
-   This is done by activating
    -   antithrombin in blood

Storage of Blood
================
-   Blood clotting is prevented in by adding
    -   oxalate
    -   citrate
-   Oxalate or citrate react with
    -   calcium
-   The reaction of oxalate or citrate with calcium forms
    -   insoluble compound
-   The free calcium ions for clotting of blood are absent.

### Temperature for storing blood

-   Blood is stored at
    -   4 degrees Celsius.


# Lymph


## Location of lymph

Lymph is located at interstitial spaces

## Opacity of lymph

Lymph is transparent

## Colour of lymph

The colour of lymph may be


- Colourless
- Faint Yellow


## Chemical nature of lymph on the basis of three classes


Lymph is


- Slightly alkaline


## Contents of lymph

The contents of lymph are
 
 - Matrix
 - Cells
 - Gases
 - Substances
 - Protein

#### Matrix of the lymph

The matrix of the lymph is


- Plasma


#### Cells of the lymph

The cells present in lymph are


- Lymphocytes


#### Gases in lymph 

The gases present in the lymph are


- Oxygen
- Carbon dioxide


#### Substances present in lymph

The substances present in the lymph are


- Urea
- Glucose
- Vitamins
- Salts



### Quantity of protein in lymph

The quantity of protein in lymph is


- Very less





# Lymphatic system

## Division of lymphatic system

The division of lymphatic system are


- Lymphatic ducts
- Lymphatic vessels
- Lymph nodes
- Lymphatic capillaries


### Formation of lymphatic vessels

The formation of lymphatic vessels occurs by


- Union of lymphatic capillaries


### System for production of lymph fluid

The system producing the contents of lymph and lymph fluid is


- Lymphatic system




# Lymph nodes


#### Shape of lymph nodes

The shape of lymph nodes is

- Oval shaped
- Bean shaped
- Kidney shaped

#### Location of lymph nodes

The location of lymph nodes in lymphatic system is


- Lymphatic vessels



## Function of lymph nodes


The functions of lymph nodes are


- Lymph nodes filter.
- Lymph nodes produce lymphocytes.




#### Cells produced by lymph nodes

The cells produced by the lymph nodes are


- Lymphocytes



### Structure having valve in lymphatic system

The structure having valve in lymphatic system are


- Lymphatic vessels


### Need for valve  in lymphatic vessels

The need for valve in lymphatic vessels is

- Prevention of backward flow of lymph

# Movement of lymph

## Structural factors in movement of lymph

The structural factors in movement of lymph are


-  Skeletal muscles

## Process of movement of lymph in lymphatic system

The process of movement of lymph in lymphatic system is


- Squezzing of surrounding muscles


# Major lymphatic ducts


The major lymphatic ducts are


- Right Lymphatic duct
- Thoracic duct



## Location of right lymphatic duct


The location of right lymphatic duct is

- Right sub clavian vein


## Role of right lymphatic ducts


The role of right lymphatic ducts is to


- Collect lymph from  right portion of the body



## Location of thoracic duct

The location of left thoracic duct is

- Left sub clavian vein

## Role of thoracic duct

The role of thoracic duct is

- Collect  lymph from left portion of the body


## Direction of flow of lymph 

The direction of flow of lymph is


- Unidirectional


## Final destination of flow of lymph

The final destination of flow of lymph is

- Venous blood system

# Functions of lymph


The functions of lymph are


- Transportation
- Phagocytosis
- Blood volume



## Transportation

The materials transported by lymph are 


- Gases
- Substances


#### Gases

Lymph transports 


- Respiratory gases


#### Substances transported by lymph

The substance transported by lymph are


- Food materials
- Hormones



## Phagocytotic nature of lymph

The phagocytotic nature of lymph is


- Lymph destroys pathogens
- Lymph destroys foreign particles



#### Structure for phagocytosis in lymph

The structure of phagocytosis in lymph are


- Lymph nodes


## Balance of blood volume


#### Condition of transfer of blood from lymphatic system to blood vascular system

The condition of transfer of blood from lymphatic system to blood vascular system is

- Decrement of volume of blood in blood vascular system

# Lacetals

## Anatomy of lacteals

Lymph Capillaries

## Location of lacteals

Intestinal villi


## Function of lacteals

Absorption

## Substances  absorbed by lacetals

The components absorbed by lacetals are

- Fat soluble vitamins
- Fats




# Disorders of lymph



The disorder of lymph is called

- Oedema

### Location of oedema in disorders of lymph

The location of action of oedema in disorders of lymph is


- Around the cells


### Result of oedema in disorders of lymph

The result of oedema in disorders of lymph is


- Swelling


### Term for oedema in disorders of lymph

The other term for oedema in disorders of lymph is


- Ordropsy


# Muscular tissue



### Term for study of muscles

Myology



### Strength of muscles 

Strong


### Type of muscular tissues in terms of contraction 



Contractile tissues



### Source of flesh in the body 


Muscles


### Nature of origin of muscles 


- Mesodermal
- Ectodermal



### Organs having muscles of mesodermal origin 


- Almost muscles of all organs



### Organs having muscles of ectodermal origin


- Mammary gland
- Sweat gland





### Features of muscles 


- Excitability
- Contractility
- Extensibility
- Elasticity



# Functions of Muscles



- Shape
- Locomotion
- Facial Expression
- Mastication
- Heart Beat





### Actions conducted by involuntary muscles 


- Respiration
- Peristalsis
- Propulsion of urine




# Types of muscles



- Voluntary
- Involuntary
- Cardiac





# Voluntary muscles: 


### Terms for voluntary muscles 


- Skeletal
- Striped
- Striated



### Nature of voluntary muscles on the basis of control according to will 


- Controllable




### Fatigueness of voluntary muscles 


- Get easily tired




### Arrangement of muscles with skeleton in voluntary muscles 


- Attached with skeleton




### Structure for attachment of muscles with skeleton in voluntary muscles



- Tendon


### Source of muscle fibre cells in voluntary muscles 



- Myoblast



### Nature of voluntary muscles in terms of division  


- Cannot divide on their own



### Location of voluntary muscles 


- Limb muscles
- Facial muscles
- Tongue
- Facial muscles
- Eye Muscles
- Abdominal Muscles





### Functions of voluntary muscles 


- Movement
- Chewing
- Facial expression
- Posture




# Structure of voluntary muscles



### Branching of voluntary muscles

Unbranched

### Shape of muscles in voluntary muscles

Cylindrical

### Structure of end of voluntary muscles 

Blunt



### Binding structure of muscle fibre in voluntary muscles


Sarcolemma



### Cytoplasm of muscle fibres of voluntary muscles 

Sarcoplasm



### Location of nucleus in muscle fibres of voluntary muscles

Periphery


### Type of cells of muscle fibres of voluntary muscles in terms of number of nucleus

Multinucleated



### Type of origin of cells of muscle fibres on the basis of arrangement of cells

Syncytial

### Source of formation of cells of muscle fibres of voluntary muscles


Myoblasts


### Nature of syncytial origin

Fusion of multiple cells




### Contents of sarcoplasm in voluntary muscles



- Syncytial nucleus
- Sarcoplasmic Reticulum
- Muscle glycogen
- Myoglobin



### Term for cytoplasm in voluntary muscles 


- Sarcoplasm


### Term for endoplasmic reticulum in sarcoplasm  in voluntary muscles


- Sarcoplasmic reticulum




### Contents of sarcoplasmic reticulum in sarcoplasm in voluntary muscles


- Calcium



### Function of myoglobin in voluntary muscles in voluntary muscles

- Store oxygen 



### Function of presence of oxygen in myoglobin  in voluntary muscles 


- Production of ATP





### Colour of muscle fibre in voluntary muscles

Deep red


### Cause of deep red coloration of muscle fibre  in voluntary muscles

Myoglobin pigment



### Contents of myofibril  in voluntary muscles


- Light bands
- Dark bands



### Contents of Bands of myofibril in voluntary muscles 


- Myofilaments



### Types of myofilaments in voluntary muscles


- Thick filament
- Thin filament


### Contents of myofilaments in voluntary muscles


Protein

### Cause of name of striated muscles in voluntary muscles



Light and Dark Bands



## Terms for  Dark bands in voluntary muscles

- Anisotropic band
- A band




### Contents of dark bands in voluntary muscles

Thick filament


### Protein present in dark bands of voluntary muscles 

Myosin



### Line of bisection for dark bands of voluntary muscles

Hensen's line


### Term for hensen's line in voluntary muscles 

H line




## Terms for  Light band  in voluntary muscles 



- Isotropic band

- I band




### Contents of light bands in voluntary muscles 


Thin filaments


### Protein present in light bands of voluntary muscles

Actin


### Line of bisection in light bands in voluntary muscles 

Z line



### Term for Z line in light band of voluntary muscles 

Krause's membrane




### Function of Z line in voluntary muscles 

- Divide myfibril



### Sacromere in voluntary muscles 

- Functional unit of skeletal muscles




### Location of sacromere in voluntary muscles 


Myofibril




### Range of location of sacromere in voluntary muscles 

- Between Z lines



### Length of sacromere in voluntary muscles 

2.5 micrometer




### Mechanism of contraction of voluntary muscles 

Slide actin over myosin




### Approximation of length of sacromere during muscle contraction in voluntary muscles 

Short



### Destination of Z lines in muscles contraction of voluntary muscles 

A band


### Ions mediating the contraction of voluntary muscles 


Calcium

### Proteins present at actin


- Troponin
- Tropomyosin



### Location of proteins present at actin 



- Surface


### Function of troponin and tropomyosin  


- Block actin and myosin binding


### Function of calcium ions in muscle contraction of voluntary muscles 


- Bind Troponin





### Consequence of binding of  troponin in muscle contraction


- Change configuration of tropomyosin



# Activities in myosin in contraction of voluntary muscles


- Breakdown of ATP
- Formation of Bridge
- Contraction of muscles



## Breakdown of ATP

### Reactants in release of energy in contraction of voluntary muscles 

ATP



### Products in release of energy in contraction of voluntary muscles 

- ADP
- Phosphate


### Location of breakdown of ATP in muscle contraction 

Myosin 

## Formation of bridge 


### Structure between myosin and acting in contraction of voluntary muscles 

- Cross bridge


### Function of myosin in contraction of voluntary muscles 

- Pull actin



## Contraction of muscle 


- Shorten fibre




### Product in contraction of muscles in extrenous exercise 

Lactic acid





### Condition of formation of lactic acid  in muscles 


- High energy demand




### Reactant in production of lactic acid in muscles 


- Pyruvic acid



### Nature of respiration of skeletal muscles at the condition of high energy deman

Anaerobic

### Products in production of lactic acid in muscles 


-   Lactic acid
-   ATP

### Number of ATP molecules released in the reaction for product of lactic acid in muscles 


2



### Consequence of accumulation of lactic acid in muscles 

Fatigue


### Consequence of muscle fatigue in muscles 



- Decrease force of contraction of muscles






# Outer covering of voluntary muscles 


### Epimysium in voluntary muscles 



- Sheath of connective tissue




### Function of epimysium in voluntary muscles 


- Cover muscle



### Fasicula in voluntary muscles 

- Bundle of muscle fibres



### Composition of fasicula in voluntary muscles


- Myofibrils


### Perimysium in voluntary muscles 


Sheath of connective tissue




### Function of perimysium in voluntary muscles



- Cover fasicula


### Number of muscle fibres present in fasicula 


100 - 1000


### Endomysium in voluntary muscles 


Covering of muscle fibre


### Function of endomysium in voluntary muscle 

Insulate muscle fibre


### Location of sarcolemma in voluntary muscles

Beneath endomysium


### Function of sarcolemma in voluntary muscles 

Line sarcoplasm








# Involuntary muscles 

### Terms for involuntary muscles


- Smooth
- Unstriated
- Unstriped





### Nature of involuntary muscles in terms of control by will 



Uncontrollable



### Involuntary muscles in terms of power of division 


- Can divide 



### Structures influencing activities of involuntary muscles 

- Hormones
- Nervous System



### Nervous system involved in the functioning of involuntary muscles

Autonomic nervous system


## Location of involuntary muscles 


- Wall of hollow organs
- Alimentay canal
- Blood vessels
- Respiratory passage
- Urinary bladder
- Ureter
- Genital tract



 


# Structure of involuntary muscles



### Appromixation of length of involuntary muscles 

Elongated



### Shape of involuntary muscles 

Spindle



### Contents of involuntary muscles 

Myofibrils



### Arrangement of myofibrils in involuntary muscles 

Longitudinal

### Type of cells of involuntary muscles in terms of number of nucleus 

Uninucleated



### Structure absent in involuntary muscle but present in voluntary 


Sarcolemma






### Function of plasma membrane in involuntary muscles 

Cover involuntary muscles



### Cause of name of unstriped muscles in involuntary muscles 


Absence of dark and light bands



### Contents of involuntary muscles 

- Thick filaments
- Thin filaments


### Proteins present at the involuntary muscles



- Actin
- Myosin




### Pattern of arrangement of actin and myosin proteins in irregular muscles 

Irregular


### Cause of name as ustriated muscles of smooth muscles




Irregular pattern of arrangement


### Muscle unit absent in involuntary muscles 


Myofibrils





### Function of gap junctions in involuntary  muscles 

Connect smooth muscles

### Connection of involuntary muscles with the skeleton 

No connection


### Approximation of  time of  contraction of involuntary muscles 

Prolonged

 
### Approximation of rate of time of contraction of involuntary muscles

Slow


### Fatigueness expressed in contraction of involuntary muscles 

Not fatigued



### Cause of muscle contraction in involuntary muscles 

Slide actin and myosin filament

(a sliding filament mechanism) over each other. 


### Source of energy for the contraction of involuntary muscles

ATP



### Calcium binding protein present in involuntary muscles

Calmodulin




### Function of calmodulin in involuntary muscles 



Bind with calcium during contraction



# Cardiac muscles 




### Location of cardiac muscles 

Heart Wall


## Structure of cells of cardiac muscles

### Approximation of length of cells of cardiac muscles

Long

### Shape of cells of cardiac muscles

Cylindrical


### Branching of cells of cardiac muscles 


Branched

### Type of cells of cardiac muscles in terms of quantity of nucleus 

Uninucleated





### Structure of connection of cells of cardiac muscles 

Bridge


### Shape of connection of bridge of cells of cardiac muscles 

Oblique



### Thickness of filaments of cardiac muscles 

- Thick filaments
- Thin filaments



### Type of protein in cardiac muscles 

Troponin

### Covering of cardiac muscles 

Sarcolemma



### Location of intercalated discs in cardiac muscles 

Cardiac muscles fibres



### Function of intercalated discs in cardiac muscles 

- Connect muscle fibres
- Interlock fibres




### Consequence of action of intercalated discs in cardiac muscles 

Strength 



### Location of nucleus at the cells of cardiac muscles 

Centre


### Contents of intercalated discs in cardiac muscles


- Gap junctions
- Desmosomes







### Function of gap junctions in cardiac muscles 

Connect cytoplasm of muscle columns


### Consequence of connection of cytoplasm with adjacent columns in cardiac muscles

Transfer cations 

for the transmission of cations for muscle contractions 

### Event of transmission of cations with adjacent columns in cardiac muscles

Muscle Contraction


### Function of desmosomes in cardiac muscles 

Connect cells


### Function of oblique bridge in cardiac muscles 

Connect muscle fibres




### Nature of cardiac muscles in terms of interaction with nervous system 

Myogenic



### Myogenic

- Contractions generated within the muscles are  not initiated by the nervous sytem


### Shape of cardiac muscles

Cylindrical


### Structure of end of cardiac muscles

Blunt


### Proteins present at cardiac muscles

- Actin
- Myosin

### Type of cardiac muscles in terms of movement by will 



Involuntary




### Nerves supplying the heart 


- Vagus Nerve
- Autonomic nervous system






### Approximation of amount of mitochondria in cardiac muscles 


Abundant

### Tiring of cardiac muscles

Dont get fatigued


















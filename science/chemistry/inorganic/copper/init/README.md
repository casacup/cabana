### Valency of copper

1,2

### Electronic configuration of copper

$$ [Ar]3d^{10}4s^{1} $$ 

### Atomic mass of copper

63.57 amu

### Atomic number of copper

29

### List of  Ores of copper

- Copper pyrite
- Cuprite
- Chalcocite
- Malachite
- Azurite

### Molecular formula of copper pyrite

$$ \ce{ CuFeS2 } $$ 

### Term for copper pyrite 

Chalcopyrite

### Molecular formula of  Cuprite

$$ \ce{ Cu2O } $$ 

### Term for cuprite 

Ruby copper

### Molecular formula of Chalcocite 

$$ \ce{ Cu2S } $$

### Term of chalcocite

Copper glance

### Molecular formula of  Malachite

$$ \ce{ Cu(OH)2CuCO3 } $$ 

### Colour of malachite 

Green



### Molecular formula of  Azurite

$$ \ce{ Cu(OH)2CuCO3 } $$ 

### Colour of Azurite 

Blue










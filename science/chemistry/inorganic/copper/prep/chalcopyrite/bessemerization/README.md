### Material making up the bessemer converter in extraction of copper

Steel

### Compounds present at the internal lining in bessemer converter in extraction of copper


- Lime
- Magnesium oxide



# Activities in bessemerization in extraction of copper

- Mix hot blast of air with sand
- Blow hot blast of air
- Oxidation of sulphides to oxides
- Form slag
- Hinder formation of cuprous oxide
- Reduce copper
- Pour molten copper in sand moulds

### Expression of reaction for reduction of copper in extraction of copper

$$ \ce{ Cu2S + Cu2O -> Cu + SO2 } $$ 

### Reactants in reaction for reduction of copper in extraction of copper


- Cuprous sulphide
- Cuprous oxide


### Products in reaction for reduction of copper in extraction of copper


- Copper
- Sulphur dioxide



### Blister copper in extraction of copper

Copper having rasied surface to escape absorbed sulphur dioxide






### Magnitude of impurities in percentage in blister copper

2 %

# List of methods for purification of blister copper in extraction of copper


- Poling
- Electrolytic refining


## List of activities in poling process in purification of copper in extraction of copper


- Melt blister copper
- Skim floating impurity
- Sprinkle powdered antracite coke
- Stir with pole of green wood


### Consequence of excess of cuprous oxide in extraction of copper

Make copper brittle


### Anode in electrolytic purification in extraction of copper

Crude copper

### Cathode in electrolytic purification in extraction of copper

Pure copper

### List of compounds present at electrolytic solution in extraction of copper


- Copper sulphate
- Sulphuric acid


### Percentage amount of copper sulphate present in electrolytic solution in extraction of copper

15

### Percentage amount of sulphuric acid present in electrolytic solution in extraction of copper

15




### Expression for reaction at anode at electrolytic refining at extraction of copper


$$ \ce{ Cu + SO4-- -> CuSO4 + 2e- } $$ 

### Reactants for reaction at anode at electrolytic refining at extraction of copper


- Copper
- Sulphate ions


### Products at reaction at anode at electrolytic refining at extraction of copper 


- Copper sulphate
- 2 electrons


### Expression for reaction at cathode at electrolytic refining at extraction of copper


$$ \ce{ CuSO4 + 2e- -> Cu + SO4- } $$ 

### Reactants in reaction at cathode at electrolytic refining at extraction of copper


- Copper sulphate
- 2 electrons


### Products in reaction at cathode at electrolytic refining at extraction of copper


- Copper
- Sulphate ions


### Anode mud at extraction of copper

Insoluble sulphate compounds below anode

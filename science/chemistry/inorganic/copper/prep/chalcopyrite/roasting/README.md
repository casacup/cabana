### Activities in roasting in extraction of copper


- Heat concentrated ore in excess air in reverberatory furnace



### List of impurities removed in roasting in extraction of copper


- Moisture
- Volatile impurities
- S, P, As




### Consequence of roasting in extraction of copper


- Removal of impurities
- Conversion of copper pyrite to sulphides
- Partial oxidation of sulphide


### Expression of reaction of removal of sulphide impurities from in roasting in extraction of copper

$$ \ce{ S + O2 -> SO2 } $$ 

### Expression of reaction of removal of phosphorus impurities from in roasting in extraction of copper

$$ \ce{ P4 + O2 -> P2O5 } $$ 

### Expression of reaction of copper pyrite to sulphides in roasting in extraction of copper

$$ \ce{ CuFeS2 + O2 -> Cu2S + FeS + SO2 } $$ 

### Expression of reaction of partial oxidation of copper sulphide in roasting in extraction of copper

$$ \ce{ Cu2S + O2 -> Cu2O + SO2 } $$ 

### Expression of reaction of partial oxidation of iron sulphide in roasting in extraction of coper 

\ce{ FeS + O2 -> FeO + SO2 }

### Reactants in   Expression of reaction of removal of sulphide impurities from in roasting in extraction of copper


- Sulphur 
- Oxygen


### Reactants in  Expression of reaction of removal of phosphorus impurities from in roasting in extraction of copper


- Phosphorus
- Oxygen


### Reactants in  Expression of reaction of copper pyrite to sulphides in roasting in extraction of copper


- Copper pyrite
- Oxygen


### Reactants in  Expression of reaction of partial oxidation of copper sulphide in roasting in extraction of copper


- Cuprous sulphide
- Oxygen


### Reactants in  Expression of reaction of partial oxidation of iron sulphide in roasting in extraction of coper 


- Ferrous sulphide
- Oxygen


### Products in Expression of reaction of removal of sulphide impurities from in roasting in extraction of copper


- Sulphur dioxide


### Products in Expression of reaction of removal of phosphorus impurities from in roasting in extraction of copper


- Phosphorus pentoxide


### Products in Expression of reaction of copper pyrite to sulphides in roasting in extraction of copper


- Cuprous sulphide
- Ferrous sulphide
- Sulphur dioxide


### Products in Expression of reaction of partial oxidation of copper sulphide in roasting in extraction of copper


- Cuprous oxide
- Sulphur dioxide


### Products in Expression of reaction of partial oxidation of iron sulphide in roasting in extraction of coper 


- Ferrous oxide
- Sulphur dioxide



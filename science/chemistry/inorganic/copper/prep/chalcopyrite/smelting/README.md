# Activities in smelting in extraction of copper


- Mix roasted ore with powdered coke and sand
- Heat strongly in jacketted blast furnace
- Oxidize cuprous and ferrous sulphides
- Form ferrous silicate slag
- Hinder copper oxidation by iron sulphide

### Structure of arrangement at the top of blast furnace in extraction of copper



- Charging arrangement
- Outlet of waste gases



### Structure of arrangement at the bottom of blast furnace in extraction of copper 


- Outlet of molten slag
- Outlet of matte


### Function of tuyeres in blast furnace in extraction of copper


- Provide hot blast of air



### Copper matte in extraction of copper

Copper sulphide containing trace amount of iron sulphide

### Expression for reaction of formation of slag of ferrous silicate in extraction of copper

$$ \ce{ FeO + SiO2 -> FeSiO3 } $$ 

### Reactants for expression of reaction of formation of slag of ferrous silicate in extraction of copper


- Ferrous oxide
- Silicate


### Expression for reaction of hindrance of ferrous sulphide in oxidation of copper in extraction of copper


$$ \ce{ Cu2O + FeS -> Cu2S + FeO } $$ 


### Reactants in expression for reaction of hindrance of ferrous sulphide in oxidation of copper in extraction of copper


- Cuprous oxide
- Ferrous sulphide


### Products in expression for reaction of hindrance of ferrous sulphide in oxidation of copper in extraction of copper 


- Cuprous sulphide
- Ferrous oxide



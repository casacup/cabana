### Expression of  Reaction of copper with moist air

$$ \ce{ Cu + H2O + O2 + CO2 -> Cu(OH)2CuCO3 } $$ 

### Reactants in reaction of copper with moist air


- Copper
- Moisture
- Oxygen
- Carbondioxide


### Products in reaction of copper with with moist air


- Malachite

### List of conditions of reaction of copper with direct oxygen


- Below 1100 Celsius
- Above 1100 Celsius



### Expression of reaction of copper with direct oxygen below 1100 C

$$ \ce{ Cu + O2 ->[below 1100] Cu2O } $$ 

### Expression of reaction of copper with direct oxygen above 1100 C

$$ \ce{ Cu + O2 ->[above 1100] Cu) } $$ 

### Expression of reaction of copper with water

$$ \ce{ Cu + H2O ->[High temperature] CuO + H2 ) } $$ 

### Expression of reaction of copper with dil Sulphuric acid

$$ \ce{ Cu + dil. H2SO4 ->[Oxygen] CuSO4 + SO2 + H2O } $$ 

### Expression of reaction of copper with conc Sulphuric acid

$$ \ce{ Cu + conc H2SO4 -> CuSO4 + SO2 + H2O } $$

### Expression of reaction of copper with dil Hydrochloric acid

$$ \ce{Cu + dil HCl ->[Oxygen] CuCl2 + H2O  } $$


### Expression of reaction of copper with hot conc Nitric acid

$$ \ce{ Cu + hot conc HNO3 -> Cu(NO3)2 + N2 + H2O } $$ 

### Expression of reaction of copper with  conc Nitric acid

$$ \ce{ Cu +  conc HNO3 -> Cu(NO3)2 + NO2 + H2O } $$ 

### Expression of reaction of copper with Moderate Nitric acid

$$ \ce{ Cu + moderate HNO3 -> Cu(NO3)2 + NO + H2O } $$ 

### Expression of reaction of copper with dil Nitric acid

$$ \ce{ Cu + dil HNO3 -> Cu(NO3)2 + N2O + H2O } $$ 

### Expression of reaction of copper with Halogen

$$ \ce{ Cu + X2 -> CuX2 } $$ 








### Products in reaction of copper with direct oxygen above 1100 Celsius


- Cuprous oxide


### Colour of cuprous oxide

Red








### Products in reaction of copper with direct oxygen above 1100 Celsius


- Cupric oxide


### Colour of cupric oxide

Black

### Reactants in reaction of copper with  water


- Copper
- Water


### Products in reaction of copper with water


- Cupric oxide
- Hydrogen


### Condition of reaction in copper with  water

High Temperature





### Products in reaction of copper with dil sulphuric acid


- Copper sulphate
- Sulphur dioxide
- Water


### Condition of reaction in copper with  dil sulphuric acid

Presence of Oxygen




### Products in reaction of copper with  conc sulphuric acid


- Copper sulphate
- Sulphur dioxide
- Water




### Products in reaction of copper with  dil hydrochloric acid


- Cupric chloride
- Water


### Condition of reaction in copper with  dil hydrochloric acid

Presence of air

### Products in reaction of copper with hot conc nitric acid


- Cupric nitrate
- Nitrogen
- Water





### Products in reaction of copper with conc nitric acid



- Cupric nitrate
- Nitrogen dioxide
- Water



### Products in reaction of copper with moderate nitric acid


- Cupric nitrate
- Nitrogen oxide
- Water

### Products in reaction of copper with  dil nitric acid


- Cupric nitrate
- Dinitrogen oxide
- Water


### Products in reaction of copper with halogen


- Cupric halide



### Condition of reactants in  reaction in copper with displacement reaction 

Salts of less reactive metals



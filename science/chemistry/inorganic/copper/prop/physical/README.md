### Colour of copper

Characteristic red

### Boiling point of copper

2560 Celsius

### Melting point of copper

1080 Celsius

### Density of copper 

8.92 gm/cc

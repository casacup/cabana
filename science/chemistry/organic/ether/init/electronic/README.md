### Type of hybridization shown by carbon atom in ether

sp3

### List of activities at formation of bonding in ether

- Form 2 sigma bond with carbon by oxygen

### Number of sigma bonds formed by carbon with oxygen in ether

2



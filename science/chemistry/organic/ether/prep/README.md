### List of methods of preparation of ether

- Alcohols
- Halo alkanes



### List of reagents for preparation of ether by dehydration  of alcohol

- Conc Sulphuric acid
- Alumina


### List of type of ether prepared by dehydration of alcohol

Symmetrical ether

### Expression for reaction of preparation of ether by dehydration of alcohol in sulphuric acid

$$ \ce{ R-OH + OH-R ->[conc H2SO4][413 K] R-O-R + H2O } $$ 

### Reactants for reaction of preparation of ether by dehydration of alcohol in sulphuric acid

- 2 Alcohols

### Products for reaction of preparation  of ether by dehydration of alcohol in sulphuric acid

- Ether 
- Water

### Temperature needed for reaction of preparation of ether by dehydration of alcohol in sulphuric acid

413 K

### Molecular formula of alumina

$$ \ce{ Al2O3 } $$ 

### Expression for reaction of preparation of ether by dehydration of alcohol in alumina

$$ \ce{ R-OH + OH-R ->[Al2O3][ 525 K] R-O-R + H2O } $$ 

### Reactants for reaction of preparation of ether by dehydration of alcohol in alumina

- 2 Alcohols

### Products for reaction of preparation  of ether by dehydration of alcohol in alumina

- Ether
- Water

### Temperature needed for reaction of preparation of ether by dehydration of alcohol in alumina

525 K


### List of reactions for preparation of ether from halo alkanes

- Williamson's synthesis
- Haloalkane with dry silver oxide


### Expression for reaction of preparation of ether from halo alkane by williamson's synthesis

$$ \ce{ RONa+ -> XR^{|} -> R-O-R^{|} + NaX } $$ 

### Reactants for reaction of preparation of ether from halo alkanes by williamson's synthesis

- Charge separated sodium alkoxide / Potassium alkoxide
- Halo alkane

### Products for reaction of preparation of ether from halo alkanes by williamson's synthesis

- Ether
- Sodium Halide / Potassium halide

### Condition for reaction of preparation of ether form halo alkanes by williamson's synthesis

Heat

### Product at reaction of formation of ether from halo alkane by williamson synthesis on using tertiary alkyl halide


Alkene


### Reaction mechanism of williamson's synthesis

- Nucleophilic substitution of halide ion by alkoxide ion



### List of types of ether prepared by williamson's synthesis

- Unsymmetrical ether
- Symmetrical ether


### List of types of ether prepared by heating halo alkanes with dry silver oxide

- Symmetrical ether
- Unsymmetrical ether


### Expression for reaction of preparation of ether by dry silver oxide

$$ \ce{  R-X + R^{|}X + Ag2O - R-O-R^{|} + 2AgX } $$ 

### Reactants for reaction of preparation of ether by dry silver oxide

- Silver oxide
- Halo alkane
- Halo alkane

### Condition for reaction of preparation of ether by dry silver oxide 

Heat

### Products for reaction of preparation of ether by dry silver oxide

- Ether
- Silver chloride

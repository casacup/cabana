### List of reactions of ether involving alkyl and aryl groups

- Halogenation



### List of conditions for reaction of halogenation of diethyl ether

- Light
- Darkness

### Expression for reaction of halogenation of diethyl ether  in darkness

$$ \ce{ CH3CH2-O-CH3CH2CH3 ->[Cl2][Dark] CH3-CHCl-O-CH2-CH3 ->[Cl2][Dark] CH3-CHCl-O-CHCl-CH3 } $$ 


### Carbon at which substitution occurs in halogenation of diethyl ether in darkness

Alpha carbon






### Products in reaction of halogenation of diethyl ether in darkness

1,1 dichloro diethyl ether


### Expression for reaction of halogenation of diethyl ether in presence of light

$$ C2H5-O-C2H5 ->[Cl2][Light] C2Cl5-O-C2Cl5 $$ 


### Products in reaction of halogenation of diethyl ether in presence of light

Per chloro diethyl ether


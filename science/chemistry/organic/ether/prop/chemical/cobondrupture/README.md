### List of reagents on cleavage of carbon oxygen bond in ether

- Water
- Halogen acids
- Sulphuric acids
- Phosphorus pentachloride


### Expression for reaction of cleavage of carbon oxygen bond in ether by water

$$ \ce{ R-O-R^{|} } + HO-H -> ROH + R^{|}OH $$ 


### Reactants for reaction of cleavage of carbon oxygen bond in ether by water

- Ether
- Water

### Products for reaction of cleavage of carbon oxygen bond in ether by water

- Alkyl alcohol
- Alkyl 2 alcohol

### Condition for reaction of cleavage of carbon oxygen bond in ether by water 

Boiling water


### List of conditions for reaction of ethers with halo acids

- Cold
- Higher temperature / Excess acid


### List of reactivity of order of halo acids in reaction of ether

$$ HI > HBr > HCl $$ 

### Expression for reaction of cleavage of carbon oxygen bond in ether by halo acid in cold 


$$ \ce{ R-O-R + HX -> ROH + RX } $$ 

### Reactants for reaction of cleavage of carbon oxygen bond in ether by  in cold

- Ether
- Halo acid

### Products for reaction of cleavage of carbon oxygen bond in ether by haloacid in cold

- Alcohol
- Haloalkane


### Size of alkyl group taken by haloalkane on unsymmetrical ether of primary or secondary alkyl groups in cleavage of carbon oxygen bond in ether by haloacid in cold

- Smaller

Haloalkane formed is from the small sized alkyl group


### Branching of alkyl group taken by haloalkane on tertiary alkyl group groups in cleavage of carbon oxygen bond in ether by haloacid in cold

- Tertiary halide

Haloalkane formed is tertiary halide


### Expression for reaction of cleavage of carbon oxygen bond in ether by halo acid in high temperature


$$ \ce{ R-O-R + HX -> RX + H2O } $$ 

### Reactants for reaction of cleavage of carbon oxygen bond in ether by halo acid in high temperature

- Ether
- Excess Halo acid

### Products for reaction of cleavage of carbon oxygen bond in ether by halo acid in high temperature

- Haloalkane
- Water


### List of steps involved at cleavage of carbon oxygen bond in ether by halo acid

- Protonation of ether by hydrogen from acid
- Nucleophilic attack by halide ion

### Type of reaction mechanism exhibited by cleavage of carbon oxygen bond in ether by halo acid on primary or secondary alkyl groups 

SN2

### Type of reaction mechanism exhibited by cleavage of carbon oxygen bond in ether by halo acid on tertiary alkyl groups

SN1


### Effect of cold conc sulphuric acid on carbon oxygen bond of ether

Null


### Expression for reaction of cleavage of carbon oxygen bond in ether by sulphuric acid 

$$ \ce{ R-O-R + H2SO4 ->[\triangle] ROH + RSO4 } $$ 

### Reactants for reaction of cleavage of carbon oxygen bond in ether by sulphuric acid

- Ether
- Conc Sylphuric acid


### Products for reaction of cleavage of carbon oxygen bond in ether by sulphuric acid

- Alkanol
- Alkyl hydrogen sulphate


### Condition for reaction of cleavage of carbon oxygen bond in ether by sulphuric acid

Heat

### Expression for reaction of cleavage of carbon oxygen bond in ether by phosphorus penta chloride

$$ \ce{ R-O-R + PCl5 ->[\triangle] RCl + POCl3 } $$ 


### Reactants for reaction of cleavage of carbon oxygen bond in ether by  phosphorus penta chloride

- Ether
- Phosphorus penta chloride

### Products for reaction of cleavage of carbon oxygen bond in ether by phosphorus penta chloride

- Alkyl Halide
- Phosphoryl chloride


### Molecular formula of phosphoryl chloride

$$ \ce{ POCl3 } $$ 

### Condition for reaction of cleavage of carbon oxygen bond in ether by phosphorus penta chloride

Heat



### List of reactions due to ethereal oxygen

- Peroxide formation
- Oxonium salts
- Addition compounds


### Cause of reaction of ethereal oxygen despite it's inertness

Lone pairs of electrons


### Expression for reaction of peroxide formation from ether

$$ \ce{ C2H5-O-C2H5 + O2 ->[Light][Air] \chemfig{  C_{2}H_{5}-O(<:>[2]O)-C_{2}H_{5} }  } $$ 


### Reactants at reaction of peroxide formation form ether

- Ethoxyethane
- Oxygen

### Products at reaction of peroxide formation of ether

Dative bond from oxygen to oxygen. Peroxide


### Catalysts at reaction of peroxide formation from ether

- Light



### List of conditions for reaction of formation of oxonium salts from ether

- Conc. Hydrochloric acid
- Conc. Sulphuric acid

### Expression for reaction of formation of  oxonium salts form ether in hydrochloric acid

$$ \ce{ C2H5-O-C2H5 ->[Conc HCl][Cold] \chemfig{  C_{2}H_{5}-O(-[2]H)-C_{2}H_{5} }Cl-  } $$ 


### Reactants for reaction of formation of oxonium salts from ether in hydrochloric acid 

Ethoxy ethane

### Products for reaction of formation of oxonium salts from ether in hydrochloric acid

- Hydrogen bonded with ethereal oxygen
- Cl ion bonded with ether with charge separation

Diethyl oxonium chloride

### Condition for reaction of formation of oxonium salts from ether in hydrochloric acid

Cold


### Expression for reaction of formation of oxonium salts from ether in sulphuric acid

$$ \ce{ \chemfig{C2H5-O-C2H5} ->[Conc HCl][Cold] \chemfig{C_{2}H_{5}-O(-[2]H)-C_{2}H_{5} }HSO4-}} $$ 

### Reactants for reaction of formation of oxonium salts from ether in sulphuric acid


Ethoxy ethane

### Products for reaction of formation of oxonium salts from ether in sulphuric acid

- Hydrogen bonded with ethereal oxygen
- HSO4 ion bonded with ether with charge separation

Dethyloxonium hydrogen sulphate


### Condition for reaction of formation of oxonium salts from ether in sulphuric acid


Cold


### Bassicity strength of ether

Weak 

### Products of oxonium salts on dilution with water

- Ether
- Acid

### Cause of reversible dissassociation of oxonium salts on dilution 


Water stronger lewis base


### List of reagents giving addition compounds with ether

- Boron Triflouride
- Grignard reagent


### Expression for reaction of formation of addition compounds with ether in boron triflouride
$$ \ce{ \chemfig{  C_{2}H_{5}-O(-[2]C_{2}H_{5}) } + BF3 -> \chemfig{  C_{2}H_{5}-O(-[2]C_{2}H_{5}):>BF_{3} }  } $$ 


### Reactants for reaction of formation of addiion compound with ether in boron triflouride

- Ethoxy ethane
- Boron triflouride


### Products for reaction of formation of addition compound with ether in boron triflouride

- Boron triflouride etherate


Boron triflouride bonded at ethereal oxygen with charge separation

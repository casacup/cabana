### List of type of materials that do not affect ether in terms of reactivity

- Alkalies
- Alkali metals
- Dilute acids


### List of types of chemical reactions of ether on the basis of reaction by it's components

- Reaction due to alkyl or aryl group
- Reaction due to ethereal oxygen
- Reaction due to rupture of Carbon and oxygen bond

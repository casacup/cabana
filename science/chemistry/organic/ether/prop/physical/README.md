### List of compounds of ether present as gases

- Methoxymethane
- Methoxyethane


### Smell of ethers

Pleasant

### Physical state exhibited by most members of ether

- Volatile liquids


### Approximation of density of ether compared to water

Lighter



### Bond angle between carbon oxygen and carbon in ether


110 degree

### Type of hybridization occurred at ether in oxygen atom

sp3

### Cause of dipole moment at ether despite the symmetricity 

C-O bond is polar


### Dipole moment of methoxy methane

1.3 D


### Dipole moment of ethoxyethane

1.18 D


### Cause of absence of hydrogen bonding at ether

- No hydrogen bonded directly to alcohol




### Approximation of boiling point of ether compared to alcohol

Lower than alcohol



### Cause of low boiling point of ether compared to alcohol

Absence of hydrogen bond



### Number of carbon atoms till which ether is soluble in water comparable with alcohol

- 3

### Cause of solubility of ether in water

Hydrogen bond

### Change in solubility of ether in water on increasing the size of alkyl group

Decreases



### List of uses of ether

- Industrial solvent
- Anaesthetic
- Reaction medium
- Refrigerants


### List of ethers used as industrial solvent

- Diethyl ether
- Tetra hydro furane
- Methyl tert butyl ether
- 1,2 Di methoxy methane
- 1,4 Di oxane

### Full form of THF in organic chemistry

Tetra hydro furane


### Full form of DME in organic chemistry

1,2 Di methoxy ethane


###  Full form of MTBE in organic chemistry

Methyl tert butyl Ether



### Uses of ethoxyethane in medicine

Muscle relaxant


### Use of phenyl ether in organic chemistry

Heat transfer medium



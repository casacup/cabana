### Expression of relation of capacitance with voltage and charge


$$ C = \frac{q}{v_{c}} $$ 

### Derivation for expression of voltage in capacitor in ac

\begin{align*}
i = \frac{dq}{dt} = I \cos( \omega t ) \\ 
\text{Integrating ,} \\ 
\int dq = \int I \cos ( \omega t ) dt \\
q = \frac{I}{\omega} \sin{\omega t} \\ 
v_{c} = \frac{q}{C} \\
v_{c} = \frac{I}{\omega C} \sin{\omega t}
\end{align*}


### Expression of voltage in capacitance in terms of sine in  ac

$$ v_{C} = \frac{I}{\omega C} \sin{\omega t} $$ 

### Expression of voltage in capacitance in terms of cosine in ac

$$ v_{C} = \frac{I}{\omega C} \cos{ \omega t - 90^{\circ} }$$ 

### Phase angle of voltage in capacitance in ac

- 90 

### Expression of capacitive reactance in ac

$$ X_{C} = \frac{1}{\omega C} $$ 

### SI unit of capacitive reactance

Ohm

### Graph of voltage and current versus time in capacitor in ac


[Illustration Missing]

Crests and trough of current and voltage are at different positions from the x axis


### Phasor diagram of voltage and current in capacitor in ac

[Illustration Missing]

Current is at positive while voltage at negative



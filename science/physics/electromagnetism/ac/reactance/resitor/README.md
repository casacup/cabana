### Expression for voltage amplitude in resistor in ac

$$ V_{R} = IR $$ 

### Derivation of instantaneous voltage in resistor in ac

\begin{align*}
i = I \cos(\omega t) \\
v_{R} = iR \\
v_{R} = I \cos(\omega t) R \\
v_{R} = V_{R} \cos(\omega t)
\end{align*}


### Expression of instantaneous voltage in resistor in ac

$$ v_{R} = v_{R} \cos \omega t $$ 

### Relation of voltage and current in resistor in ac

Voltage and current are in phase

### Graphical representation of voltage and current in resistor in ac

[Illustration Missing]

Same position of crest and trough in x axis of voltage and current 

### Phasor diagram of voltage and current in resistor in ac

[Illustration Missing]

Voltage and current overlap




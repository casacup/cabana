### Root mean square value in ac

Square root of average of square of instantenous current

### Derivation of root mean square value in ac

\begin{align*}
i = I \cos(\omega t) \\
i^{2} = I^2 \cos^{2}(\omega t) \\ 
i^{2} = I^{2}\frac{1}{2}(1 + \cos 2 \omega t) \\
i^{2} = \frac{1}{2}I^{2} + \frac{1}{2}I^{2}\cos 2 \omega t \\
I_{rms} = \frac{I}{\sqrt{2}} 
\end{align*}

### Expression for root mean square value in ac

$$ I_{rms} = \frac{I} {\sqrt{2}} $$ 

### Expression for root mean square value in ac in terms of magnitude

$$ I_{rms} = 1.41 \times I  $$ 


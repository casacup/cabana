Condition for coloumb’s law
===========================

-   The **coloumb’s** law is valid under following **considerations** :

    -   The **charges** are *stationary* .

    -   The **charges** in consideration are point charges.

        -   The **radius** is negligible compared to the distance of
            *separation*

Structures satisfying the condition of applicability of coloumb’s law
---------------------------------------------------------------------

-   **Coloumb’s law** is *satisfied* by conducting **spheres.**

    -   **Charge** is distributed at the conducting sphere.

    -   This **charge** appears to originate from the *center* of the
        **spheres.**

    -   The **charge** at the center behaves as a **point** charge.

Couloumb’s law
==============

-   The expression for **coloumb’s** law is given by:
    $$F = k \frac{q_1 q_2}{r^2}$$

    -   In $CGS$ system:

        -   $k$ = $1$

    -   In $MKS$ system:

        -   $k$ = $9 \times 10^{9}$

Value of $k$ 
------------

-   The term $k$ is expressed as: $$k = \frac{1}{4\pi \epsilon_{0}}$$

-   $\epsilon_{0}$ = $8.85 \times 10^{-12} C^2 N^{-1} m^{-2}$

-   The term $\epsilon_{0}$ is the **permitivitty** of **free space** .

-   The **electrostatic force** in *air* and *vacuum* are equal.

-   **Air** is not considered as a medium.

-   **Electrostatic force** at medium of $\epsilon$ is given by:
    $$F_{medium} = \frac{1}{4\pi \epsilon} \frac{q_1 q_2}{r^{2}}$$

Relative Permitivitty
=====================

-   **Relative Permitivitty** expresses the *number* of times the
    **permitivitty** of a *medium* is greater than that of **air.**
    $$\epsilon_{r} = \frac{\epsilon}{\epsilon_{0}}$$

-   **Relative Permitivitty** is also termed as **dielectric constant**
    .

$$\begin{aligned}
    F_{medium} &= \frac{q_1 q_2}{4 \pi \epsilon r^2} \\
    F_{medium} &= \frac{q_1 q_2}{4 \pi \epsilon_{r} \times \epsilon_{0} r^2} 
  \end{aligned}$$

-   The force in a medium of **relative** permitivitty $\epsilon_{r}$ is
    given by: $$F_{medium} = \frac{F_{air}}{\epsilon_{r}}$$

Analysis of relative permitivitty
---------------------------------

-   The value of **relative permitivitty** only lies in between :
    $$1 < \epsilon_{r} \leq \infty$$

-   The value of **relative permitivitty** of metals is **infinity** .

Physical Meaning of Permitivitty
--------------------------------

-   **Current** is opposed by a conductor **maintained** at a
    **constant** *p.d.* .

-   This **opposition** is called **resistance** .

-   **Electric Force** set up by an **electric field** is opposed by
    **medium** .

-   The **greater** the permittivity the **less** *lines of electric*
    force can pass through the **medium** .

Electrostatic Shielding
-----------------------

-   **Metals** having **infinite** relative permittivity completely
    block **electrostatic** force.

-   This **effect** is called **electrostatic shielding.**

Cases of sphere.
----------------

-   If **two** spheres of same charges are *considered* :
    $$F < k \frac{q_1 q_2}{r^2}$$

-   If **two** spheres of opposite charges are *considered* :
    $$F > k \frac{q_1 q_2}{r^2}$$

-   These cases **charges** are not present exactly at the **center.**

-   Change in **magnitude** of **radius** is seen.

Partial Medium
--------------

-   The **electrostatic** *force* experienced at a *partial medium* of
    *thickness* $t$ is:
    $$F_{p.medium} = \frac{q_1 q_2}{4 \pi \epsilon_{0} [(r-t) + \sqrt{\epsilon_r} \times t]^{2}}$$





## Equilibrium

A body is said to be in equilibrium  if net force is 

- $$ 0 $$

## Types of equilibrium on the basis of motion

The types of equilibrium on the basis of motion are


- Translational equilibrium
- Rotational equilibrium


##  Condition for Translation  equilibrium

A body is said to be in translation equilibrium if net force in the body is zero.


## Condition for Rotational equilibrium

A body is said to be in rotational equilibrium if net torque in the body is zero.

## Types of equilibrium on the basis of number of particles

The types of equilibrium on the basis of number of particles are


- Particle equilibrium
- System equilibrium



### Condition for particle equilibrium

A particle is said to be in particle equilibrium if net force in a particle is 


- $$ 0 $$

### Condition for system equilibrium

A system is said to be in equilibrium if net force in every particle of the system is 
 
- $$ 0 $$


# Rules for equilibrium

## Location of equilibrium point in like charges

The location of equilibrium  point in like charges is 


- Between the charges



## Location of equilibrium point in unlike charges


The location of equilibrium point in unlike charges is

- Outside the charges


## Location of equilibrium point based on the magnitude

The location of equilibrium point lies 

- Near the charge having smaller magnitude



# Special cases for equilibrium of charges

## Same magnitude of charge

### Like charges

The equilibrium point of like charges of same magnitude is located at


- the mid point


### Unlike charges

The equilibrium point of unlike charges of same magnitude 


- does not exist


# Problem Solving on two charges

## Location of equilibrium point

- Equilibrium point is such a point where an external test charge cannot feel any force.


### Like charges

- A external test charge is taken as
- $$ q $$

- The charge of small magnitude is taken as
- $$ q_{small} $$ 
- The distance of the test charge from the charge of small magnitude  is taken as 
- $$ x $$ 



- The charge of greater magnitude is taken as
- $$ q_{big} $$ 

- The distance of test charge from the charge of greater magnitude is taken as 
- $$ r - x $$ 

### Derivation for magnitude of charges in equilibrium condition

\begin{align*} 
F_{\text{$q_{big}$ on $q$ }} = F_{\text{$q_{small}$ on \( q \)  }} \\
k \times q \times \frac{q_{small}}{(x)^{2}} = k \times q \times \frac{q_{big}}{(r-x)^{2}} \\
(\frac{r -x }{x})^{2} = \frac{q_{big}}{q_{small}} \\
\frac{r}{x} = \sqrt{\frac{q_{big}}{q_{small}}} + 1
\end{align*}

### Expression for magnitude of bigger charge in equilibrium condition

$$ q_{big} = q_{small}(\frac{r}{x} - 1)^{2} $$

### Expression for magnitude of smaller charge in equilibrium condition

$$ q_{small} = q_{big} \times \frac{1}{(\frac{r}{x} -1)^{2} }$$

### Expression for location of equilibrium point from smaller charge

$$ x = \frac{r}{\sqrt{\frac{q_{big}}{q_{small}}} + 1} $$

### Expression for distance of separation in equilibrium condition

$$ r = x \times (\sqrt{\frac{q_{big}}{q_{small}}} + 1 ) $$


### Unlike charges

- A external test charge is taken as
- $$ q $$

- The charge of small magnitude is taken as
- $$ q_{small} $$ 
- The distance of the test charge from the charge of small magnitude  is taken as 
- $$ x $$ 



- The charge of greater magnitude is taken as
- $$ q_{big} $$ 

- The distance of test charge from the charge of greater magnitude is taken as 
- $$ r + x $$ 

### Derivation for magnitude of charges in equilibrium condition

\begin{align*}
F_{\text{$q_{big}$ on $q$ }} = F_{\text{$q_{small}$ on \( q \)  }} \\
k \times q \times \frac{q_{small}}{(x)^{2}} = k \times q \times \frac{q_{big}}{(r+x)^{2}} \\
(\frac{r + x }{x})^{2} = \frac{q_{big}}{q_{small}} \\
\frac{r}{x} = \sqrt{\frac{q_{big}}{q_{small}}} - 1
\end{align*}

### Expression for magnitude of bigger charge in equilibrium condition

$$ q_{big} = q_{small}(\frac{r}{x} + 1)^{2} $$

### Expression for magnitude of smaller charge in equilibrium condition

$$ q_{small} = q_{big}\frac{1}{(\frac{r}{x} + 1)^{2} }$$

### Expression for location of equilibrium point from smaller charge

$$ x = \frac{r}{\sqrt{\frac{q_{big}}{q_{small}}} - 1} $$

### Expression for distance of separation in equilibrium condition

$$ r = x \times (\sqrt{\frac{q_{big}}{q_{small}}} - 1 ) $$





# System equilibrium

### Special charge to maintain system equilibrium

- A charge \( q \) is considered to maintain the system at equilibrium.
- The equilibrium point is calculated as \( x \) .

### Derivation for magnitude of charge at system equilibrium

- The sum of forces in system equilibrium amounts to zero.
- The magnitude of charge is such that the force with the smaller charge and special charge is equal with the force between smaller charge and greater charge.
- The direction of the forces are opposite.
- The forces cancel each other.

\begin{align*}
F_{\text{ \( q_{small} \) on \( q \)  }} = - F_{\text{ \( q_{big} \) on \( q_{small} \)  }} \\
k \times q_{small} \times \frac{q}{x^{2}} = - k \times \frac{q_{big} q_{small}}{r^{2}} 
\end{align*}


### Expression for magnitude of special charge in equilibrium condition

$$ q = -q_{big} \times (\frac{x}{r})^{2} $$




# Electric field intensity

Electric field intensity at a point is defined as force experienced by unit positive test charge placed at that point.

## Expression for electric field intensity

The expression for electric field intensity is given by

$$ E = \frac{F}{q} $$

### Derivation for dimension of electric field intensity

\begin{align*}
[E] = \frac{[F]}{[q]} \\
[E] = \frac{[MLT^{-2}]}{[AT]} 
\end{align*}

### Expression for dimension of electric field intensity

The dimension of electric field intensity is

$$ [E] = [MLA^{-1}T^{-3}] $$ 

### Derivation for the expression of electric field intensity

\begin{align*}
E = \frac{F}{q} \\
E = \frac{kQq}{r^{2}} \times \frac{1}{q}
\end{align*}
 

### Expression for electric field intensity in terms of radius and charge

$$ E = k \frac{Q}{r^{2}} $$ 


## Direction of electric field intensity

### Positive charges
The direction of electric field intensity of positive charge is 


- Away from the charge
- $$ \leftarrow q^{+} \rightarrow $$ 



### Negative charges

The direction of electric field intensity of negative charge is


- Towards the charge 
- $$ \rightarrow q^{-} \leftarrow $$ 


# Vector form of electric field intensity

The sign of charge is written in vector form of electric field intensity.

### Expression of vector form of electric field  intensity on positive charges

$$ E = \frac{kQ}{r^{2}}\hat{r} $$

### Direction of unit vector of radius

- The unit vector of radius (\( \hat{r}\) ) is directed towards the observable point.
- The observable point is the point at which the electric field intensity is to be determined.


### Expression of vector form of electric field intensity on negative charges

$$ E = - (\frac{kQ}{r^{2}})\hat{r} $$






Background
==========

-   **Solenoid** is a **coiled** conductor.

-   The **magnetic field** of **solenoid** is as that of **bar magnet**
    .

Magnetic field at axis
======================

-   The **magnetic field** is to be calculated at $P$ .

-   A small length segment $dl$ is taken.

-   Number of turns in $dl$ = $n$

$$\begin{aligned}
    dB &= \frac{\mu_{0}NIa \sin \theta}{2r^{2}} \\ 
    N &= ndl   \\    
    dB &= \frac{\mu_{0}nIa dl \sin\theta}{2r^{2}} \\ 
    BC &= dl \sin \theta  \\ 
    BC &= r d \theta \\ 
    dl \sin \theta &= rd \theta \\ 
    dB &= \frac{\mu_{0}nIa r d \theta}{2r^{2}} \\ 
    dB &= \frac{\mu_{0}nIa d \theta}{2r} \\ 
    \frac{a}{r} &= \sin \theta \\ 
    dB &= \frac{\mu_{0}nI \sin \theta d \theta}{2}  \\ 
    B &= \int dB  \\ 
    B &= \int_{\alpha_1}^{\alpha_2} \frac{\mu_{0}nI \sin \theta  d\theta}{2r} \\ 
    B &= \frac{\mu_{0}nI}{2} \int_{\alpha_1}^{\alpha_2} \sin \theta d \theta \\ 
    B &= \frac{\mu_{0}nI}{2}[\cos \alpha_1 - \cos \alpha 2] 
  \end{aligned}$$

-   The general **expression** for magnetic field at the axis of a
    **current** carrying **solenoid** is:
    $$B = \frac{\mu_{0}nI}{2}[\cos \alpha_1 - \cos \alpha_2]$$



Magnetic Field for long solenoid
--------------------------------

-   $\alpha_1$ = $0$

-   $\alpha_2$ = $\pi$

<!-- -->

-   The **expression** for **magnetic field** at axis for **long
    solenoid** is given by: $$B = \mu_{0}nI$$
$$\begin{aligned}
      dB &= \frac{u_{0}Idl \sin \theta}{4 \pi r^2} \\
      dl \sin \theta &= AN \\
      d \phi &= \frac{AN}{r} \\
      AN &= d \phi r \\
      dB &= \frac{u_{0}I r d\phi }{4 \pi r^2} \\
      \cos \phi &= \frac{a}{r} \\
      r &= \frac{a}{\cos \phi} \\
      dB &= \frac{u_{0}I \cos \phi d \phi }{4 \pi a} \\
      B &= \int_{\phi_1}^{-\phi_2} dB \\
      B &= \frac{u_{0}I}{4 \pi a} \int_{\phi_1}^{-\phi_2} \cos \phi d \phi \\
      B &= \frac{u_{0}I}{4 \pi a}[\sin{\phi_1} + \sin{\phi_2}] 
    \end{aligned}$$

-   The general expression for **magnetic field** due to a **long
    straight** conductor is given by:
    $$B = \frac{u_{0}I}{4 \pi a}[ \sin{\phi_1} + \sin{\phi_2} ]$$

Infinite long conductor
=======================

$$\begin{aligned}
     \phi_1 = \phi_2 &= \frac{\pi}{2} \\
     B = \frac{u_{0}I}{4 \pi a} \times 2  
   \end{aligned}$$

-   The expression for **magnetic field** due to **infinitely** long
    magnetic field is given by: $$B = \frac{u_{0}I}{2 \pi a }$$

# Application of Ampere's law

# Solenoid

![Structure of a solenoid](https://t3.ftcdn.net/jpg/04/33/05/92/360_F_433059226_dMZPK2szWTXE88kvcSCOXvJsdHeTDlRg.jpg)


## Application of ampere's law in solenoid.

- Ampere's law is valid only under closed path.
- A closed path is formed in a solenoid in the form of a rectangle.


## Derivation for the expression of magnetic field at the axis of solenoid from ampere's law

![Magnetic field of closed loop in a solenoid](https://images.topperlearning.com/topper/tinymce/imagemanager/files/85081471a753ffddaab3c809a2b8d7d75c71019eea6626.16017378sole.PNG)

- The solenoid has turns.
- The turns of solenoid are ring shaped.


### Line integral in amperes law

\begin{align*}
\oint \vec{B}. \vec{dl} = \oint_{a}^{b} \vec{B}.\vec{dl} + \oint_{b}^{c} \vec{B}.\vec{dl} + \oint_{c}^{d} \vec{B}.\vec{dl} + \oint_{d}^{a} \vec{B}.\vec{dl} \\
\end{align*}

### Magnitude of angle of parallel sides with the axis of magnetic field

The angle between side of rectangle parallel to the axis of magnetic field  with the magnetic field is

- $$ 0 $$ 

### Evaluation of expression for side parallel \( ab \)  to the magnetic field

\begin{align*}
\oint_{a}^{b} Bdl \cos \theta \\
= \oint_{a}^{b} Bdl \cos (0) \\
= \oint_{a}^{b} Bdl \\
= B \oint_{a}^{b} \\
= B [l]_{a}^{b} \\
= B (b -a ) \\
= Bl
\end{align*}


### Evaluation of expression for side parallel \( cd \)  to the magnetic field


- The side \( cd \) doesnot lie on the axis of magnetic field.
- The magnetic field present across \( cd \) is negligible compared to the field present inside.



### Magnitude of angle of perpendicular sides with the axis of magnetic field

The angle between side of rectangle perpendicular to the axis of magnetic field with the magnetic field is


-  $$ 90 $$ 


### Evaluation of expression for the side  \( bc\) perpendicular to the magnetic field

\begin{align*}
\oint_{b}^{c} \vec{B}.{dl} = \oint_{b}^{c} Bdl \cos \theta \\
= \oint_{b}^{c} Bdl \cos(90) \\
= 0
\end{align*}


### Evaluation of expression for the side  \( da \) perpendicular to the magnetic field

\begin{align*}
\oint_{d}^{a} \vec{B}.{dl} = \oint_{d}^{a} Bdl \cos \theta \\
= \oint_{d}^{a} Bdl \cos(90) \\
= 0
\end{align*}


### Sum of line integral of magnetic field across the closed loop

$$ \oint \vec{B} \vec{dl} = Bl $$ 

### Equality of amperes law in solenoid

The expression of amperes law is

$$ \oint \vec{B} \vec{dl} = \mu_{0}I  $$

The expression for \( N \) number of turns is

$$ \oint \vec{B} \vec{dl} = \mu_{0}NI  $$


\begin{align*}
\oint \vec{B} \vec{dl} = \mu_{0}NI \\
\oint \vec{B} \vec{dl} = Bl \\
Bl = \mu_{0}NI \\
B = \mu_{0}I\frac{N}{l} \\
B = \mu_{0}nI 
\end{align*}



## Expression for magnetic field at the axis of solenoid from ampere's law

The expression for magnetic field at the axis of solenoid from ampere's law is

$$ B = \mu_{0}nI $$ 

# Toroid


## Structure of toroid

- Toroid is a circular structure.
- The circular structure is a form of bent solenoid.
- Toroid is a bent solenoid.


## Magnetic field at the axis of toroid

- The magnetic field at the axis of toroid is circular .
- The axis is distributed over the circle.
- The length of the distribution of the axis is the circumference of circle.
- The length of the distribution of axis is \( 2 \pi r \) .


## Derivation for the expression of magnetic field at the axis of a toroidal coil from ampere's law

\begin{align*}
\oint_{0}^{2 \pi r} \vec{B}.\vec{dl} = \oint_{0}^{2 \pi r} Bdl \cos \theta \\
= \oint_{0}^{2 \pi r} Bdl \cos(0) \\
= \oint_{0}^{2 \pi r} Bdl  \\
= B \oint_{0}^{2 \pi r} dl \\
= B [l]_{0}^{2 \pi r} \\
= B (2 \pi r - 0 ) \\
= B( 2 \pi r )
\end{align*}


### Expression of ampere's law in toroid

\begin{align*}
\oint_{0}^{2 \pi r} \vec{B}.\vec{dl} = B 2 \pi r \\
\oint_{0}^{2 \pi r} \vec{B}.\vec{dl} = \mu_{0}NI \\
B 2 \pi r = \mu_{0}NI \\
B = \frac{\mu_{0}NI}{2 \pi r}
\end{align*}


## Expression for the magnetic field at the axis of a toroidal coil from ampere's law

The expression for the magnetic field at the axis of a toroidal coil from ampere's law is

$$ B = \frac{\mu_{0}NI}{2 \pi r} $$ 





















Types of Vectors
================

-   **Null** vectors are those that have:

    -   zero magnitude

    -   any arbitary direction

-   **Polar** vectors :

    -   Start from a point and end at another point.

    -   Force, Velocity , Acceleration are examples

-   **Axial** vectors:

    -   Have no Starting point

    -   Torque , Angular momentum are examples

Addition of Two Vectors
=======================

Triangle Law
------------

-   When two vectors taken in order are represented by two sides of a
    triangle taken in order then the third side of the triangle taken in
    opposite order represents the resultant of the two vectors in
    magnitude and direction.

Parallelogram Law
-----------------

-   When two placed tail to tail are represented by adjacent sides of a
    parallelogram then the resultant of the two vectors is given by the
    diagonal of the parallelogram.

Magnitude of Resultant
----------------------

-   The magnitude is give by:
    $$R = \sqrt{A^2 + 2 AB \cos \theta + B^2}$$

-   The direction of vector $A$ with vector $R$ is:
    $$\tan \alpha = \frac{B \sin\theta}{A + B\cos\theta}$$

-   The direction of vector $B$ with vector $R$ is:

    $$\tan \beta = \frac{A \sin \theta}{A + B\cos \theta}$$

-   The magnitude of direction of a vector with resultant comes
    separately .

Special Cases for addition of two vectors
-----------------------------------------

-   The **maximum** resultant is given by: $$R_{max} = A +B$$

-   The **minimum** resultant is given by: $$R_{min} = A -B$$

-   If the direction of vectors is not mentioned the resultant lies
    between $R_{max}$ and $R_{min}$ .

Special Cases for angles
========================

Equal Magnitude
---------------

The resultant of **vectors** in the case of two vectors $A$ **equal
magnitude** *in particular* angles are:

-   Case $60^{\circ}$: $$R = \sqrt{3}A$$

-   Case $90^{\circ}$: $$R = \sqrt{2}A$$

-   Case $120^{\circ}$: $$R = A$$

Number of Vectors
=================

Resultant Zero
--------------

The number of vectors required to have the resultant zero in:

-   a plane of same magnitude is: **2**

-   a plane is: **3**

-   not in a plane is: **4**





Equilibrium on 3 vectors
========================

-   If resultant of any two vectors is equal to third vector in
    magnitude and opposite in direction.

-   The net resultant of $3$ vectors is zero.

-   The case where this condition is not satisfied, the net resultant is
    non zero.

Equilibrium on 4 vectors
========================

-   If resultant of any three vectors is equal to fourth vector in
    magnitude and opposite in direction.

-   The net resultant of $4$ vectors is zero.

-   The case where this condition is not satisfied, the net resultant is
    non zero.

Application of equilibrium
==========================

-   If the vectors are initially in equilibrium.

-   One of them is removed.

-   The change is body is equivalent to the removed vector.

<!-- -->

-   If the resultant is perpendicular to either vector, the angle
    between vectors is obtuse.

Method of solving problem related to vectors
============================================

1.  Draw the figure for condition.

2.  Apply parallelogram law for composing the figure of the condition.

3.  Check for evaluation with Pythagorean theorem.

4.  Check for evaluation with angle made by the vector with the
    resultant.

    -   This step is used often in conditions of two cases.





Vector myths
============

-   Polygonal law can never give the:

    -   magnitude of resultant of many vectors

    -   direction of resultant of many vectors

Resolution of vectors
=====================

-   When $\vec{A_1}, \vec{A_2} ..... \vec{A_n}$ act on a body at an
    angle $\theta_{1}, \theta_{2}..... \theta_{n}$ then, Total
    components of vector along the direction of $\hat{i}$ and $\hat{j}$
    is given by: $$\begin{aligned}
                    \vec{R_{x}} &= A_{1} \cos \theta_{1} \times \hat{i} + A_{2} \cos \theta_{2} \times \hat{i} + .... + A_{n} \cos \theta_{n} \times \hat{i} \\
                    \vec{R_{y}} &= A_{1} \sin \theta_{1} \times \hat{i} + A_{2} \sin \theta_{2} \times \hat{i} + .... + A_{n} \sin \theta_{n} \times \hat{i} \\
                  \end{aligned}$$

The expression for resultant by resolving into components is given by:
$$R = \sqrt{R_{x}^{2} + R_{y}^2}$$ The angle $\alpha$ with the axis
along $\hat{i}$ is given by: $$\tan \alpha = \frac{R_{y}}{R_{x}}$$

Convention of vector notation
=============================

-   East = $\hat{i}$

-   West = $\hat{-i}$

-   North = $\hat{j}$

-   South = $\hat{-j}$

-   Upward = $\hat{k}$

-   Downward = $\hat{-k}$

Convention of angle
===================

The term representing the angle of vectors is expressed as:

-   Where the angle is pointing from which side.

    -   $30^{\circ}$ North of East

        -   Oriented in north direction originating from east.

    -   $40^{\circ}$ West of North

        -   Oriented in west originating from north.

    -   $60^{\circ}$ South of West

        -   Oriented in west originating from south.

    -   $45^{\circ}$ South of East

        -   Oriented in east originating from south.


 
## Difference of vectors

The difference of vectors is the sum of vectors with the negative of another vector.


## Relation with change in vectors

The change is vector is always the difference of final vector with the initial vector

### Precaution in change of vectors

The change in vector is always evaluated from the difference of final with the initial even if

- The final vector is less in magnitude than the initial vector.

### Magnitude in change in vectors

The magnitude in change in vectors is 

- $$ Difference = Final - Initial $$


## Expression for difference in vectors

The expression for difference in vectors is

$$ \vec{R} = \vec{A} - \vec{B} \\
\vec{R} = \vec{A} + (-\vec{B}) $$

## Magnitude of resultant difference in vectors

The magnitude of difference in vectors is

$$ |\vec{R}| = \sqrt{A^{2}-2AB \cos\theta + B^{2} } $$

## Direction of resultant in difference of vectors

The magnitude of direction of resultant with a vector is

$$ \tan\alpha = \frac{B \sin\theta}{A - B \cos\theta} $$

## Minimum resultant in difference of vectors


The minimum resultant occurs in difference of vectors if the angle between the vectors is

- $$ 0^{\circ} $$

## Magnitude of minimum resultant in difference of vectors

The magnitude of minimum resultant in difference of vectors is

- $$ R = A - B $$

## Maximum resultant in difference of vectors

The maximum resultant occurs in difference of vectors if the angle between the vectors is 

- $$ 180^{\circ} $$

## Magnitude of maximum resultant in difference of vectors

The magnitude of maximum resultant in difference of vectors is

- $$ R = A + B $$



## Magnitude of difference in vectors in perpendicular vectors

The magnitude of resultant of perpendicular  vectors is expressed as


- $$ R = \sqrt{A^{2} + B^{2}} $$


## Magnitude of angle in difference of vectors in perpendicular vectors

The magnitude of angle in difference of vectors in perpendicular vectors is expressed as

- $$ \tan\alpha = \frac{B}{A} $$

# Application of difference of vectors

## Relative velocity

Relative velocity is the rate of change of displacement of a body with another body in motion.


## Convention for relative velocity

The velocity of a body relative to another body is written first in the notation.

- Velocity of body \( A \) relative to \( B \) is \( v_{ab} \).

- The velocity of the body that observes the body.
- The velocity of the body with which is referred in calculation is 
    - negated in terms of it's sign. 


## Terms Expression for relative velocity

- A body \( A \) is in motion.
- A body \( B \) is in motion.
- The velocity of body \( A \) is \( \vec{v_{a}} \).
- The velocity of body \( B \) is \( \vec{v_{b}} \).
- The angle between the velocity of body \( A \) and \( B \) is \( \theta \).

## Expression for relative velocity

The expression for velocity of body \( A \) relative to \( B \) is

- $$ \vec{v_{ab}} = \vec{v_{a}} - \vec{v_{b}} $$
- $$ \vec{v_{ab} } = \vec{v_{a}} + (- \vec{v_{b}}) $$

The expression for velocity of a body \( B \) relative to \( A \) is


- $$ \vec{v_{ba}} = \vec{v_{b}} - \vec{v_a} $$
- $$ \vec{v_{ba}} = \vec{v_{b}} + (- \vec{v_a}) $$

## Analysis of relative velocity

The introduction of \( - \vec{v_b} \) in the first expression of relative velocity implies

- The velocity is analysed with \( \vec{v_b} \) a rest.
-  $$ v_{rb} = v_{b} - v_{b} = 0 $$



## Magnitude of resultant of  relative velocity

The magnitude of relative velocity is expressed as


- $$ | \vec{v_{ab}}  | = \sqrt{v_{a}^{2} - 2v_{a} v_{b} \cos\theta + v_{b}^{2}}  $$


## Magnitude of direction of relative velocity

The magnitude of direction of relative velocity is

-   $$ \tan\alpha = \frac{v_{b}\sin\theta}{v_{a} - v_{b}\cos\theta} $$

## Condition for maximum resultant of relative velocity

The maximum resultant of relative velocity occurs is the angle between the relative velocity vectors is


-  $$ 180^{\circ} $$


## Magnitude of maximum resultant of relative velocity

The magnitude of maximum resultant of relative velocity is 

- $$ v_{ab} = v_{a} + v_{b} $$


## Magnitude of resultant of relative velocity of perpendicular vectors

The magnitude of resultant of relative velocity of perpendicular vectors is


- $$  v_{ab} = \sqrt{ v_{a}^{2} + v_{b}^2 } $$


## Magnitude of angle of resultant of relative velocity of perpendicular vectors

The magnitude of angle of resultant of relative velocity of perpendicular vectors is


- $$ \tan\alpha = \frac{v_{b}}{v_{a}} $$


## Condition for minimum resultant of relative velocity


The relative velocity of two vectors is minimum if the angle between them is 


- $$ 0^{\circ} $$



## Magnitude of minimum resultant of relative velocity

The magnitude of minimum resultant of relative velocity of two vectors is


- $$ v_{ab} = v_{a} - v_{b} $$

## Closest distance in relative velocity

The closest distance in relative velocity is the perpendicular distance to the resultant of the relative velocity.




# Crossing a river

### Precautions on problem relating crossing of river

- The condition of crossing a river is not in terms of relative velocity.
- The condition of crossing a river is in terms of resultant of vectors.  


### Vectors compositing expression in crossing of river

The vectors compositing the resultant in crossing of river are:


- Velocity of river
- Velocity of person


## Shortest path

### Magnitude of shortest path in crossing a river

The magnitude of shortest path in crossing a river is

- Resultant of width of river


### Direction of person in crossing the river

The direction of velocity of person in crossing the river in shortest path is


- Against the flow of river


### Quantities expressed in crossing of river

- The velocity of river is \( \vec{v_{r}} \) .
- The velocity of person is \( \vec{v_{p}} \) .   
- The width of the river is \( d \) .
- The time taken to cross the river is \( t \) .

### Resultant velocity in crossing of river in shortest path

- The resultant velocity in crossing of river in shortest path is \( \vec{v_{R}} \).
- The angle of resultant velocity with flow of river in shortest path is \( 90^{\circ} \) 
- The magnitude of resultant velocity in crossing the river in shortest path is \( \sqrt{v_{p}^{2} - v_{r}^{2}} \). 



### Expression for time in  shortest path in crossing the river

The expression for shortest path  for distance in crossing the river is

- $$  t = \frac{d}{|\vec{v_{R}}|}  = \frac{d}{\sqrt{\vec{v_{p}}^{2}  - \vec{v_{r}}^{2}  }}    $$ 

### Expression for direction with width of river in shortest path in crossing the river

\begin{align*}
\sin \alpha = \frac{v_{r}}{v_{p}} \\
\alpha = \sin^{-1}{\frac{v_{r}}{v_p}}
\end{align*}

### Expression for direction with the flow of river in shortest path in crossing the river

\begin{align*}
\theta = 90^{\circ} + \alpha 
\end{align*}


## Minimum time

### Components of consideration at minimum time in crossing the river

The component of consideration at minimum time in crossing the river is

- Cosine component of the velocity of the person.
- Width of river.


### Angle for minimum time in crossing the river

The angle for crossing the river in minimum time is \( 0^{\circ} \) .

### Expression of time for crossing the river in minimum time

The expression for crossing the river in minimum time is

$$ t = \frac{\text{width of river}}{\text{ cosine component of velocity of person}} = \frac{d}{v_{p}} $$


### Position of person in opposite direction.

- The width of the river is \( AB \) .
- The position of the person in opposite direction after swimming is \( C \) .


### Derivation for the expression for relation of position of person in terms of velocity in opposite direction

\begin{align*}
\tan \alpha = \frac{BC}{AB} = \frac{v_{r}}{v_{p}} \\
BC = \frac{v_{r}}{v_{p}} \times d
\end{align*}


### Expression for the magnitude of distance of person in opposite direction

The expression for magnitude of distance of person in opposite direction is expressed as

- $$ \text{distance} = \frac{v_{r}}{v_{p}} \times \text{width} $$ 







### List of domains of use of  x rays

- Medical
- Industries
- Technologies
- Security

### List of uses of x rays in medical domains


- Detect diseases
- Detect fractures
- Radiography
- Teeth decay
- Cancer therapy


### List of industrial uses of x rays

- Structural defects
- Faulty joints
- Faulty weldings

### List of technological uses of x rays

- Cathode ray tube
- Computer terminals
- Oscilloscope

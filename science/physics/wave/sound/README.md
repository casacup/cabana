# Wave

The disturbance created in the medium is called wave.

## Classification of types of wave

- The types of waves are classified on the basis of nature of transfer on a medium.

The types of waves are


- Mechanical wave
- Electromangnetic wave


## Nature of mechanical wave

Mechanical wave requires material medium to travel.

## Example of mechanical wave

The examples of mechanical wave is

- sound wave




## Nature of electromagnetic wave

Electromagnetic  wave do not require material medium to travel.


## Example of electromagnetic wave

The examples of electromagnetic wave are


- radio wave
- light wave

# Mechanical wave

## Classification of mechanical wave
The classification of mechanical wave is done on the basis of nature of vibration of particle along with the direction of wave.

## Types of mechanical wave

The types of mechanical wave are


- Longitudinal wave
- Transverse wave


# Longitudinal wave

## Vibration of particle in longitudinal wave 


- The particles vibrate in to and fro direction in longitudinal wave.
- The direction of propagation of wave and vibration of particle is same.
- The direction of propagation of wave and vibration of particle is parallel.
- Compressions and rarefactions are formed in longitudinal waves.

### Compressions

Compressions are the region of tightly packed particles.

### Rarefactions

Rarefactions are the region of loosely packed particles.



# Transverse wave

## Vibration of particle in transverse wave

- The particles vibrate in up and down direction in transverse wave.
- The direction of propagation of wave and vibration of particle is not same.
- The direction of propagation of wave and vibration of particle is perpendicular.
- There is the formation of crests and trough



### Crests
Crests are the region of vibration of particles in upward direction.

### Trough

Trough are the region of vibration of particles in downward direction.

## Amplitude


Amplitude is the maximum displacement traversed by a particle.


### Nature of propagation of sound wave (tl;dr)

Unlike the imagination of a particle moving from one point to another as an example of a particle moving from near the loudspeaker to the sensory  cells of the ear of the person standing 20 m away, sound waves execute a different kind of motion. A wave is said to be a disturbance set up in a medium. The first instance a wave is produced it's energy hits the first particle it meets in it's direction of  propagation. The first particle that is hit by the source of sound wave in turn strikes the next particle in front of it. The next particle strikes the next and so on. What is interesting is that the particles after hitting the ones in the front don't just stay at there after collision. Instead they return to their equilibrium position but their magnitude of restoring force is such that they move a farther than where they are supposed to and collide with the particle behind them. These particles strike back and forth back and forth and a sound wave is traversed by these motions of these particles. What is noticeable is that if mechanical waves need medium to travel, isn't there some space between two particles at the subtlest level? How does the description  of medium apply to the case of these separation?



# Sound waves

## Path difference 

The path difference is the magnitude of distance of a complete cycle from the symmetrical axis of the half cycles.

### Term for path difference

The analogus term for path difference is wavelength in transverse waves.

##  Wavelength

Wavelength is the distance of a full cycle of a wave along the direction of travel.

## Representation of path difference

The path difference is represented by


- $$ \lambda $$ 

## Phase difference

The phase difference is the magnitude of angle expressed as path difference.

### Representation of phase difference

The phase difference is represented by


- \( 2 \pi \) 



### Relation between phase difference and path difference

- The path difference of \( \lambda \) implies a phase difference of \( 2 \pi \).
- The phase difference of \( 2 \pi  \) implies a path difference of \( \lambda \) .
- The path difference of \( x \)  implies a phase difference \( \phi \) of
- $$ \frac{2 \pi}{\lambda} \times x $$ 


## Expression of the displacement of sound waves the origin

The expression of sound waves from the origin is

$$ y = A \sin(\omega t - \phi)$$

where \( \phi = 0 \) 


## Derivation  for the displacement of sound waves at a distance from the origin

\begin{align*}
y  = A  \sin(\omega t - \phi) \\
y = A \sin(\omega t - \frac{2 \pi}{\lambda} \times x) \\
k = \frac{2 \pi}{\lambda}
\end{align*}

## Expression for the displacement of sound waves at a distance from the origin

$$ y = A \sin(\omega t - kx) $$


### Expression of the velocity of sound waves at a distance from the origin

$$ v = \frac{dy}{dt} = \omega A \cos(\omega t - kx) $$

### Expression of the acceleration of sound waves at a distance from the origin

$$ a = \frac{d^{2}y}{dt} = - \omega^{2} A  \sin( \omega t - kx ) $$ 

## Other expressions

\begin{align*}
v_{max} = A \omega (\theta = 0)\\
v_{min} = 0 (\theta = \frac{\pi}{2})\\ 
a_{max} = - A \omega^{2} (\theta = \frac{\pi}{2})\\
a_{min} = 0 (\theta = 0)\\ 
\end{align*}

